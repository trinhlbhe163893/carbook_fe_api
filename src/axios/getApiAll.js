import axios from './myAxios'

const getCurrentRound = (data) => {
  return axios.get(`tournament/${data['tournament_search_id']}/current-round`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

const getNextRound = (data) => {
  return axios.get(`tournament/${data['tournament_search_id']}/next-round`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

const getBackTestInfo = (symbol) => {
  return axios.get(`tournament/return_and_risk_tournament/backtest-info?symbol=${symbol}`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

const downloadDataZip = (url) => {
  return axios.get(url, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
    responseType: 'blob'
  })
}

//Get all model

//Get all model
const getTournamentAll = () => {
  return axios.get('tournament/all', {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

//Get all model
const getSymbolByTournamentId = (data) => {
  return axios.get(`tournament/${data}/symbols`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

//Delete submission
const deleteSubmission = (data) => {
  if (data['submitTime'] === null) {
    return axios.delete(
      `tournament/${data['tournamentId']}/submission?submission_type=${data['submissionType']}&symbol=${data['symbol']}&model_id=${data['model']}`
    )
  } else {
    if (data['submissionType'] == 'live') {
      return axios.delete(
        `tournament/${data['tournamentId']}/submission?submission_type=${data['submissionType']}&symbol=${data['symbol']}&model_id=${data['model']}`
      )
    }
    return axios.delete(
      `tournament/${data['tournamentId']}/submission?submission_type=${data['submissionType']}&symbol=${data['symbol']}&model_id=${data['model']}&submission_time=${data['submitTime']}`
    )
  }
  // headers: {
  //   'Authorization': `Bearer ${token1}`
  // }
}

//Submit model input
const submitModelInput = (data) => {
  return axios.post(`tournament/return_and_risk_tournament/submit/input`, data, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

//Get performance all
const getPerformanceAll = (data) => {
  return axios.get(`tournament/${data['tournament']}/performance/all/?symbol=${data['symbol']}&submission_type=live`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

//PUT Change API key
const putChangeAPIKey = () => {
  return axios.put(`user/api-key`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

//getData for chart
const getDataChart = (data) => {
  return axios({
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // },
    method: 'get',
    url: `tournament/${data.tournament_search_id}/submission/overall-performance?submission_type=${data.submission_type}&symbol=${data.symbol}&model_id=${data.model_id}&submission_time=${data.submission_time}`
  })
}

//Get all model
const getSubmissionTimeAll = (data) => {
  return axios.get('tournament/return_and_risk_tournament/submission-time/all', {
    data
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

// check regiser tournament
const checkRegiterTournament = (data) => {
  return axios.get(`tournament/${data.tournament_search_id}/registered`)
}

// regiser tournament
const regiserTournament = (data) => {
  return axios.post(`tournament/register`, data, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

const getImage = () => {
  return axios.get(`user/image`, {
    headers: {
      'Cache-Control': 'no-cache',
      Pragma: 'no-cache',
      Expires: '0'
    },
    responseType: 'blob'
  })
}

// get metric info
const getMetricInfo = (id) => {
  return axios.get(`metric/${id}/info`, {
    // headers: {
    //   'Authorization': `Bearer ${token1}`
    // }
  })
}

// auth Google
const googleLogin = (data) => {
  return axios.post(`auth/google`, data, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

const getUserProfile = () => {
  return axios.get(`user`
  )
}

export {
  getCurrentRound,
  getImage,
  getNextRound,
  getSubmissionTimeAll,
  getDataChart,
  getTournamentAll,
  getSymbolByTournamentId,
  deleteSubmission,
  submitModelInput,
  getPerformanceAll,
  putChangeAPIKey,
  downloadDataZip,
  checkRegiterTournament,
  regiserTournament,
  getMetricInfo,
  googleLogin,
  getUserProfile,
  getBackTestInfo
}
