import axios from '@src/axios/myAxios'
const getCompanyList = () => {
    return axios.get(`/api/company/listcompany`)
}
const addCompany = (data) => {
    return axios.post('api/company/addcompany', data)
}

export {
    getCompanyList,
    addCompany
}