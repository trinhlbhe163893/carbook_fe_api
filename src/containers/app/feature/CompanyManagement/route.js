import AppLayout from '@src/components/Layouts/AppLayout'
import { Outlet } from 'react-router'
import Company from './page'

export const companyRouteList = [
  {
    path: '/company',
    element: (
      <AppLayout>
        <Company />
      </AppLayout>
    )
  }
]
