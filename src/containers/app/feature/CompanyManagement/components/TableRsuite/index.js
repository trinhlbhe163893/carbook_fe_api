import React from 'react'
import './Table.scss'

import { Table, Pagination } from 'rsuite'

const TableRsuite = (props) => {
  const { dataTable } = props
  const { Column, HeaderCell, Cell } = Table
  const defaultData = dataTable

  const [sortColumn, setSortColumn] = React.useState()
  const [sortType, setSortType] = React.useState()
  const [loading, setLoading] = React.useState(false)

  const [limit, setLimit] = React.useState(10)
  const [page, setPage] = React.useState(1)

  //   const [defaultData, setDefaultData] = React.useState(dataDetailsModel);
  //   console.log('DataModelssdasdasdas: ' + JSON.stringify(dataDetailsModel))
  const handleChangeLimit = (dataKey) => {
    setPage(1)
    setLimit(dataKey)
  }

  const data = defaultData.filter((v, i) => {
    const start = limit * (page - 1)
    const end = start + limit
    return i >= start && i < end
  })
  const getData = () => {
    if (sortColumn && sortType) {
      return data.sort((a, b) => {
        let x = a[sortColumn]
        let y = b[sortColumn]
        if (typeof x === 'string') {
          x = x.charCodeAt()
        }
        if (typeof y === 'string') {
          y = y.charCodeAt()
        }
        if (sortType === 'asc') {
          return x - y
        } else {
          return y - x
        }
      })
    }
    return data
  }

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
      setSortColumn(sortColumn)
      setSortType(sortType)
    }, 500)
  }

  return (
    <>
      <div className='tableScore'>
        <Table
          height={520}
          minHeight={520}
          data={getData()}
          sortColumn={sortColumn}
          sortType={sortType}
          onSortColumn={handleSortColumn}
          loading={loading}
        >
          {/* <Column width={110} align='center' fixed sortable>
            <HeaderCell>ROUNDS</HeaderCell>
            <Cell dataKey='round' />
          </Column> */}

          {/* <Column width={90} sortable>
                        <HeaderCell>TIME</HeaderCell>
                        <Cell dataKey="time" />
                    </Column> */}

          {/* <Column width={150} sortable>
            <HeaderCell>Prediction</HeaderCell>
            <Cell className='prediction' dataKey='prediction' />
          </Column>

          <Column width={110} sortable>
            <HeaderCell>Ms</HeaderCell>
            <Cell style={{ color: '#36A9E1' }} dataKey='movementScore' />
          </Column>

          <Column width={130} sortable>
            <HeaderCell>Corr15</HeaderCell>
            <Cell className='score' dataKey='corr15' />
          </Column>

          <Column width={130} sortable>
            <HeaderCell>Ms15</HeaderCell>
            <Cell className='score' dataKey='ms15' />
          </Column> */}
          <Column width={200} sortable>
            <HeaderCell>Company Name</HeaderCell>
            <Cell dataKey='name' />
          </Column>
          <Column width={200} sortable>
            <HeaderCell>Manager Name</HeaderCell>
            <Cell className='prediction' dataKey='userName' />
          </Column>

          <Column width={250} sortable>
            <HeaderCell>Location</HeaderCell>
            <Cell style={{ color: '#36A9E1' }} dataKey='location' />
          </Column>

          <Column width={130} sortable>
            <HeaderCell>Phone</HeaderCell>
            <Cell className='score' dataKey='phone' />
          </Column>

          <Column width={130} sortable>
            <HeaderCell>Status</HeaderCell>
            <Cell className='score' dataKey='status' />
          </Column>
        </Table>
      </div>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={3}
          size='l'
          layout={['total', '-', 'limit', '|', 'pager', 'skip']}
          // layout={["total", "|", "pager", "skip"]}
          total={defaultData.length}
          limitOptions={[10, 30, 50]}
          limit={limit}
          activePage={page}
          onChangePage={setPage}
          onChangeLimit={handleChangeLimit}
        />
      </div>
    </>
  )
}

export default TableRsuite
