import React from 'react';
import { Modal, Typography, Paper, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import styles from './PaymentPopup.sass';
import cn from 'classnames/bind';

const cx = cn.bind(styles);

const ImageModal = ({ open, handleClose, imgSrc }) => {
  return (
    <Modal open={open} onClose={handleClose} className={cx('modal')}>
      <Paper className={cx('modalContent')}>
        <IconButton className={cx('closeButton')} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
        <Typography variant="h6">Xác nhận thanh toán vé</Typography>
        <img src={imgSrc} alt="QR Code" className={cx('qrImage')} />
      </Paper>
    </Modal>
  );
};

export default ImageModal;
