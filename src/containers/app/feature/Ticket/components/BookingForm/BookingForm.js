// src/components/BookingForm/BookingForm.js
import React, { useState, useEffect } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'
import {
  Container,
  Grid,
  Paper,
  Typography,
  Button,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  useMediaQuery
} from '@mui/material'
import styles from './BookingForm.module.sass'
import cn from 'classnames/bind'
import WifiIcon from '@mui/icons-material/Wifi'
import RestaurantIcon from '@mui/icons-material/Restaurant'
import LocalDrinkIcon from '@mui/icons-material/LocalDrink'
import PowerIcon from '@mui/icons-material/Power'
import ChairIcon from '@mui/icons-material/Chair'
import SeatSelectionModal from '../SeatSelectionModal/SeatSelectionModal'
import AccessTimeIcon from '@mui/icons-material/AccessTime'
import ImageModal from '../Payment/PaymentPopup'
import { useSelector } from 'react-redux'
import { getTicketList } from '../../ticketService'
import { /*Toaster,*/ toast } from 'react-hot-toast'
import 'react-toastify/dist/ReactToastify.css'

const amenityIcons = {
  wifi: <WifiIcon />,
  food: <RestaurantIcon />,
  drink: <LocalDrinkIcon />,
  power: <PowerIcon />
}

const cx = cn.bind(styles)

const BookingForm = () => {
  const [busList, setBusList] = useState([])
  const [selectedBus, setSelectedBus] = useState(null)
  const [selectedSeats, setSelectedSeats] = useState([])
  const [currentBus, setCurrentBus] = useState(null)
  const [imageModalOpen, setImageModalOpen] = useState(null)

  const navigate = useNavigate()
  const [modalOpen, setModalOpen] = useState(false)
  const isMobile = useMediaQuery('(max-width: 768px)')

  const [searchParams] = useSearchParams()

  // const busList = []

  // Hàm để tính khoảng thời gian chi tiết giữa hai thời điểm
  function calculateTimeBetween(startDateTimeStr, endDateTimeStr) {
    // Chuyển đổi chuỗi thời gian thành đối tượng Date
    const startDateTime = new Date(startDateTimeStr)
    const endDateTime = new Date(endDateTimeStr)

    // Tính toán khoảng cách thời gian bằng mili giây
    const timeDifference = endDateTime - startDateTime

    // Chuyển đổi mili giây thành ngày, giờ, phút, và giây
    const daysDifference = Math.floor(timeDifference / (1000 * 3600 * 24))
    const hoursDifference = Math.floor((timeDifference % (1000 * 3600 * 24)) / (1000 * 3600))
    const minutesDifference = Math.floor((timeDifference % (1000 * 3600)) / (1000 * 60))
    const secondsDifference = Math.floor((timeDifference % (1000 * 60)) / 1000)

    return daysDifference != 0
      ? daysDifference + 'd'
      : '' + hoursDifference != 0
      ? hoursDifference + 'h'
      : '' + minutesDifference != 0
      ? minutesDifference + 'm'
      : ''
  }

  // function getSeatCodeList(seatList) {
  //   var seatValueList
  //   seatList.forEach((item) => {
  //     console.log(item.value)
  //     seatValueList
  //   })
  //   console.log('okee')
  //   console.log(seatList)
  // }
  //api
  useEffect(() => {
    const startLocation = searchParams.get('startLocation')
    const endLocation = searchParams.get('endLocation')
    const dateTime = searchParams.get('dateTime')

    console.log('1 ' + startLocation)

    console.log('2 ' + endLocation)

    console.log('3 ' + dateTime)

    const dataInput = {
      startLocation: startLocation,
      endLocation: endLocation,
      dateTime: dateTime
    }

    //Old code
    getTicketList(dataInput).then((data) => {
      console.log('alo: ' + JSON.stringify(data))
      const transformedData = data.map((item) => ({
        type: 'ECO',
        price: item.price,
        name: item.name,
        departure: item.startLocation,
        arrival: item.endLocation,
        departureTime: item.timeFrom.split('T')[1].slice(0, 5),
        arrivalTime: item.timeTo.split('T')[1].slice(0, 5),
        departureDate: item.timeFrom.split('T')[0],
        duration: calculateTimeBetween(item.timeFrom, item.timeTo),
        seatsAvailable: item.seatCode.map((seat) => seat.value),
        seatIDs: item.seatCode.map((seat) => seat.id),
        amenities: ['wifi', 'food', 'drink', 'power']
      }))

      setBusList(transformedData)
    })
  }, [searchParams.get('startLocation'), searchParams.get('endLocation'), searchParams.get('endTime')])

  //Thông tin người đặt vé
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    phone: '',
    date: '',
    tickets: 1
  })

  const handleChange = (e) => {
    const { name, value } = e.target
    setFormData({
      ...formData,
      [name]: value
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    navigate('/booking', { state: { details: formData } })
  }

  const handleOpenModal = (bus) => {
    setCurrentBus(bus)
    setModalOpen(true)
  }

  const handleCloseModal = () => {
    setModalOpen(false)
  }

  const handleSeatSelection = (seats) => {
    setSelectedBus(currentBus)
    setSelectedSeats(seats)
    handleCloseModal()
  }

  const userInfo = useSelector((state) => state.auth.user)

  const [data, setData] = useState([])
  const [error, setError] = useState(false)

  const handleOpenImageModal = () => {
    setImageModalOpen(true)

    const timer = setTimeout(() => {
      const url =
        'https://script.google.com/macros/s/AKfycbxl-rpQ9w98Bu5tW1OgH8BvyFW_TLer78rfCNq61nLX3G1WhS01Ugq1i4-XSMw0OmHp/exec'

      fetch(url)
        .then((response) => response.json())
        .then((result) => {
          if (!result.error) {
            setData(result.data)
            try {
              if (!selectedBus || !selectedSeats.length) {
                console.error('No bus or seats selected.')
                return
              }

              const amount = parseInt(selectedBus.price) * selectedSeats.length
              const description = `${selectedSeats.map((seat) => seat.value).join(' ')} ${userInfo.userName}`
              const mapSeat = selectedSeats.map((seat) => seat.value).join(' ')
              console.log(mapSeat)
              if (result.data.Price !== undefined && result.data.Content !== undefined) {
                if (parseInt(result.data.Price) === amount && result.data.Content.includes(mapSeat)) {
                  toast.success('Thanh toán thành công!')
                } else {
                  toast.error('Thanh toán không thành công.')
                }
              } else {
                toast.error('Dữ liệu không hợp lệ.')
              }

              console.log(amount)
              console.log(description)
              console.log(result.data)
              console.log(result.data.Content)
              console.log(result.data.Price)
            } catch (error) {
              console.error('Error:', error)
            }
          } else {
            setError(true)
          }
        })
        .catch(() => setError(true))
    }, 30000)

    return () => clearTimeout(timer)
  }

  const handleCloseImageModal = () => {
    setImageModalOpen(false)
  }

  const getImgSrc = () => {
    if (!selectedBus || !selectedSeats.length) return ''
    const amount = parseInt(selectedBus.price) * selectedSeats.length
    const description = `${selectedSeats.map((seat) => seat.value).join(', ')} - ${userInfo.userName}`
    const accountName = encodeURIComponent(selectedBus.name)

    console.log(description)
    const bankId = '970415'
    const accountNo = '100872266918'
    const template = 'print'

    return `https://img.vietqr.io/image/${bankId}-${accountNo}-${template}.png?amount=${amount}&addInfo=${description}&accountName=${accountName}`
  }

  return (
    <Container className={cx('bookingFormContainer')}>
      <Grid container spacing={isMobile ? 1 : 3}>
        <Grid item xs={12} sm={2}>
          <Paper className={cx('filterPanel')}>
            <Typography variant='h6' className={cx('panelTitle')}>
              Lọc Kết Quả
            </Typography>
            <br />
            <FormControl component='fieldset' className={cx('filterGroup')}>
              <Typography variant='body1' className={cx('filterTitle')}>
                Giờ xuất bến
              </Typography>
              <RadioGroup name='time' onChange={handleChange}>
                <FormControlLabel value='all' control={<Radio />} label='Tất cả' />
                <FormControlLabel value='earliest' control={<Radio />} label='Sớm nhất' />
                <FormControlLabel value='latest' control={<Radio />} label='Muộn nhất' />
              </RadioGroup>
            </FormControl>
            <FormControl component='fieldset' className={cx('filterGroup')}>
              <Typography variant='body1' className={cx('filterTitle')}>
                Giá vé
              </Typography>
              <RadioGroup name='price' onChange={handleChange}>
                <FormControlLabel value='all' control={<Radio />} label='Tất cả' />
                <FormControlLabel value='ascending' control={<Radio />} label='Tăng dần' />
                <FormControlLabel value='descending' control={<Radio />} label='Giảm dần' />
              </RadioGroup>
            </FormControl>
            <FormControl component='fieldset' className={cx('filterGroup')}>
              <Typography variant='body1' className={cx('filterTitle')}>
                Hạng xe
              </Typography>
              <RadioGroup name='class' onChange={handleChange}>
                <FormControlLabel value='all' control={<Radio />} label='Tất cả' />
                <FormControlLabel value='royal' control={<Radio />} label='Royal' />
                <FormControlLabel value='vip' control={<Radio />} label='VIP' />
                <FormControlLabel value='eco' control={<Radio />} label='ECO' />
              </RadioGroup>
            </FormControl>
            <FormControl component='fieldset' className={cx('filterGroup')}>
              <Typography variant='body1' className={cx('filterTitle')}>
                Giờ chạy
              </Typography>
              <RadioGroup name='departureTime' onChange={handleChange}>
                <FormControlLabel value='all' control={<Radio />} label='Tất cả' />
                <FormControlLabel value='morning' control={<Radio />} label='00:00 - 12:00' />
                <FormControlLabel value='afternoon' control={<Radio />} label='12:00 - 24:00' />
              </RadioGroup>
            </FormControl>
            <FormControl className={cx('btnGroup')}>
              <Button variant='contained' color='warning' className={cx('filterGroup')}>
                Tìm
              </Button>
            </FormControl>
          </Paper>
        </Grid>

        <Grid item xs={12} sm={7}>
          <Paper className={cx('listPanel')}>
            {busList.map((bus, index) => (
              <Paper key={index} className={cx('busCard')}>
                <Grid container spacing={isMobile ? 1 : 2}>
                  <Grid item xs={12} sm={9}>
                    <Typography variant='body1' className={cx('busPrice')}>
                      {bus.name}
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={3}>
                    <Typography variant='body1' className={cx('busType')}>
                      {bus.type}
                      <br />
                      <span className={cx('priceTag')}>{bus.price}đ</span>
                    </Typography>
                  </Grid>
                </Grid>
                <div className={cx('horizontalLine')}></div>
                <div className={cx('routeContainer')}>
                  <div className={cx('routePoint')}>
                    <Typography variant='body2' className={cx('MuiTypography-body2')}>
                      {bus.departure} <br />
                      {bus.departureTime}
                    </Typography>
                  </div>
                  <div className={cx('routeLine')}>
                    <div className={cx('routeDot')}></div>
                    <div className={cx('routeDash')}></div>
                  </div>
                  <div className={cx('routeDuration')}>
                    <Typography variant='body2' className={cx('MuiTypography-body2')}>
                      {bus.duration}
                    </Typography>
                  </div>
                  <div className={cx('routeLine')}>
                    <div className={cx('routeDash')}></div>
                    <div className={cx('routeDot')}></div>
                  </div>
                  <div className={cx('routePoint')}>
                    <Typography variant='body2' className={cx('MuiTypography-body2')}>
                      {bus.arrival} <br />
                      {bus.arrivalTime}
                    </Typography>
                  </div>
                </div>
                <Grid container spacing={isMobile ? 1 : 2}>
                  <Grid item xs={8.5}>
                    <div className={cx('busAmenities')}>
                      Tiện ích:
                      <br />
                      <br />
                      {bus.amenities.map((amenity, i) => (
                        <span key={i} className={cx('amenityIcon')}>
                          {amenityIcons[amenity]}
                        </span>
                      ))}
                    </div>
                    <br />
                    <br />
                    <Typography variant='body2' className={cx('MuiTypography-body3')}>
                      <AccessTimeIcon />
                      Khởi hành: {bus.departureDate}
                    </Typography>
                  </Grid>
                  <Grid item xs={3.5}>
                    <div className={cx('BtnContainer')}></div>
                    <Typography variant='h6' className={cx('avaiableSeatStyle')}>
                      <ChairIcon className={cx('chairIcon')} />
                      {bus.seatsAvailable.length} chỗ trống
                    </Typography>
                    <br />
                    <Button
                      variant='contained'
                      color='warning'
                      className={cx('BtnItem')}
                      onClick={() => handleOpenModal(bus)}
                    >
                      Chọn chỗ
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            ))}
          </Paper>
        </Grid>
        <Grid item xs={12} sm={3}>
          <Paper className={cx('summaryPanel')}>
            <Typography variant='h6' className={cx('panelTitle')}>
              Chỗ đã chọn
            </Typography>
            <br />
            {selectedBus && (
              <>
                <Typography variant='body1' className={cx('MuiTypography-body4')}>
                  Loại xe: {selectedBus.type}
                </Typography>
                <Typography variant='body1' className={cx('MuiTypography-body4')}>
                  Nhà xe: {selectedBus.name}
                </Typography>
                <Typography variant='body1' className={cx('MuiTypography-body4')}>
                  Giờ khởi hành: {selectedBus.departureTime}
                </Typography>
                <Typography variant='body1' className={cx('MuiTypography-body4')}>
                  Giá vé: {selectedBus.price}đ
                </Typography>
                <Typography variant='body1' className={cx('MuiTypography-body4')}>
                  Chỗ ngồi: {selectedSeats.map((seat) => seat.value).join(', ')}
                </Typography>
                {/* <Typography variant='body1' className={cx('MuiTypography-body4')}>
                  Mã chỗ ngồi: {selectedSeats.map((seat) => seat.id).join(', ')}
                </Typography> */}
              </>
            )}
            <Typography variant='body1' className={cx('MuiTypography-body5')}>
              Tổng cộng: {selectedBus ? parseInt(selectedBus.price) * selectedSeats.length + 'đ' : 0 + 'đ'}
            </Typography>
            <br />
            <Button variant='contained' color='warning' className={cx('checkoutButton')} onClick={handleOpenImageModal}>
              Thanh toán
            </Button>
          </Paper>
        </Grid>
      </Grid>

      <SeatSelectionModal
        open={modalOpen}
        handleClose={handleCloseModal}
        onSeatSelect={handleSeatSelection}
        bus={currentBus}
      />

      <ImageModal open={imageModalOpen} handleClose={handleCloseImageModal} imgSrc={getImgSrc()} />
    </Container>
  )
}

export default BookingForm
