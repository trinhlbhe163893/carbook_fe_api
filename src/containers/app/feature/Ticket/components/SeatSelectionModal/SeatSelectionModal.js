import React, { useState } from 'react'
import { Modal, Box, Button, Typography } from '@mui/material'
import styles from './SeatSelectionModal.module.sass'
import PersonIcon from '@mui/icons-material/Person'
import cn from 'classnames/bind'

const cx = cn.bind(styles)

//2. Nhận thông tin từ Form và hiển thị Modal
const SeatSelectionModal = ({ open, handleClose, onSeatSelect, bus }) => {
  const [selectedSeats, setSelectedSeats] = useState([])

  //3. Xử lý chọn và hủy chọn ghế
  const handleSeatClick = (seat) => {
    setSelectedSeats((prevSeats) => {
      if (prevSeats.some((s) => s.id === seat.id)) {
        return prevSeats.filter((s) => s.id !== seat.id)
      } else {
        return [...prevSeats, seat]
      }
    })
  }

  //4. Xác nhận chọn
  const handleConfirm = () => {
    onSeatSelect(selectedSeats) // 4.1 Gửi danh sách ghế đã chọn về `BookingForm`
    handleClose() //4.2 Đóng modal
  }

  return (
    <Modal open={open} onClose={handleClose}>
      <Box className={cx('modalBox')}>
        <Typography variant='h6'>Chọn chỗ ngồi</Typography>
        <div className={cx('seatsContainer')}>
          <div className={cx('steeringWheelIcon')}>
            <PersonIcon sx={{ fontSize: 40 }} />
          </div>
          {bus &&
            bus.seatsAvailable.map((seatValue, index) => (
              <div
                key={bus.seatIDs[index]}
                className={cx('seat', {
                  selected: selectedSeats.some((s) => s.value === seatValue)
                })}
                onClick={() => handleSeatClick({ id: bus.seatIDs[index], value: seatValue })}
              >
                {seatValue}
              </div>
            ))}
        </div>
        <div className={cx('actions')}>
          <Button variant='contained' color='primary' onClick={handleConfirm}>
            Xác nhận
          </Button>
          <Button variant='contained' color='secondary' onClick={handleClose}>
            Hủy
          </Button>
        </div>
      </Box>
    </Modal>
  )
}

export default SeatSelectionModal
