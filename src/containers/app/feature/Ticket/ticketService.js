import axios from '@src/axios/myAxios';
import { useState, useEffect } from 'react';

const getTicketList = (data) => {
  return axios.get('api/ticket/' + data.startLocation + '/' + data.endLocation + '/' + data.dateTime)
}


// const useFetchData = () => {
//   const [data, setData] = useState([]);
//   const [error, setError] = useState(false);

//   const url = 'https://script.google.com/macros/s/AKfycbxl-rpQ9w98Bu5tW1OgH8BvyFW_TLer78rfCNq61nLX3G1WhS01Ugq1i4-XSMw0OmHp/exec';

//   useEffect(() => {
//     fetch(url)
//       .then(response => response.json())
//       .then(result => {
//         if (!result.error) {
//           setData(result.data);
//         } else {
//           setError(true);
//         }
//       })
//       .catch(() => setError(true));
//   }, []);

//   return { data, error };
// };




export {  getTicketList };