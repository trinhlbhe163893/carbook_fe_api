import AppLayout from '@src/components/Layouts/AppLayout'
import BookingForm from './pages/Booking'

export const booking = [
  {
    path: '/booking',
    element: (
      <AppLayout>
        <BookingForm />
      </AppLayout>
    )
  }
]
