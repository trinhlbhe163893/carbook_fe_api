/* eslint-disable no-unused-vars */
import React from 'react'
import cn from 'classnames/bind'
import { Container } from '@mui/material'

import styles from './TeamProfile.module.sass'

import BookingForm from '../../components/BookingForm/BookingForm'

const cx = cn.bind(styles)

const Booking = () => {
  return (
    <Container>
      <BookingForm />
    </Container>
  )
}

export default Booking
