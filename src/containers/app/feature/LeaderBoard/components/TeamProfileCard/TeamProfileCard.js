/* eslint-disable no-unused-vars */
import React, { useState } from 'react'

import cn from 'classnames/bind'
import { ChevronRight } from 'lucide-react'
import { Button } from '@mui/material'

import styles from './TeamProfileCard.module.sass'
import { DiamondPoint, TrophyBronze, TrophyGold, TrophySilver } from '@src/assets/svgs'

// eslint-disable-next-line react/jsx-key
const trophys = [<TrophyGold />, <TrophySilver />, <TrophyBronze />]

const cx = cn.bind(styles)

const TeamProfileCard = ({ top }) => {
  return (
    <div className={cx('team-profile-card')}>
      <div className={cx('team-info')}>
        <div className={cx('avatar-box')}>
          <img
            src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUxFRyyPtXzq-Cvpe0UN4cuzXKuXwDkCNwxnhcoo8jdbQAzLZN6eCV13e4IMlFsMcb148&usqp=CAU'
            alt='avatar'
            className={cx('avatar-img')}
          />
        </div>
        <div>
          <div className={cx('team-name')}>Oraichain US</div>
          <div>6 members</div>
        </div>
      </div>

      <div className={cx('rank')}>
        Top 1 &nbsp; <TrophyGold />
      </div>

      <div>
        <div className={cx('group-score')}>
          {new Array(3).fill(1).map((_, id) => (
            <div key={`score-${id}`} className={cx('score-item')}>
              <div className={cx('data')}>
                <div className={cx('score-point')}>92</div>
                <div className={cx('score-name')}>{`Score ${id + 1}`}</div>
              </div>
              {id < 2 && <div className={cx('border')}></div>}
            </div>
          ))}
        </div>
        <div className={cx('point')}>
          <DiamondPoint />
          <div className={cx('num-of-point')}>92</div>
          <div>points</div>
        </div>
      </div>
    </div>
  )
}

export default TeamProfileCard
