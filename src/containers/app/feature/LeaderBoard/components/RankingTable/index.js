/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
import cn from 'classnames/bind'
import { Col, Row } from 'antd'
import styles from './RankingTable.module.sass'
import { InforIcon, TrophyBronze, TrophyGold, TrophySilver } from '@src/assets/svgs'
import Avatar from '@src/components/Avatar'
import Hint from '@src/components/Hint'

// eslint-disable-next-line react/jsx-key
const trophys = [<TrophyGold />, <TrophySilver />, <TrophyBronze />]
const scoreName = {
  MOVEMENT_SCORE: 'Movement Score',
  CORRELATION: 'Correlation',
  TRUE_CONTRIBUTION: 'True Contribution',
  GENERAL_SCORE: 'General Scrore'
}

const cx = cn.bind(styles)

const RankingTable = ({ data }) => {

  function getWeekNumber(timestamp) {
    const date = new Date(timestamp);

    // Get the year and week number from the date
    const year = date.getFullYear();
    const week = Math.ceil(((date - new Date(year, 0, 1)) / 86400000 + new Date(year, 0, 1).getDay() + 1) / 7);
    
    return week;
  }

  // console.log("isShowHintObj: "+JSON.stringify(isShowHintObj))
  if (!data?.length) return
  return (
    <div className={cx('ranking-table')}>
      <Row className={cx('table-header')}>
        <Col span={5} className={cx('collumn')}>
          Name
        </Col>
        <Col span={2} className={cx('collumn')}>
          Rank
        </Col>
        {/* <Col span={4} className={cx('collumn')}>
          Number of submissions
        </Col> */}
        <Col span={3} className={cx('collumn')}>
          Model ID
        </Col>
        <Col
          span={2}
          className={cx('collumn')}
          style={{display: 'flex'}}
        >
          MS
          <Hint content={`Current week's movement score`} />
        </Col>
        <Col
          span={2}
          className={cx('collumn')}
          style={{display: 'flex'}}
        >
          MS15
          <Hint content={`Last 15 rounds' movement score`} />
        </Col>
        <Col
          span={2}
          className={cx('collumn')}
          style={{ display: 'flex' }}
        >
          CORR
          <Hint content={`Current week's correlation`} />
        </Col>
        <Col
          span={3}
          className={cx('collumn')}
          style={{ display: 'flex' }}
        >
          CORR15
          <Hint content={`Last 15 rounds' correlation`} />
        </Col>
        <Col span={3} className={cx('collumn')}>
          General Score
        </Col>
        <Col span={2} className={cx('collumn')}>
          Week
        </Col>
      </Row>
      {/* <Row className={cx('table-row-owner')}>
        <Col span={8} className={cx('collumn')}>
          <Avatar src='https://www.zmo.ai/wp-content/uploads/2023/01/2_0_420564623.png' /> &nbsp; &nbsp;
          <div>Your team</div>
        </Col>
        <Col span={3} className={cx('collumn')}>
          1
        </Col>
        <Col span={3} className={cx('collumn')}>
          1
        </Col>
        <Col span={3} className={cx('collumn')}>
          1
        </Col>
        <Col span={3} className={cx('collumn')}>
          1
        </Col>
        <Col span={4} className={cx('collumn')}>
          1
        </Col>
      </Row> */}
      {data?.map((user, id) => (
        <Row className={cx('table-row')} key={id}>
          <Col span={5} className={cx('collumn')}>
            {user[6] ? (
              <img alt='' className={cx('avatar')} src={user[6]} />
            ) : (
              <div className={cx('avatar')}>{user[1]?.charAt(0).toUpperCase()}</div>
            )}
            <div>{user[1]}</div>
          </Col>
          <Col span={2} className={cx('collumn')}>
            {id + 1}
          </Col>
          {/* <Col span={4} className={cx('collumn')}>
            1
          </Col> */}
          <Col span={3} className={cx('collumn')}>
            {user[2]}
          </Col>
          <Col span={2} className={cx('collumn')} style={{ whiteSpace: 'nowrap' }}>
            {user[3].toFixed(4)}
          </Col>
          <Col span={2} className={cx('collumn')} style={{ whiteSpace: 'nowrap' }}>
            {user[7]?.toFixed(4)}
          </Col>
          <Col span={2} className={cx('collumn')} style={{ whiteSpace: 'nowrap' }}>
            {user[8]?.toFixed(4)}
          </Col>
          <Col span={3} className={cx('collumn')} style={{ whiteSpace: 'nowrap' }}>
            {user[4].toFixed(4)}
          </Col>
          <Col span={3} className={cx('collumn')}>
            {user[5]?.toFixed(4)}
          </Col>
          <Col span={2} className={cx('collumn')}>
            {getWeekNumber(user[0])}
          </Col>
        </Row>
      ))}
    </div>
  )
}



export default RankingTable
