/* eslint-disable no-unused-vars */
import React, { useState } from 'react'

import cn from 'classnames/bind'
import { ChevronRight } from 'lucide-react'
import { Button } from '@mui/material'

import styles from './TopCard.module.sass'
import { DiamondPoint, TrophyBronze, TrophyGold, TrophySilver } from '@src/assets/svgs'
import { useNavigate } from 'react-router'

// eslint-disable-next-line react/jsx-key
const trophys = [<TrophyGold />, <TrophySilver />, <TrophyBronze />]
const avatarImg = [
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRUxFRyyPtXzq-Cvpe0UN4cuzXKuXwDkCNwxnhcoo8jdbQAzLZN6eCV13e4IMlFsMcb148&usqp=CAU',
  'https://images.nightcafe.studio//assets/tdraw-girl.jpg?tr=w-1200,c-at_max',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSr0JUZMIusYn5HRr0EpjHYgES0PtBzCb_f6SDWbfiNvkgT7jGY_gaVElrTSTDO9KhWgaI&usqp=CAU'
]
const scoreName = {
  MOVEMENT_SCORE: 'Movement Score',
  CORRELATION: 'Correlation',
  TRUE_CONTRIBUTION: 'True Contribution',
  GENERAL_SCORE: 'General scrore'
}

const cx = cn.bind(styles)

const TopCard = ({ top, info }) => {
  if (!info) return

  return (
    <div className={cx('top-card')}>
      <div className={cx('avatar-box')} style={{ padding: top === 1 ? '5px' : '0px' }}>
        <img src={info[6] ? info[6] : avatarImg[top - 1]} alt='avatar' className={cx('avatar-img')} />
        <div className={cx('rank')}>{top}</div>
      </div>
      <div style={{}}>
        <div className={cx('title')}>
          <div className={cx('name')}>Model: {info[2]}</div>
          {trophys[top - 1]}
        </div>
        <div className={cx('score-name')} style={{ textAlign: 'center' }}>
          Developed by: {info[1]}
        </div>
        <div className={cx('group-score')}>
          {
            <div className={cx('score-item')}>
              <div className={cx('data')}>
                <div className={cx('score-point')}>MS</div>
                <div className={cx('score-name')}>{info[3].toFixed(4)}</div>
              </div>
              {<div className={cx('border')}></div>}
              <div className={cx('data')}>
                <div className={cx('score-point')}>CORR15</div>
                <div className={cx('score-name')}>{info[4].toFixed(4)}</div>
              </div>
              {<div className={cx('border')}></div>}
              <div className={cx('data')}>
                <div className={cx('score-point')}>General Score</div>
                <div className={cx('score-name')}>{info[5].toFixed(4)}</div>
              </div>
            </div>
          }
        </div>
        <div className={cx('point')}>
          <DiamondPoint />
          <div className={cx('num-of-point')}>{info[5].toFixed(4)}</div>
          <div>points</div>
        </div>
        {/* <Button className={cx('button')} onClick={() => navigate('/team-profile')}>
          View Profile &nbsp; <ChevronRight />{' '}
        </Button> */}
      </div>
    </div>
  )
}

export default TopCard
