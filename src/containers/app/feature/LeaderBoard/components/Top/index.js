/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import useMediaQuery from '@src/hooks/useMediaQuery'
import classNames from 'classnames/bind'
import styles from './index.module.sass'
import TopCard from './TopCard'

const cx = classNames.bind(styles)

function Top({ data }) {
  // console.log("DATAA: "+JSON.stringify(data))
  const isSm = useMediaQuery('(max-width: 768px)')
  if (data) {
    return (
      <div className={cx('top')}>
        {!isSm ? (
          <>
            {data?.length > 1 && (
              <div className={cx('top2')}>
                <TopCard top={2} info={data[1]} />
              </div>
            )}

            <div className={cx('top1')} style={{ paddingTop: data?.length < 2 ? '180px' : '60px' }}>
              <TopCard top={1} info={data[0]} />
            </div>

            {data?.length > 2 && (
              <div className={cx('top3')}>
                <TopCard top={3} info={data[2]} />
              </div>
            )}
          </>
        ) : (
          <>
            <div className={cx('top1')}>
              <TopCard top={1} info={data[0]} />
            </div>
            {data?.length > 1 && (
              <div className={cx('top2')}>
                <TopCard top={2} info={data[1]} />
              </div>
            )}
            {data?.length > 2 && (
              <div className={cx('top3')}>
                <TopCard top={3} info={data[2]} />
              </div>
            )}
          </>
        )}
      </div>
    )
  } else {
    return <div></div>
  }
}

export default Top
