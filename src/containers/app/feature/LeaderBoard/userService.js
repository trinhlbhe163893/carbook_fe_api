import { createApi } from '@reduxjs/toolkit/query/react'
import customFetchBase from '@src/configs/customFetchBase'

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: customFetchBase,
  endpoints: (build) => ({
    getPersonalPerformation: build.query({
      query: () => ({
        url: `/user/performance`
      })
    }),
    getLeaderboard: build.query({
      query: () => {
        return {
          url: '/competition/nestquant_tournament_2023/performance/all?submission_type=public-test'
        }
      }
    })
  })
})

export const { useGetPersonalPerformationQuery, useGetLeaderboardQuery } = userApi
