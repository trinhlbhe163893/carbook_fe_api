import HeaderOnlyLayout from '@src/components/Layouts/HeaderOnlyLayout'
import Leaderboard from './pages/Leaderboard'
import TeamProfile from './pages/TeamProfile'

export const boardRouteList = [
  {
    path: '/leaderboard',
    element: (
      <HeaderOnlyLayout>
        <Leaderboard />
      </HeaderOnlyLayout>
    )
  },
  {
    path: '/team-profile',
    element: (
      <HeaderOnlyLayout>
        <TeamProfile />
      </HeaderOnlyLayout>
    )
  }
]
