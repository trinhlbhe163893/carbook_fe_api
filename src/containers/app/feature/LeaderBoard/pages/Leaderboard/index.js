/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import { Container } from '@mui/material'
import classNames from 'classnames/bind'

import styles from './Leaderboard.module.sass'
import './Leaderboard.scss'

import Top from '../../components/Top'

import bgr_leaderboard from '@src/assets/images/bgr_leaderboard.png'
import leaderboardImg from '@src/assets/images/leaderboard.png'
import { ROUND, ROUND_ID } from '@src/configs'
import { useEffect, useState } from 'react'
// import { useLazyGetLeaderboardQuery, userApi } from '../../../User/userService'
import RankingTable from '../../components/RankingTable'

import ModelTables from '../../../User/components/ModelTable/ModelTable'
import DropDownComponent from '../../../User/components/DropDown'
import { getTournamentAll, getSymbolByTournamentId, getPerformanceAll } from '@src/axios/getApiAll'
import { Col, Row } from 'antd'
import useMediaQuery from '@src/hooks/useMediaQuery'
import ReactLoading from 'react-loading'

const cx = classNames.bind(styles)
const competitionId = process.env.COMPETITION_ID

function Leaderboard() {
  const maxSm = useMediaQuery('(max-width: 992px)')
  const [listTournamentId, setListTournamentId] = useState([])
  const [tournamentSearchObj, setTournamentSearchObj] = useState()

  const [listSymbol, setListSymbol] = useState([])
  const [symbol, setSymbol] = useState('')

  const [data, setData] = useState()

  // const { data: currentRound } = userApi.endpoints.getCurrentRound.useQuery(competitionId)
  // const [getLeaderboard, { data: leaderboard }] = useLazyGetLeaderboardQuery()
  const [leaderboardSorted, setLeaderboardSorted] = useState([])

  const [selected, setSelected] = useState()

  const [isLoadingData, setIsLoadingData] = useState(false)

  if (listTournamentId.length > 0 && !tournamentSearchObj) {
    setTournamentSearchObj(listTournamentId[0])
  }

  // console.log('tournamentSearchObj: '+JSON.stringify(tournamentSearchObj))
  // console.log('symbol: '+JSON.stringify(symbol))

  useEffect(() => {
    getTournamentAll().then((data) => {
      var lis = data.map((i) => {
        return { id: i['search_id'], value: i['name'] }
      })
      // console.log('datatat: '+JSON.stringify(data))
      setListTournamentId(lis)
      console.log(lis)
    })
    if (tournamentSearchObj && symbol) {
      setIsLoadingData(true)
      getPerformanceAll({ tournament: tournamentSearchObj['id'], symbol: symbol['id'] })
        .then((data) => {
          setData(
            data['Data']
            // data['Data'].sort((a, b) => {
            //   return -(a[5] - b[5])
            // })
          )
          setIsLoadingData(false)
        })
        .catch((error) => {
          setIsLoadingData(false)
        })
    }
    // getSymbolByTournamentId('return_and_risk_tournament' ,token).then(data => {
    //   setListSymbol(data['Symbols'])
    // })
    getSymbolByTournamentId('return_and_risk_tournament').then((data) => {
      // console.log("SYM: "+JSON.stringify(data))
      var lis = data['Symbols'].map((s) => {
        return { id: s, value: s }
      })
      // console.log("LIS: "+JSON.stringify(lis))
      setListSymbol(lis)
    })
  }, [tournamentSearchObj, symbol])

  if (listSymbol.length > 0 && !symbol) {
    setSymbol(listSymbol[0])
  }

  // console.log('data: '+JSON.stringify(data))

  // useEffect(() => {
  //   console.log('round:: ', ROUND_ID[selected], ROUND[currentRound])
  //   if (currentRound)
  //     getLeaderboard({
  //       competition_id: competitionId,
  //       submission_type: ROUND_ID[selected] || currentRound['Current round']
  //     })
  // }, [selected, currentRound])

  // useEffect(() => {
  //   if (currentRound) setSelected(ROUND[currentRound['Current round']])
  // }, [currentRound])

  // useEffect(() => {
  //   leaderboard && sortLeaderboard()
  // }, [leaderboard])

  // const sortLeaderboard = () => {
  //   const leaderboardArray = Object.entries(leaderboard)
  //   const sort = leaderboardArray.sort((a, b) => {
  //     return b[1].GENERAL_SCORE - a[1].GENERAL_SCORE
  //   })
  //   setLeaderboardSorted(sort)
  // }

  const handleFilter = (option) => {
    setSelected(option)
  }

  // console.log("DATASLICE: "+JSON.stringify(data?.slice(0,3)))
  // console.log("DATASLICERAW: "+JSON.stringify(data))

  return (
    <div className={cx('leaderboard')} style={{ backgroundImage: `url(${bgr_leaderboard})` }}>
      <Container className={cx('container')}>
        <div className={cx('mainContent')} style={{ backgroundImage: `url(${leaderboardImg})` }}>
          <div style={{}} className={cx('filter')}>
            <div className='' style={maxSm ? { width: '100%' } : { width: '60%' }}>
              <Row style={{ justifyContent: 'space-around' }}>
                <Col md={11} xs={24} style={{ marginBottom: 10 }}>
                  <DropDownComponent
                    width={'100%'}
                    currentState={tournamentSearchObj}
                    setCurrentState={setTournamentSearchObj}
                    listElement={listTournamentId?.map((i) => {
                      return { id: i['id'], value: i['value'] }
                    })}
                  />
                </Col>
                <Col md={11} xs={24}>
                  <DropDownComponent
                    width={'100%'}
                    currentState={symbol}
                    setCurrentState={setSymbol}
                    listElement={listSymbol?.map((i) => i)}
                  />
                </Col>
              </Row>
            </div>
          </div>
          {isLoadingData ? (
            <div style={{ height: 250, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <ReactLoading type='spin' height={32} width={32} />
            </div>
          ) : (
            <>
              {data?.length !== 0 && <Top data={data?.slice(0, 3)} />}
              {data?.length !== 0 ? (
                <div className={cx('table')}>
                  <RankingTable data={data} />
                </div>
              ) : (
                <div className={cx('no-data')}>No data to display</div>
              )}
            </>
          )}
        </div>
      </Container>
    </div>
  )
}

export default Leaderboard
