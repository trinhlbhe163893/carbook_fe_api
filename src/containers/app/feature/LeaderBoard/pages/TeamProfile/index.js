/* eslint-disable no-unused-vars */
import React from 'react'
import cn from 'classnames/bind'
import { Container, IconButton } from '@mui/material'

import styles from './TeamProfile.module.sass'
import TeamProfileCard from '../../components/TeamProfileCard/TeamProfileCard'
import { Avatar } from 'antd'
import { MoreVertical } from 'lucide-react'

const cx = cn.bind(styles)

const TeamProfile = () => {
  return (
    <Container>
      <div className={cx('team-profile')}>
        <div className={cx('left')}>
          <TeamProfileCard />
        </div>
        <div className={cx('right')}>
          <div className={cx('title')}>Team member</div>
          <div className={cx('list-member')}>
            {new Array(6).fill(1).map((_, id) => (
              <div className={cx('member')} key={id}>
                <div className={cx('member-left')}>
                  <Avatar size={50} src='https://images.nightcafe.studio//assets/tdraw-girl.jpg?tr=w-1200,c-at_max' />{' '}
                  &nbsp; &nbsp; &nbsp; &nbsp;
                  <div>
                    <div className={cx('name')}>User name</div>
                    <div className={cx('label')}>Admin</div>
                  </div>
                </div>
                <div>
                  <IconButton>
                    <MoreVertical className={cx('member-right')} />
                  </IconButton>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Container>
  )
}

export default TeamProfile
