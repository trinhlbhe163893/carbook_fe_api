import { createSlice } from '@reduxjs/toolkit'

const homeSlice = createSlice({
  name: 'home',
  initialState: { showIntro: true, showGettingStart: true, isShowLoading: false },
  reducers: {
    setShowOff: (state, action) => {
      state.showIntro = false
    },
    setShowGettingStartOff: (state, action) => {
      state.showGettingStart = false
    },
    setShowGettingStartOn: (state, action) => {
      state.showGettingStart = true
    },
    setIsShowLoading: (state, action) => {
      state.isShowLoading = action.payload
    },
  }
})

export const { setShowOff, setShowGettingStartOff, setShowGettingStartOn, setIsShowLoading } = homeSlice.actions
export default homeSlice.reducer
