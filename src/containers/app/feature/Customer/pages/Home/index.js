/* eslint-disable no-undef */

import classNames from 'classnames/bind'
import styles from './Home.module.sass'
import IntroduceSection from '../../components/IntroductSection'
// import RoundSection from '../../components/RoundSection'
import GuideSection from '../../components/GuideSection'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { Container } from '@mui/material'
import { useSearchParams, useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { googleLogin } from '@src/axios/getApiAll'
import Cookies from 'universal-cookie'
import { useLazyGetProfileQuery } from '@src/containers/authentication/feature/Auth/authService'
import { useDispatch, useSelector } from 'react-redux'
import { login, setUser } from '@src/containers/authentication/feature/Auth/authSlice'
import AppModal from '@src/components/AppModal/AppModal'
import imageBackgroundImgHome from '@src/assets/images/popup_image_home.png'
import { setIsShowLoading, setShowOff } from './homeSlice'
import { ExitIcon } from '@src/assets/svgs'

const cookies = new Cookies()

const cx = classNames.bind(styles)

function Home() {
  const isShowPopup = useSelector((state) => state.home.showIntro)
  const navigate = useNavigate()
  const maxSm = useMediaQuery('(max-width: 992px)')
  const [searchParams, setSearchParams] = useSearchParams();
  const [getProfile] = useLazyGetProfileQuery()

  const dispatch = useDispatch()

  // const [isOpenPopup, setIsOpenPopup] = useState(true)

  const handleClosePopup = () => {
    dispatch(setShowOff())
  }


  useEffect(() => {
    if (searchParams.has('auth_str')) {
      googleLogin({ auth_str: searchParams.get('auth_str') })
        .then((data) => {
          try {
            cookies.set('access_token', data['access_token'], {
              path: '/',
              domain: process.env.DOMAIN
            }) //path=/;domain=localhost
          } catch (error) {
            console.log('errorSetCookie: ' + error)
          }
          try {
            cookies.set('access_token_signal', data['access_token'], {
              path: '/',
              domain: process.env.DOMAIN
            })
          } catch (error) {
            console.log('errorSetCookieSignal: ' + error)
          }
          if (data['access_token']) {
            // dispatch(setIsShowLoading(true))
            getProfile('b', false)
              .then((response) => {
                dispatch(setUser({ ...response.data }))
                dispatch(login())
                try {
                  if (cookies.get('current_page') == process.env.SIGNAL_PAGE) {
                    searchParams.delete('auth_str')
                    setSearchParams(searchParams)
                    navigate(`//${process.env.SIGNAL_PAGE}`)
                  } else {
                    searchParams.delete('auth_str')
                    setSearchParams(searchParams)
                    // navigate('/')
                  }
                } catch (error) {
                  console.log("errorGetCurrentPage: " + error)
                  searchParams.delete('auth_str')
                  setSearchParams(searchParams)
                  // navigate('/')
                }
                // dispatch(setIsShowLoading(false))
              })
              .catch((err) => {
                console.log(err);
                // dispatch(setIsShowLoading(false))
              })


          }
        })
        .catch((error) => {
          console.log('error: ' + error)
        })
    }
  }, [])

  return (
    <div className={cx('home-wrapper')}>
      <Container>
        <div className={cx('home-child')}>
          <IntroduceSection />
          {/* <RoundSection /> */}
          <GuideSection />{' '}
          {maxSm ? (
            <>
              <div className={cx('gradient-1-mobile')}></div>
              <div className={cx('gradient-2-mobile')}></div>
              <div className={cx('gradient-4-mobile')}></div>
              <div className={cx('gradient-5-mobile')}></div>
              <div className={cx('ellipse-23-mobile')}></div>
              <div className={cx('ellipse-89-mobile')}></div>
              <div className={cx('ellipse-91-mobile')}></div>{' '}
            </>
          ) : (
            <>
              <div className={cx('gradient-1')}></div>
              <div className={cx('gradient-2')}></div>
              <div className={cx('gradient-4')}></div>
              <div className={cx('gradient-5')}></div>
              <div className={cx('ellipse-23')}></div>
              <div className={cx('ellipse-89')}></div>
              <div className={cx('ellipse-91')}></div>
            </>
          )}
        </div>
        <AppModal
          width={maxSm ? 300 : 399}
          contentStyle={{
            borderRadius: '16px',
            padding: '20px 24px 24px 24px',
            border: 'none',
            backgroundColor: '#29384E'
          }}
          isOpen={isShowPopup}
        >
          <div style={{ display: 'flex' }}>
            <div className={cx('textBoxUpShow')}>NestQuant Open Beta 2.0+ is now available!</div>
          </div>
          <div onClick={() => handleClosePopup()} className={cx('iconExit')}>
            <ExitIcon />
          </div>

          <div style={{ fontSize: 15, color: '#9DA7BA', textAlign: 'center', lineHeight: '1.3', marginBottom: 18 }}>
            The Quant Tournament has returned, and registration is open globally with a prize pool of
            <a
              style={{
                color: 'white',
                fontSize: 15,
                fontWeight: 550
              }}
            >
              {' '}
              100 USDT
            </a>
          </div>
          <div style={{ height: maxSm ? 140 : 197, width: maxSm ? 247 : 346 }}>
            <img src={imageBackgroundImgHome} style={{ width: '100%', objectFit: 'cover' }}></img>
          </div>
        </AppModal>
      </Container>
    </div>
  )
}

export default Home
