import React, { useState, useRef, useEffect } from 'react'
import Modal from 'react-bootstrap/Modal'
import 'bootstrap/dist/css/bootstrap.min.css'
import DropDownComponent from '@src/components/DropDown'
import AppButton from '@src/components/AppButton'
import TableRsuite from '../components/TableRsuite'
import { getRouteList } from '../routeService'

const RoutesListScreen = () => {
  const [isShowViewDemoModal, setIsShowViewDemoModal] = useState(false)
  const viewDemoModalBodyRef = useRef(null)
  const [isShowSummitBtn, setIsShowSubmitBtn] = useState(false)
  const [dataRote, setDataRote] = useState([])

  const [currentStateEnd, setCurrentStateEnd] = useState({ id: 1, text: 'Hà Nội' })
  const [endDateTime, setEndDateTime] = useState('')

  const handleEndDateTimeChange = (event) => {
    setEndDateTime(event.target.value)
  }

  useEffect(() => {
    getRouteList().then((data) => {
          setDataRote(data)
    })
  }, [])

  const listEnd = [
    { id: 0, value: '1' },
    { id: 1, value: '2' },
    { id: 2, value: '3' },
    { id: 3, value: '4' },
    { id: 4, value: '5' }
  ]


  const listStatus = [
    { id: 0, value: 'True' },
    { id: 1, value: 'Flase' }
  ]
  const handleValueChange = () => {
    console.log('Value changed')
  }

  const handleClickOpenViewDemoModal = () => {
    setIsShowViewDemoModal(true)
  }

  const handleClickCloseViewDemoModal = () => {
    setIsShowViewDemoModal(false)
  }

  const handleScrollViewDemoModal = () => {
    const scrollTop = viewDemoModalBodyRef.current.scrollTop
    const scrollHeight = viewDemoModalBodyRef.current.scrollHeight
    const clientHeight = viewDemoModalBodyRef.current.clientHeight
    if (clientHeight + scrollTop > scrollHeight - 75) {
      setIsShowSubmitBtn(true)
    } else {
      setIsShowSubmitBtn(false)
    }
  }

  return (
    <>
      <div class=' text-center'>
        <div className='row'>
          <h1 style={{ color: 'White', marginTop: '5%' }}>Routes Management</h1>
          <div className='col-md-10' style={{ marginTop: '6%', marginBottom: '15%' }}>
            <div style={{ marginBottom: '20px', width: '500px' }}>
              <input type='text' className='form-control' placeholder='Search...' style={{ width: '100%' }} />
            </div>
            <div className='tableCompany'>
              {/* <table className='table table-striped table-hover'>
                <thead className='thead-dark table-primary'>
                  <tr>
                    <th scope='col'>Routes ID</th>
                    <th scope='col'>Company Name</th>
                    <th scope='col'>Location</th>
                    <th scope='col'>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope='row'>1</th>
                    <td>Sao Viet</td>
                    <td>Lao Cai</td>
                    <td>Active</td>
                  </tr>
                  <tr>
                  <th scope='row'>2</th>
                    <td>Nha Nam</td>
                    <td>Bac Giang</td>
                    <td>Active</td>
                  </tr>
                  <tr>
                  <th scope='row'>3</th>
                    <td>Binh An</td>
                    <td>Hoa Binh</td>
                    <td>Active</td>
                  </tr>
                  <tr>
                  <th scope='row'>4</th>
                    <td>Ha Son</td>
                    <td>Dien Bien</td>
                    <td>Active</td>
                  </tr>
                  <tr>
                  <th scope='row'>5</th>
                    <td>Kumo Viet Thanh</td>
                    <td>Quang Ninh</td>
                    <td>Active</td>
                  </tr>
                </tbody>
              </table> */}
              <TableRsuite dataTable = {dataRote}/>
            </div>
          </div>
          <div class='col-md-2' style={{ marginTop: '6%', marginBottom: '15%' }}>
            {/* các nút chức năng */}
            <div style={{ padding: '20px' }}>
            <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: '10px',
        width: '200px',
        marginTop: '20%'
      }}
    >
      <AppButton
        style={{ backgroundColor: 'green', color: 'white' }}
        onClick={handleClickOpenViewDemoModal}
      >
        Add Routes
      </AppButton>
      <AppButton
        style={{ backgroundColor: 'orange', color: 'black' }}
        onClick={handleClickOpenViewDemoModal}
      >
        Edit Routes
      </AppButton>
      <AppButton
        style={{ backgroundColor: 'red', color: 'white' }}
      >
        Delete Routes
      </AppButton>
    </div>

              {isShowViewDemoModal && (
                <Modal
                  size='md'
                  aria-labelledby='contained-modal-title-vcenter'
                  centered
                  className='my-modal'
                  show={isShowViewDemoModal}
                  onHide={handleClickCloseViewDemoModal}
                >
                  <Modal.Header style={{ borderBottomWidth: 0, position: 'relative' }}>
                    <div onClick={handleClickCloseViewDemoModal} style={{ cursor: 'pointer' }}>
                      <i style={{ fontSize: 24 }} className='fa-solid fa-angle-left'></i>
                    </div>
                    <Modal.Title style={{ margin: '0 auto' }} id='contained-modal-title-vcenter'>
                      Routes
                    </Modal.Title>
                    <div onClick={handleClickCloseViewDemoModal} style={{ cursor: 'pointer' }}>
                      <i style={{ fontSize: 24 }} className='fa-solid fa-xmark'></i>
                    </div>
                  </Modal.Header>
                  <Modal.Body
                    ref={viewDemoModalBodyRef}
                    onScroll={handleScrollViewDemoModal}
                    className='hideScrollBar'
                    style={{
                      border: 0,
                      backgroundColor: '#181F38',
                      color: '#fff',
                      maxHeight: 'calc(100vh - 210px)',
                      overflowY: 'auto'
                    }}
                  >
                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginBottom: 20
                      }}
                    >
                      <div style={{ width: '96%' }}>
                        <div style={{ fontWeight: 600, marginBottom: 6 }}>
                        Company Name<span style={{ color: '#FF7373' }}>*</span>
                        </div>
                        <DropDownComponent
                          listElement={listEnd}
                          width={'300px'}
                          currentState={currentStateEnd}
                          setCurrentState={setCurrentStateEnd}
                          height='50px'
                          CallFnWhenChangeValue={handleValueChange}
                        />
                      </div>
                      
                    </div>

                    
                    

                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginBottom: 20
                      }}
                    >
                      <div style={{ width: '96%' }}>
                        <div style={{ fontWeight: 600, marginBottom: 6 }}>
                          Name Routes<span style={{ color: '#FF7373' }}>*</span>
                        </div>
                        <input
                          placeholder='Enter your Name'
                          style={{
                            border: 'none',
                            paddingTop: 6,
                            paddingBottom: 6,
                            paddingLeft: 10,
                            background: '#29384E',
                            borderRadius: 10,
                            width: '100%',
                            color: 'white'
                          }}
                        />
                      </div>
                    </div>

                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginBottom: 20
                      }}
                    >
                      <div style={{ width: '96%' }}>
                        <div style={{ fontWeight: 600, marginBottom: 6 }}>
                          Location<span style={{ color: '#FF7373' }}>*</span>
                        </div>
                        <input
                          placeholder='Enter your Location'
                          style={{
                            border: 'none',
                            paddingTop: 6,
                            paddingBottom: 6,
                            paddingLeft: 10,
                            background: '#29384E',
                            borderRadius: 10,
                            width: '100%',
                            color: 'white'
                          }}
                        />
                      </div>
                    </div>
                   



                    <div
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        marginBottom: 20
                      }}
                    >
                      <div style={{ width: '96%' }}>
                        <div style={{ fontWeight: 600, marginBottom: 6 }}>
                        Status<span style={{ color: '#FF7373' }}>*</span>
                        </div>
                        <DropDownComponent
                          listElement={listStatus}
                          width={'300px'}
                          currentState={currentStateEnd}
                          setCurrentState={setCurrentStateEnd}
                          height='50px'
                          CallFnWhenChangeValue={handleValueChange}
                        />
                      </div>
                      
                    </div>


                  </Modal.Body>
                  <div style={{ display: 'flex', justifyContent: 'space-between', margin: 20 }}>
                    <div
                      onClick={handleClickCloseViewDemoModal}
                      style={{
                        cursor: 'pointer',
                        width: '48%',
                        paddingTop: 6,
                        paddingBottom: 6,
                        background: '#2F4C78',
                        textAlign: 'center',
                        borderRadius: 10,
                        fontSize: 18,
                        fontWeight: 600
                      }}
                    >
                      Cancel
                    </div>
                    <div
                      style={{
                        color: isShowSummitBtn ? 'white' : '#455066',
                        cursor: isShowSummitBtn ? 'pointer' : 'not-allowed',
                        width: '48%',
                        paddingTop: 6,
                        paddingBottom: 6,
                        background: isShowSummitBtn ? '#5768DE' : '#29384E',
                        textAlign: 'center',
                        borderRadius: 10,
                        fontSize: 18,
                        fontWeight: 600
                      }}
                    >
                      Submit
                    </div>
                  </div>
                </Modal>
              )}
            </div>
          </div>
        </div>
      </div>

    </>
  )
}

export default RoutesListScreen
