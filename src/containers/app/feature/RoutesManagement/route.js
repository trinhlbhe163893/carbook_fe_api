import AppLayout from '@src/components/Layouts/AppLayout'
import { Outlet } from 'react-router'
import Route from './page'

export const routeRouteList = [
  {
    path: '/route',
    element: (
      <AppLayout>
        <Route />
      </AppLayout>
    )
  }
]
