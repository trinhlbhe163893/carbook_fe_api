import axios from '@src/axios/myAxios'
const getRouteList = () => {
    return axios.post(`api/schedule/listschedule`)
}

export {
    getRouteList
}