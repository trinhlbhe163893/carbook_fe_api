/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react'
import AppButton from '@src/components/AppButton'
import './style.scss'
import DropDownComponent from '@src/components/DropDown'
import DateTimePicker from '../DateTimePicker'
import Banner from '../../../../../../assets/images/banner_1.png'
import { useNavigate } from 'react-router'
import { getStartLocation } from '../../homePageService'
import { getEndLocation } from '../../homePageService'
import { id } from 'ethers/lib/utils'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { Hidden } from '@mui/material'

function Center() {
  const [currentStateStart, setCurrentStateStart] = useState({ id: null, value: '' })
  const [currentStateEnd, setCurrentStateEnd] = useState({ id: null, value: '' })
  const [startDateTime, setStartDateTime] = useState('')
  const [endDateTime, setEndDateTime] = useState('')

  const [startLocation, setStartLocation] = useState([])
  const [endLocation, setEndLocation] = useState([])
  // const [onChoose, setOnChoose] = useState(false)
  // const [arrayData, setArrayData] = useState()
  const navigate = useNavigate()

  const maxSm = useMediaQuery('(max-width: 992px)')

  //api
  useEffect(() => {
    getStartLocation().then((data) => {
      const startData = data.map((item) => ({
        id: item.id,
        value: item.value
      }))
      setStartLocation(startData)
    })

    getEndLocation().then((data) => {
      const endData = data.map((item) => ({
        id: item,
        value: item.value
      }))
      setEndLocation(endData)
    })
  }, [])

  const handleValueChange = () => {
    console.log(currentStateEnd)
    console.log(currentStateStart)
  }

  const handleStartDateTimeChange = (event) => {
    setStartDateTime(event.target.value)
  }

  const handleEndDateTimeChange = (event) => {
    setEndDateTime(event.target.value)
  }

  const handleNavigation = () => {
    // Lấy thông tin từ state và thực hiện xử lý
    const startLocation = currentStateStart['value']
    const endLocation = currentStateEnd['value']
    const startTime = startDateTime
    const endTime = endDateTime.split('T')[0]

    // Bạn có thể thực hiện các xử lý hoặc điều hướng tại đây
    console.log('Điểm khởi hành:', startLocation)
    console.log('Điểm đến:', endLocation)
    console.log('Thời gian khởi hành:', startTime)
    console.log('Thời gian kết thúc:', endTime)

    var path = `/booking?startLocation=${startLocation}&endLocation=${endLocation}&dateTime=${endTime}`
    navigate(path)
  }

  return (
    <div>
      <div
        className={maxSm ? 'topLayoutMobile' : ' topLayout'}
        style={
          maxSm
            ? { display: 'flex', justifyContent: 'right', backgroundColor: '#cccbc9', backgroundSize: 'cover' }
            : { display: 'flex', justifyContent: 'right', backgroundImage: `url(${Banner})`, backgroundSize: 'cover' }
        }
      >
        {/* <img className='banner' src={Banner} /> */}
        <div className='intro-ticket' style={{ width: 1000 }}>
          <div style={{ textAlign: 'start', fontSize: 40, color: 'white', fontWeight: 700 }}>ĐẶT VÉ XE KHÁCH</div>
          <div style={{ color: 'black', fontSize: 18, textAlign: 'start' }}>
            An toàn - Chất lượng - Tin cậy - Trung thực
          </div>
          <div
            style={
              maxSm
                ? { background: '#323232B3', borderRadius: 15, padding: 15, width: 350 }
                : { background: '#323232B3', borderRadius: 15, padding: 15, width: 700 }
            }
          >
            <div className='row-input'>
              <AppButton>Xe Liên Tỉnh</AppButton>
            </div>
            <div style={{ display: 'flex', flexDirection: 'column', gap: 20, marginBottom: 20 }}>
              <div style={{ display: 'flex', gap: 20 }}>
                <div style={{ flex: 1 }}>
                  <div style={{ fontWeight: 600, marginBottom: 6, color: 'white', fontSize: 20 }}>
                    Điểm khởi hành<span style={{ color: 'red' }}>*</span>
                  </div>
                  <DropDownComponent
                    listElement={startLocation}
                    width={'200px'}
                    currentState={currentStateStart}
                    setCurrentState={setCurrentStateStart}
                    height='50px'
                    CallFnWhenChangeValue={handleValueChange}
                  />
                </div>
                <div style={{ flex: 1 }}>
                  <div style={{ fontWeight: 600, color: 'white', marginBottom: 6, fontSize: 20 }}>
                    Điểm đến<span style={{ color: 'red' }}>*</span>
                  </div>
                  <DropDownComponent
                    listElement={endLocation}
                    width={'200px'}
                    currentState={currentStateEnd}
                    setCurrentState={setCurrentStateEnd}
                    height='50px'
                    CallFnWhenChangeValue={handleValueChange}
                  />
                </div>
              </div>
              <div style={{ gap: 20, width: '48%' }}>
                <div style={{ flex: 1 }}>
                  {/* <div style={{ fontWeight: 600, marginBottom: 6, color: 'white' }}>
                                        Thời gian khởi hành<span style={{ color: 'white' }}>*</span>
                                    </div> */}
                </div>
                <DateTimePicker label='Thời gian kết thúc' value={endDateTime} onChange={handleEndDateTimeChange} />
              </div>
            </div>
            <AppButton
              style={{
                width: '100%'
              }}
              onClick={handleNavigation}
            >
              Tìm xe
            </AppButton>
          </div>
        </div>
      </div>
      {/* ======================Center 2============================== */}
      <div className='center-2'>
        <div className='introduct-travel'>
          <div className='content'>
            <div className='item'>
              <img src='https://futahason.com/Image/Trang_chu/icon_phone1.png' />
              <span className='title'>Đặt vé dễ dàng</span>
              <span className='sub-title'>Thao tác đơn giản, đặt vé giữ chỗ nhanh chóng chỉ 30s</span>
            </div>
            <div className='item'>
              <img src='https://futahason.com/Image/Trang_chu/icon_phone3.png' />
              <span className='title'>Đa dạng dịch vụ</span>
              <span className='sub-title'>Hạng thấp đến cao, vé nào cũng có. Dòng xe đa dạng phục vụ mọi nhu cầu</span>
            </div>
            <div className='item'>
              <img src='https://futahason.com/Image/Trang_chu/icon_phone2.png' />
              <span className='title'>Chất lượng dịch vụ</span>
              <span className='sub-title'>Dịch vụ chuyên nghiệp,phục vụ chu đáo, nhân sự tận tâm</span>
            </div>
          </div>
        </div>
      </div>
      {/* =====================Center 3=============================== */}
      <div className='center-3'>
        <div className='population-travel'>
          <div className='content'>
            <div className='item'>
              <img src='https://Futahason.vn//Image/Banner//Sa Pa.jpg' />
              <span className='title-population-travel'>Sapa - Mỹ Đình</span>
              <span className='sub-title-population-travel'>Giá từ 310.000đ - 420.000đ</span>
            </div>
            <div className='item'>
              <img src='https://futahason.vn//Image/Banner//H%C3%ACnh%20%E1%BA%A3nh%20website%20(1).jpg' />
              <span className='title-population-travel'>Sapa - Bắc Giang</span>
              <span className='sub-title-population-travel'>Giá từ 310.000đ - 420.000đ</span>
            </div>
            <div className='item'>
              <img src='https://futahason.vn//Image/Banner//L%C3%A0o%20Cai%20-%20M%C4%90.jpg' />
              <span className='title-population-travel'>Bắc Hà - Bắc Giang</span>
              <span className='sub-title-population-travel'>Giá từ 310.000đ - 420.000đ</span>
            </div>
            <div className='item'>
              <img src='https://futahason.vn//Image/Banner//Th%C3%A0nh%20c%E1%BB%95%20S%C6%A1n%20T%C3%A2y.jpg' />
              <span className='title-population-travel'>Lào Cai - Sơn Tây</span>
              <span className='sub-title-population-travel'>Giá từ 310.000đ - 420.000đ</span>
            </div>
          </div>
        </div>
      </div>
      {/* ===========================Center 4============================= */}
      <div className='introduce'>
        <div className='intro'>
          <ul className='col-title'>
            <span className='introduce-title'>Về chúng tôi</span>
            <li>Giới thiệu chung</li>
            <li>Giá trị cốt lõi</li>
            <li>Sứ mệnh và tầm nhìn</li>
            <li>Văn hóa doanh nghiệp</li>
            <li>Tin tức & Sự kiện</li>
          </ul>
          <ul className='col-title'>
            <span className='introduce-title'>Hỗ trợ</span>
            <li>Hướng dẫn đặt vé</li>
            <li>Quản lý đặt chỗ</li>
            <li>Câu hỏi thường gặp</li>
            <li>Quy định về bảo mật</li>
          </ul>
          <ul className='col-title'>
            <span className='introduce-title'>Tuyến phổ biến</span>
            <li>Lào Cai - Mỹ Đình</li>
            <li>Lào Cai - Gia Lâm</li>
            <li>Lào Cai - Yên Nghĩa</li>
            <li>Lào Cai -Sơn Tây</li>
            <li>Sapa - Mỹ Đình</li>
            <li>Sapa - Gia Lâm</li>
            <li>Sapa - Yên Nghĩa</li>
          </ul>
          <ul className='col-title-2'>
            <span className='introduce-title'>Đối tác</span>
            <li>
              <img
                style={{ width: '50%' }}
                src='https://futahason.vn//Image/Banner//Logo-VNPAY-QR%20-50x139px%20(n%E1%BB%81n%20tr%E1%BA%AFng).jpg'
              />
            </li>
            <span className='introduce-title'>Thành viên</span>
          </ul>
        </div>
      </div>
    </div>
  )
}
export default Center
