/* eslint-disable prettier/prettier */
import React from 'react';
import './style.scss'

const DateTimePicker = ({ label, value, onChange }) => {
    return (
        <div style={{ marginBottom: 20 }}>
            <div style={{ fontWeight: 600, marginBottom: 6, color: 'white' , fontSize: 20}}>
                {label}<span style={{ color: 'red' }}>*</span>
            </div>
            <input
                className='input-datetime'
                type="datetime-local"
                value={value}
                onChange={onChange}
            />
        </div>
    );
};

export default DateTimePicker;
