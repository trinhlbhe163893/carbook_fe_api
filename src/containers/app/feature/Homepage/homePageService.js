import axios from '@src/axios/myAxios'

const getStartLocation = () => {
  return axios.get('api/Ticket/startlocation')
}

const getEndLocation = () => {
  return axios.get('api/Ticket/endlocation')
}

export { getStartLocation, getEndLocation }
