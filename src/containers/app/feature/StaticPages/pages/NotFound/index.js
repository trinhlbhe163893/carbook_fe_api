// import notfound_bg from '@src/assets/images/notfound_bg.png'
import classNames from 'classnames/bind'
import styles from './NotFound.module.sass'
import useMediaQuery from '@src/hooks/useMediaQuery'
import imageBackgroundImgHome from '@src/assets/images/bgr_leaderboard.png'
import { Bg1, Bg2 } from '@src/assets/svgs'
const cx = classNames.bind(styles)

function NotFound() {
  const maxSmSize = useMediaQuery('(max-width: 768px)')

  return (
    <div className={cx('not-found-wrapper')} style={{ backgroundImage: `url(${imageBackgroundImgHome})` }}>
      {maxSmSize ? (
        <div>
          <Bg2 />
        </div>
      ) : (
        <div>
          <Bg1 />
        </div>
      )}
    </div>
  )
}

export default NotFound
