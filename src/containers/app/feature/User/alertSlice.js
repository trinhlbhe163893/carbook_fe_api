import { createSlice } from '@reduxjs/toolkit'

const alertSlice = createSlice({
  name: 'alert',
  initialState: { type: null, message: null, url: null },
  reducers: {
    setAlert: (state, action) => {
      state.type = action.payload.type
      state.message = action.payload.message
      state.url = action.payload.url
    },
    clearAlert: (state) => {
      state.type = null
      state.message = null
      state.url = null
    }
  }
})

export const { setAlert, clearAlert } = alertSlice.actions
export default alertSlice.reducer
