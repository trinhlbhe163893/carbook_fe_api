/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import analyze from '@src/assets/images/analyze.png'
import { CloseIcon, FileIcon } from '@src/assets/svgs'
import AppButton from '@src/components/AppButton'
import AppModal from '@src/components/AppModal/AppModal'
import { dataSelection } from '@src/configs'
import { isEmptyValue } from '@src/helpers/check'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { Col, Row } from 'antd'
import classNames from 'classnames/bind'
import moment from 'moment'
import { Fragment, useEffect, useRef, useState } from 'react'
import ReactLoading from 'react-loading'
import { Bar, CartesianGrid, ComposedChart, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts'
import { userApi } from '../../userService'
import styles from './Analyze.module.sass'
import { getDataChart } from '@src/axios/getApiAll'
import Chartc from '../../components/Chart'
import './Analyze.scss'

const cx = classNames.bind(styles)
const competitionId = process.env.COMPETITION_ID

function Analyze({ data, currentRound, setIsOpenSubmitModelInDetailModelAnalyze }) {
  const closeModal = useRef()
  const [filter, setFilter] = useState('Movement Score')
  const [timeId, setTimeId] = useState()
  const [model, setModel] = useState()
  const maxSm = useMediaQuery('(max-width: 992px)')
  const [getModelPerformance, { data: modelPerformance, isFetching: isGettingModelPerformance }] =
    userApi.endpoints.getModelPerformance.useLazyQuery()

  const [dataLastModelDetailsChart, setDataLastModelDetailsChart] = useState([])
  const [averageMovementScore, setAverageMovementScore] = useState(0)
  const [averageCORR15, setAverageCORR15] = useState(0)
  const [averageMS15, setAverageMS15] = useState(0)

  const [loadingChart, setLoadingChart] = useState(false)

  const submissionCount = data?.length
  useEffect(() => {
    if (!isEmptyValue(data) && currentRound)
      getModelPerformance(
        {
          path: { competitionId: competitionId, submission: currentRound['Current round'] },
          params: { submission_time: timeId ? timeId : data[0]['Submission Time'] }
        },
        false
      )
  }, [data, timeId])

  const handleFilter = (name) => {
    setFilter(name)
  }
  const handleDataChart = (type, symbol, model, time) => {
    setAverageMovementScore(0)
    setAverageCORR15(0)
    setAverageMS15(0)
    setDataLastModelDetailsChart([])
    setModel()

    var listCorr15 = []
    var listMs15 = []

    var averageMovementScore = 0
    var averageCORR15 = 0
    var averageMS15 = 0
    var count = 0

    var dataLastModelDetailsChart = []
    getDataChart({
      tournament_search_id: 'return_and_risk_tournament',
      submission_type: type,
      symbol: symbol,
      model_id: model,
      submission_time: time
    })
      .then((data) => {
        var keys = Object.keys(data)
        keys.map((item2) => {
          var keys2 = Object.keys(data[item2])
          var listModel = Object.keys(data[item2][keys2])
          listModel.map((item3) => {
            var listSubmit = Object.keys(data[item2][keys2][item3])
            listSubmit.map((item4) => {
              var four = Object.keys(data[item2][keys2][item3][item4].Data)
              four.map((item5) => {
                var five = data[item2][keys2][item3][item4].Data[item5]
                averageMovementScore += five[1]
                averageCORR15 += five[2]
                averageMS15 += five[3]
                count++
                var corr15 = { x: five[0], y: five[2] ? five[2].toFixed(4) : five[2] }
                var ms15 = { x: five[0], y: five[3] ? five[3].toFixed(4) : five[3] }
                listCorr15.push(corr15)
                listMs15.push(ms15)
              })
            })
          })
        })
        var dataChart = {}
        dataChart = { name: 'CORR15', type: 'line', data: listCorr15 }
        dataLastModelDetailsChart.push(dataChart)
        dataChart = { name: 'MS15', type: 'line', data: listMs15 }
        dataLastModelDetailsChart.push(dataChart)
        setAverageMovementScore((averageMovementScore / count).toFixed(4))
        setAverageCORR15((averageCORR15 / count).toFixed(4))
        setAverageMS15((averageMS15 / count).toFixed(4))
        setDataLastModelDetailsChart(dataLastModelDetailsChart)
        setModel(model)
        setLoadingChart(false)
      })
      .catch((error) => {
        console.log('DataChartError: ' + error)
        setLoadingChart(false)
      })
  }
  const tickFormatter = (unixTime) => {
    return moment(unixTime).format('MM/DD/YYYY')
  }
  // console.log("DATA: "+JSON.stringify(data))
  return (
    <div className={cx('analyze')}>
      <div className={cx('content')}>
        <img src={analyze} alt='analyzing' />
        <div className={cx('text')}>
          <div className={cx('top')}>Deep Analysis</div>
          <p className={cx('bottom')}>
            Backtest and analyze the model outcomes from the previous round and backtesting process.
          </p>
        </div>
      </div>
      <div className={cx('btn')}>
        <AppModal
          ref={closeModal}
          width={maxSm ? '90%' : '80%'}
          height={maxSm ? '90%' : 'auto'}
          triggerBtn={
            <AppButton
              style={{ width: '100%', background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
            >
              Backtest now
            </AppButton>
          }
          contentStyle={{
            borderRadius: '20px',
            padding: '24px',
            border: 'none',
            backgroundColor: '#29384E'
          }}
        >
          <div className={cx('content-wrapper')}>
            <div className={cx('heading')}>
              <h4 className={cx('text')}>Analyzing model</h4>
              <div onClick={() => closeModal.current.click()} className={cx('close')}>
                <CloseIcon />
              </div>
            </div>
            <Row gutter={[24, 24]}>
              <Col lg={7} md={24} sm={24} xs={24}>
                <div className={cx('history')}>
                  <div style={{ display: 'flex', justifyContent: 'space-between' }} className={cx('heading')}>
                    <div style={{ display: 'flex' }}>
                      <FileIcon />
                      <p style={{ marginLeft: 6 }}>Submitted</p>
                    </div>
                    <AppButton
                      className='btnWidthResponsive'
                      onClick={() => setIsOpenSubmitModelInDetailModelAnalyze(true)}
                      style={{
                        width: '21%',
                        height: '80%',
                        background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)',
                        fontSize: 14
                      }}
                    >
                      Add
                      <svg
                        style={{ cursor: 'pointer', marginLeft: 2 }}
                        width='20'
                        height='20'
                        viewBox='0 0 24 24'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          d='M13 5C13 4.44772 12.5523 4 12 4C11.4477 4 11 4.44772 11 5V11H5C4.44772 11 4 11.4477 4 12C4 12.5523 4.44772 13 5 13H11V19C11 19.5523 11.4477 20 12 20C12.5523 20 13 19.5523 13 19V13H19C19.5523 13 20 12.5523 20 12C20 11.4477 19.5523 11 19 11H13V5Z'
                          fill='white'
                        />
                      </svg>
                    </AppButton>
                  </div>

                  <div className={cx('submit-list')}>
                    {data
                      ?.filter((i) => i['type'] == 'back_test')
                      .map((item, index) => {
                        return (
                          <Fragment key={item['Submission Time']}>
                            <div
                              onClick={() => {
                                setLoadingChart(true)
                                handleDataChart(item['type'], item['symbol'], item['model'], item['submitTime'])
                              }}
                              className={cx(timeId === item['Submission Time'] ? 'submit-item--active' : 'submit-item')}
                            >
                              <div className={cx('__left')}>{item['model']}</div>
                              <div className={cx('__right')}>
                                <p>{moment.unix(item['submitTime'] / 1000).format('MM/DD/YYYY')}</p>
                                <p>{moment.unix(item['submitTime'] / 1000).format('HH:mm:ss')}</p>
                              </div>
                            </div>
                            {submissionCount !== index + 1 && <div className={cx('divider')}></div>}
                          </Fragment>
                        )
                      })}
                  </div>
                </div>
              </Col>
              <Col lg={17} md={24} sm={24} xs={24}>
                <div className={cx('score-wrapper')}>
                  <Row gutter={[12, 12]}>
                    <Col span={8}>
                      <div className={cx('score')}>
                        <div className={cx('name')}>Movement score</div>
                        {isGettingModelPerformance ? (
                          <div className={cx('data-notify')}>
                            <ReactLoading type='bubbles' height={32} width={32} />
                          </div>
                        ) : averageMovementScore != 0 ? (
                          <div className={cx('value')}>{Number(averageMovementScore)}</div>
                        ) : (
                          <div className={cx('data-notify')}>No data</div>
                        )}
                      </div>
                    </Col>
                    <Col span={8}>
                      <div className={cx('score')}>
                        <div className={cx('name')}>CORR15</div>
                        {isGettingModelPerformance ? (
                          <div className={cx('data-notify')}>
                            <ReactLoading type='bubbles' height={32} width={32} />
                          </div>
                        ) : averageCORR15 != 0 ? (
                          <div className={cx('value')}>{Number(averageCORR15)}</div>
                        ) : (
                          <div className={cx('data-notify')}>No data</div>
                        )}
                      </div>
                    </Col>
                    <Col span={8}>
                      <div className={cx('score')}>
                        <div className={cx('name')}>MS15</div>
                        {isGettingModelPerformance ? (
                          <div className={cx('data-notify')}>
                            <ReactLoading type='bubbles' height={32} width={32} />
                          </div>
                        ) : averageMS15 != 0 ? (
                          <div className={cx('value')}>{Number(averageMS15)}</div>
                        ) : (
                          <div className={cx('data-notify')}>No data</div>
                        )}
                      </div>
                    </Col>
                  </Row>
                </div>
                <div className={cx('chart')}>
                  {/* <div className={cx('filter')}>
                    <div
                      onClick={() => handleFilter('Movement Score')}
                      className={cx(filter === 'Movement Score' ? 'item-active' : 'item')}
                    >
                      Movement Score
                    </div>
                    <div
                      onClick={() => handleFilter('Correlation')}
                      className={cx(filter === 'Correlation' ? 'item-active' : 'item')}
                    >
                      Correlation
                    </div>
                    <div
                      onClick={() => handleFilter('True Contribution')}
                      className={cx(filter === 'True Contribution' ? 'item-active' : 'item')}
                    >
                      True Contribution
                    </div>
                  </div> */}
                  {isGettingModelPerformance ? (
                    <div style={{ height: '250px' }} className={cx('data-notify')}>
                      <ReactLoading type='bubbles' height={32} width={32} />
                    </div>
                  ) : modelPerformance ? (
                    <div style={{ height: '250px' }}>
                      <ResponsiveContainer width='100%' height={250}>
                        <ComposedChart height={250} data={modelPerformance[filter]}>
                          <XAxis dataKey='DAY' tickFormatter={tickFormatter} />
                          <YAxis />
                          <Tooltip
                            labelFormatter={(value) => `Day ${moment.unix(value / 1000).format('MM/DD/YYYY')}`}
                          />
                          <Legend />
                          <CartesianGrid stroke='#29384E' vertical={false} strokeDasharray='3 3' />
                          <Bar dataKey={dataSelection[filter]} barSize={20} fill='#fff' />
                        </ComposedChart>
                      </ResponsiveContainer>
                    </div>
                  ) : (
                    // <div style={{ height: '250px' }} className={cx('data-notify')}>
                    //   No data
                    // </div>
                    <div
                      className='chartContainer'
                      style={{
                        width: '100%',
                        background: '#181F38',
                        padding: 24,
                        borderRadius: 20,
                        marginBottom: 30
                      }}
                    >
                      <div style={{ fontSize: 20, fontWeight: 500, color: '#fff' }}>Score</div>
                      <div style={{ fontSize: 17, fontWeight: 500, color: '#3953DC', marginTop: 10, marginBottom: 10 }}>
                        Model: {model}
                      </div>
                      {loadingChart ? (
                        <div style={{ height: 250, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                          <ReactLoading type='spin' height={32} width={32} />
                        </div>
                      ) : (
                        <Chartc data={dataLastModelDetailsChart} />
                      )}
                      {/* Chart Chart */}
                    </div>
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </AppModal>
      </div>
    </div>
  )
}

export default Analyze
