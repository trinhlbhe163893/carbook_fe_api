/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { Link } from 'react-router-dom'
import { Toaster, toast } from 'react-hot-toast'
import moment from 'moment'
import classNames from 'classnames/bind'

import { userApi } from '../../userService'
import { isEmptyValue } from '@src/helpers/check'
import HistoryItem from '../HistoryItem'
import Status from '@src/components/Status'

import { ClockOpenIcon, LinkIcon } from '@src/assets/svgs'
import AppModal from '@src/components/AppModal/AppModal'
import { deleteSubmission } from '@src/axios/getApiAll'
import AppButton from '@src/components/AppButton'
// import { Toaster, toast } from 'react-hot-toast'
// import { Toaster, toast } from 'react-hot-toast'

import { SUBMISSION_TYPE } from '@src/configs'
import styles from './History.module.sass'
import { useState } from 'react'

const cx = classNames.bind(styles)

function History({
  isLoading,
  reloadData,
  data,
  currentRound,
  setIsShowHistory,
  listModalSubmission,
  setLoadListModel
}) {
  const length = listModalSubmission?.length
  const [itemDelete, setItemDelete] = useState()
  const [isShowModalDelete, setIsShowModelDelete] = useState(false)
  const [loadingDelete, setLoadingDelete] = useState(false)
  // const [deleteSubmission, { isLoading: isDeleting }] = userApi.endpoints.deleteSubmission.useMutation()

  // const handleDelete = async (data) => {
  //   if (currentRound) {
  //     const response = await deleteSubmission({
  //       path: {
  //         competitionId: process.env.COMPETITION_ID,
  //         submission: currentRound['Current round'],
  //         submission_time: data['Submission Time']
  //       }
  //     })

  //     if (response.error) {
  //       toast.error(response.error.data.detail || 'An error occurred')
  //     } else {
  //       toast.success('Delete successfully!')
  //       reloadData()
  //     }
  //   }
  // }
  const handelDeleteModel = (item) => {
    // console.log('item::'+JSON.stringify(item))
    setItemDelete(item)
    setIsShowModelDelete(true)
  }
  const performDelete = () => {
    //{"type":"live","symbol":"BTCUSDT","model":"abcdeeeee","submitTime":1698428565000}
    //`${data['tournamentId']}/submission?submission_type=${data['submissionType']}&symbol=${data['symbol']}&model_id=${data['model']}`, {
    setLoadingDelete(true)
    deleteSubmission({
      tournamentId: 'return_and_risk_tournament',
      submissionType: itemDelete['type'],
      symbol: itemDelete['symbol'],
      model: itemDelete['model']
    })
      .then((d) => {
        setLoadingDelete(false)
        toast.success('Delete model successfully...')
      })
      .catch((error) => {
        setLoadingDelete(false)
        toast.error('Delete model failed...')
      })
      .finally(() => {
        setIsShowModelDelete(false)
        setLoadListModel(true)
        setLoadingDelete(false)
      })
  }
  var listPrint = listModalSubmission
    .sort((x, y) => -(x.submitTime - y.submitTime))
    .map((it, index) => {
      return { ...it, index: index }
    })
    .slice(0, 2)
  return (
    <div className={cx('wrapper')}>
      {/* <Toaster /> */}
      <div style={{ display: 'flex', justifyContent: 'space-between' }} className={cx('heading')}>
        <div className={cx('text')}>Submission history</div>
        {/* do not delete  */}
        {/* history */}
        {/* <div 
          onClick={() => setIsShowHistory(true)}
          style={{fontSize: 14, fontWeight: 500, color: '#6270E0', cursor: 'pointer'}} className={cx('text')}>View all 
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path fillRule="evenodd" clipRule="evenodd" d="M6.70233 16.0059C6.37689 15.6805 6.37689 15.1528 6.70233 14.8274L11.5297 10L6.70233 5.17259C6.37689 4.84715 6.37689 4.31951 6.70233 3.99408C7.02777 3.66864 7.5554 3.66864 7.88084 3.99408L12.7083 8.82149C13.3591 9.47236 13.3591 10.5276 12.7083 11.1785L7.88084 16.0059C7.5554 16.3314 7.02777 16.3314 6.70233 16.0059Z" fill="url(#paint0_linear_3636_12335)"/>
          <defs>
          <linearGradient id="paint0_linear_3636_12335" x1="7.46898" y1="15.8824" x2="11.1998" y2="-3.04801" gradientUnits="userSpaceOnUse">
          <stop stopColor="#3953DC"/>
          <stop offset="1" stopColor="#9C9BE6"/>
          </linearGradient>
          </defs>
          </svg>
        </div> */}

        {/* <Link className={cx('link')} to='/'>
          <p>View all</p>
          <ChevronRight />
        </Link> */}
      </div>
      <div className={cx('divider')}></div>
      <div className={cx('list')}>
        {!isEmptyValue(listPrint) ? (
          listPrint?.map((item, index) => {
            return (
              <div key={item['submitTime']} className={cx('item')}>
                <div className={cx('heading')}>
                  <div className={cx('__left')}>
                    <div className={cx('order')}>{length - item['index']}</div>
                    <div className={cx('info')}>
                      <div className={cx('date')}>{moment.unix(item['submitTime'] / 1000).format('MM/DD/YYYY')}</div>
                      <div className={cx('name')}>{item['model']}</div>
                    </div>
                  </div>
                  <div onClick={() => handelDeleteModel(item)} className={cx('status')}>
                    <Status>Delete</Status>
                  </div>
                </div>
                <div className={cx('body')}>
                  <HistoryItem Icon={<ClockOpenIcon />} title='Submitted time'>
                    {moment.unix(item['submitTime'] / 1000).format('HH:mm:ss')}
                  </HistoryItem>
                  <HistoryItem Icon={<LinkIcon />} title='Symbol'>
                    {item['symbol']}
                  </HistoryItem>
                </div>
              </div>
            )
          })
        ) : (
          <div className={cx('data-notify')}>No data</div>
        )}
      </div>
      <AppModal
        width={500}
        height={150}
        contentStyle={{
          borderRadius: '16px',
          padding: '24px',
          border: 'none',
          backgroundColor: '#29384E'
        }}
        isOpen={isShowModalDelete}
      >
        <div
          style={{
            color: 'white',
            fontSize: 18,
            fontWeight: 550,
            textAlign: 'center',
            marginBottom: 30,
            marginTop: 10
          }}
        >
          Do you want to delete model {itemDelete ? itemDelete['model'] : ''} ?
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between', fontSize: 16, fontWeight: 600 }}>
          <div
            onClick={() => {
              setIsShowModelDelete(false)
            }}
            style={{
              cursor: 'pointer',
              width: '45%',
              paddingTop: 10,
              paddingBottom: 10,
              background: '#2F4C78',
              textAlign: 'center',
              borderRadius: 16,
              fontSize: 18,
              fontWeight: 600,
              color: 'white'
            }}
          >
            Cancel
          </div>
          <AppButton
            onClick={() => performDelete()}
            // onClick={modelType['id'] == 'Type 1' ? handleSubmitModel : handleSubmitModelType2}
            isLoading={loadingDelete}
            style={{
              cursor: 'pointer',
              width: '45%',
              paddingTop: 10,
              paddingBottom: 10,
              background: '#5768DE',
              textAlign: 'center',
              borderRadius: 16,
              fontSize: 18,
              fontWeight: 600,
              color: 'white'
            }}
          >
            Delete
          </AppButton>
          {/* <div
                      onClick={() => performDelete()}
                      style={{
                        cursor: 'pointer',
                        width: '45%',
                        paddingTop: 10,
                        paddingBottom: 10,
                        background: '#5768DE',
                        textAlign: 'center',
                        borderRadius: 16,
                        fontSize: 18,
                        fontWeight: 600,
                        color: 'white'
                      }}
                    >
                      Delete
                    </div> */}
        </div>
      </AppModal>
    </div>
  )
}

export default History
