/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { /*ClockCloseIcon, ClockOpenIcon, IconEdit, LinkIcon,*/ UploadImg } from '@src/assets/svgs'
import AppButton from '@src/components/AppButton'
import AppModal from '@src/components/AppModal/AppModal'
// import Status from '@src/components/Status'
// import { ROUND } from '@src/configs'
// import { isEmptyValue } from '@src/helpers/check'
import classNames from 'classnames/bind'
// import moment from 'moment/moment'
import { useEffect, useRef, useState } from 'react'
import { /*Toaster,*/ toast } from 'react-hot-toast'
// import ReactLoading from 'react-loading'
import { userApi } from '../../userService'
// import HistoryItem from '../HistoryItem'
import styles from './CurrentRound.module.sass'
import DropDownComponent from '../DropDown'
import { getTournamentAll, getSymbolByTournamentId, submitModelInput } from '../../../../../../../src/axios/getApiAll'
// import DateTimePicker from 'react-datetime-picker'
import 'react-datepicker/dist/react-datepicker.css'
// import DatePicker from 'react-datepicker'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { AddIcon, MinusIcon } from '@src/assets/svgs'
import { getNumberValidationRegex } from '@src/helpers/validator'
import { useDispatch, useSelector } from 'react-redux'
import { setArraySub } from './modelSlice'
import Hint from '@src/components/Hint'

const cx = classNames.bind(styles)

function CurrentRound({
  // isLoading,
  // currentRound,
  reloadData,
  // inputData,
  myModelSubmitted,
  startTime,
  endTime,
  roundNumber,
  listModel,
  setLoadListModel,
  setCurrentSymbol
}) {
  const dispatch = useDispatch()
  var arraySub = useSelector((state) => state.model.arraySub)
  const [file, setFile] = useState()
  const closeModal = useRef(null)
  const maxSm = useMediaQuery('(max-width: 768px)')
  const maxSmBig = useMediaQuery('(max-width: 992px)')

  const [submitModel, { isLoading: isSubmitting }] = userApi.endpoints.submitModel.useMutation()
  const [isSubmittingType2, setIsSubmittingType2] = useState(false)

  const handleSubmitModel = async () => {
    const data = new FormData()
    if (file) {
      data.append('file', file)
      const response = await submitModel({
        // url: `/tournament/${params['tournamentSearchId']}/submit/file?submission_type=${params['submissionType']}&symbol=${params['symbol']}&model_id=${params['model']}%201232141241241`,
        params: {
          submissionType: 'live',
          tournamentSearchId: tournamentSearchId['id'],
          symbol: symbol['id'],
          model: type == 'dropDown' ? currentModel['id'] : currentModelText
        },
        body: data
      })

      if (response?.error) {
        toast.error(response?.error?.data?.detail || `File submission error`)
      } else {
        toast.success('Round submitted successully!')
        setTimeout(() => {
          reloadData()
        }, 10000)
        closeModal.current.click()
        setFile('')
        setCurrentModelText('')
        setLoadListModel(true)
      }
    } else {
      toast.error('Please choose a file')
    }
  }

  const handleSubmitModelType2 = async () => {
    // console.log("STR: "+arraySub)
    // //Check error when time is over 7h 19h
    // var userChooseTime = new Date(date);
    // var h = hour['id'].split('h')
    // if (h) userChooseTime.setHours(h[0])

    // var now = new Date()

    // if (now.getTime() - userChooseTime.getTime() > 0) {
    //   toast.error('Time is not valid. Please try again', styles = { backgroundColor: '#2C120A', color: '#FF7373', borderColor: '#FF7373' })
    //   return
    // }

    var data = {
      submission_type: 'live',
      symbol: symbol['id'],
      model_id: type == 'dropDown' ? currentModel['id'] : currentModelText,
      data: [...arraySub.filter(item => item != null).map(item => {return {...item, min: parseFloat(item['min']), max: parseFloat(item['max']), center: parseFloat(item['center'])}})]
    }
    // console.log("DATAaaa: "+JSON.stringify(data))
    setIsSubmittingType2(true)
    submitModelInput(data)
      .then(() => {
        toast.success('Round submitted successully!')
        setIsSubmittingType2(false)
        setType('dropDown')
        setCurrentModelText('')
        setResetStatus(true)
        setNumberOfN(1)
        setLoadListModel(true)
        closeModal.current.click()
      })
      .catch((error) => {
        setIsSubmittingType2(false)
        if(error?.response['data']['detail']) {
          toast.error(`${error?.response['data']['detail']}`)
        }
        else if(error['message']){
          toast.error(`${error['message']}`)
        }
        else if(error['status']) {
          toast.error(`${error['status']}`)
        }
        else {
          toast.error(`Error...`)
        }
      })
  }

  //State dai
  const [listTournamentId, setListTournamentId] = useState([])
  const [listSymbol, setListSymbol] = useState([])

  const [tournamentSearchId, setTournamentSearchId] = useState()
  const [modelType, setModelType] = useState({ id: 'Type 2', value: 'Input Submission' })
  const [symbol, setSymbol] = useState()
  const [currentModel, setCurrentModel] = useState('')
  const [currentModelText, setCurrentModelText] = useState()

  const [type, setType] = useState('dropDown')

  const [numberOfN, setNumberOfN] = useState(1)
  // const [arraySub, setArraySub] = useState([])
  const [canSubmit, setCanSubmit] = useState(false)

  const [resetStatus, setResetStatus] = useState(false)

  const [exceedModelName, setExceedModelName] = useState(false)
  // console.log('listModel: '+listModel)
  // console.log('currentModel: '+currentModel)

  if (listTournamentId.length > 0 && !tournamentSearchId) {
    setTournamentSearchId(listTournamentId[0])
  }

  if (listSymbol.length > 0 && !symbol) {
    setSymbol(listSymbol[0])
  }

  if (listModel.length > 0 && !currentModel) {
    setCurrentModel(listModel[0])
  }

  useEffect(() => {
    getTournamentAll().then((data) => {
      var lis = data.map((i) => {
        return { id: i['search_id'], value: i['name'] }
      })
      // console.log('datatat: '+JSON.stringify(data))
      setListTournamentId(lis)
    })
    getSymbolByTournamentId('return_and_risk_tournament').then((data) => {
      // console.log("SYM: "+JSON.stringify(data))
      var lis = data['Symbols'].map((s) => {
        return { id: s, value: s }
      })
      // console.log("LIS: "+JSON.stringify(lis))
      setListSymbol(lis)
    })
  }, [])

  useEffect(() => {
    setCurrentSymbol(symbol)
    if (listModel.length > 0) {
      setCurrentModel(listModel[0])
    }
    if(listModel.length == 0) {
      setCurrentModel('')
      setType('input')
    }else {
      setType('dropDown')
    }
  }, [symbol, listModel])

  useEffect(() => {
    var check = false
    arraySub.map((item) => {
      if(!item) return
      try{
        if(!item['min'] || !item['max'] || !item['center']) {check=false;return}
      }catch(error){check=false;return}
      
      if((''+item['min']).match(getNumberValidationRegex()) && (''+item['max']).match(getNumberValidationRegex()) && (''+item['center']).match(getNumberValidationRegex())){
        if((parseFloat(item['min']) <= parseFloat(item['center'])) && (parseFloat(item['center']) <= parseFloat(item['max'])) && (parseFloat(item['min']) <= parseFloat(item['max'])))
          check = true
      }else{
        check = false
      }
    })

    if(type == 'dropDown' && currentModel == ''){
      check = false
    }else if(type == 'input' && (currentModelText == '' || currentModelText == null)){
      check = false
    }

    setCanSubmit(check)
  }, [arraySub, currentModel, currentModelText, type])


  // console.log('ListTournamentId: '+listTournamentId)

  const handleAdd = () => {
    setNumberOfN(numberOfN + 1)
  }

  const handleInputModelName = (e) => {
    var modelName = e.target.value
    if(modelName.length >= 25) {
      setExceedModelName(true)
    }else{
      setCurrentModelText(modelName)
      if(exceedModelName == true) {
        setExceedModelName(false)
      }
    }
  }

  const formatTime = (timeStamp) => {
    var dateFormat = new Date(timeStamp)
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    var minute =
      dateFormat.getMinutes().toString().length == 1 ? '0' + dateFormat.getMinutes() : dateFormat.getMinutes()
    return (
      dateFormat.getHours() +
      'h' +
      minute +
      ' ' +
      (dateFormat.getDate() >= 10 ? dateFormat.getDate() : '0' + dateFormat.getDate()) +
      ' ' +
      monthNames[dateFormat.getMonth()]
    )
  }


  return (
    <div className={cx('curr-round')}>
      {/* <Toaster /> */}
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          borderBottomColor: '#29384E',
          borderBottomWidth: 1,
          borderBottomStyle: 'solid',
          paddingBottom: 20
        }}
      >
        <div>
          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', marginBottom: 12 }}>Next round</div>
          <div style={{ fontSize: 18, fontWeight: 600, color: '#3953DC' }}>{roundNumber}</div>
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: 26,
            padding: '4px 10px 4px 10px',
            borderRadius: 12,
            background: '#34503D',
            color: '#84E4A4',
            fontSize: 10
          }}
        >
          Openning
        </div>
      </div>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 22 }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
            <svg width='17' height='16' viewBox='0 0 17 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M8.68664 13.2144C11.0921 13.2144 13.0421 11.2644 13.0421 8.859C13.0421 6.45356 11.0921 4.50357 8.68664 4.50357C6.2812 4.50357 4.33121 6.45356 4.33121 8.859C4.33121 11.2644 6.2812 13.2144 8.68664 13.2144ZM8.68664 14.6663C11.8939 14.6663 14.4939 12.0663 14.4939 8.859C14.4939 5.65175 11.8939 3.05176 8.68664 3.05176C5.47939 3.05176 2.87939 5.65175 2.87939 8.859C2.87939 12.0663 5.47939 14.6663 8.68664 14.6663Z'
                fill='#9DA7BA'
              />
              <path
                d='M5.3955 2.3879C5.21408 2.03118 4.77568 1.88634 4.43686 2.09934C3.4259 2.73489 2.57019 3.58931 1.93311 4.5993C1.71959 4.93779 1.86377 5.37642 2.22022 5.55838C2.57667 5.74034 3.00989 5.59611 3.23017 5.26198C3.71934 4.52 4.35485 3.88545 5.09758 3.3974C5.43204 3.17763 5.57692 2.74463 5.3955 2.3879Z'
                fill='#9DA7BA'
              />
              <path
                d='M11.9778 2.3879C12.1592 2.03118 12.5976 1.88634 12.9364 2.09934C13.9474 2.73489 14.8031 3.58931 15.4402 4.5993C15.6537 4.93779 15.5095 5.37642 15.1531 5.55838C14.7966 5.74034 14.3634 5.59611 14.1431 5.26198C13.654 4.52 13.0185 3.88545 12.2757 3.3974C11.9413 3.17763 11.7964 2.74463 11.9778 2.3879Z'
                fill='#9DA7BA'
              />
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M8.6866 5.22949C9.08751 5.22949 9.4125 5.55449 9.4125 5.9554V8.55834L10.6517 9.79754C10.9352 10.081 10.9352 10.5406 10.6517 10.8241C10.3682 11.1076 9.9086 11.1076 9.62512 10.8241L8.38592 9.58493C8.11365 9.31266 7.96069 8.94339 7.96069 8.55834V5.9554C7.96069 5.55449 8.28569 5.22949 8.6866 5.22949Z'
                fill='#9DA7BA'
              />
            </svg>
          </div>
          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Open time</div>
        </div>
        <div style={{ fontSize: 14, fontWeight: 600, color: '#fff' }}>{formatTime(startTime)}</div>
      </div>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 12 }}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
            <svg width='17' height='16' viewBox='0 0 17 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M3.36185 2.19526C3.6222 1.93491 4.04431 1.93491 4.30466 2.19526C4.56501 2.45561 4.56501 2.87772 4.30466 3.13807L2.97133 4.47141C2.71097 4.73175 2.28887 4.73175 2.02851 4.47141C1.76817 4.21105 1.76817 3.78895 2.02851 3.52859L3.36185 2.19526ZM12.6952 2.19526C12.9555 1.93491 13.3777 1.93491 13.638 2.19526L14.9713 3.52859C15.2317 3.78895 15.2317 4.21105 14.9713 4.47141C14.711 4.73175 14.2889 4.73175 14.0285 4.47141L12.6952 3.13807C12.4349 2.87772 12.4349 2.45561 12.6952 2.19526ZM8.49992 4C5.92259 4 3.83325 6.08934 3.83325 8.66667C3.83325 11.244 5.92259 13.3333 8.49992 13.3333C11.0773 13.3333 13.1666 11.244 13.1666 8.66667C13.1666 6.08934 11.0773 4 8.49992 4ZM2.49992 8.66667C2.49992 5.35296 5.18621 2.66667 8.49992 2.66667C11.8137 2.66667 14.4999 5.35296 14.4999 8.66667C14.4999 11.9804 11.8137 14.6667 8.49992 14.6667C5.18621 14.6667 2.49992 11.9804 2.49992 8.66667ZM6.69518 6.86193C6.95553 6.60158 7.37765 6.60158 7.63799 6.86193L8.49992 7.72387L9.36185 6.86193C9.62219 6.60158 10.0443 6.60158 10.3047 6.86193C10.565 7.13027 10.565 7.5444 10.3047 7.80473L9.44272 8.66667L10.3047 9.5286C10.565 9.78893 10.565 10.2111 10.3047 10.4714C10.0443 10.7317 9.62219 10.7317 9.36185 10.4714L8.49992 9.60947L7.63799 10.4714C7.37765 10.7317 6.95553 10.7317 6.69518 10.4714C6.43483 10.2111 6.43483 9.78893 6.69518 9.5286L7.55712 8.66667L6.69518 7.80473C6.43483 7.5444 6.43483 7.13027 6.69518 6.86193Z'
                fill='#9DA7BA'
              />
            </svg>
          </div>
          <div style={{display: 'flex', alignItems: 'center'}}>
            <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', }}>Close time</div>
            <Hint content={'The submission time value in your submission corresponds to the closing time, not the opening time'} />
          </div>
        </div>
        <div style={{ fontSize: 14, fontWeight: 600, color: '#fff' }}>{formatTime(endTime)}</div>
      </div>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 12,
          marginBottom: 20
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
            <svg width='17' height='16' viewBox='0 0 17 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
              <path
                d='M9.27559 4.75684C9.40526 5.19556 9.64474 5.58716 9.9622 5.89979L7.72421 7.24258C7.59454 6.80386 7.35506 6.41307 7.0376 6.09963L9.27559 4.75684Z'
                fill='#9DA7BA'
              />
              <path
                d='M7.0376 9.89979L9.27558 11.2426C9.40525 10.8039 9.64473 10.4123 9.96218 10.0996L7.72421 8.75684C7.59454 9.19556 7.35506 9.58715 7.0376 9.89979Z'
                fill='#9DA7BA'
              />
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M11.8334 5.33301C12.5698 5.33301 13.1667 4.73605 13.1667 3.99967C13.1667 3.26329 12.5698 2.66634 11.8334 2.66634C11.097 2.66634 10.5001 3.26329 10.5001 3.99967C10.5001 4.73605 11.097 5.33301 11.8334 5.33301ZM14.5001 3.99967C14.5001 5.47243 13.3062 6.66634 11.8334 6.66634C10.3607 6.66634 9.16675 5.47243 9.16675 3.99967C9.16675 2.52691 10.3607 1.33301 11.8334 1.33301C13.3062 1.33301 14.5001 2.52691 14.5001 3.99967Z'
                fill='#9DA7BA'
              />
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M11.8334 13.333C12.5698 13.333 13.1667 12.7361 13.1667 11.9997C13.1667 11.2633 12.5698 10.6663 11.8334 10.6663C11.097 10.6663 10.5001 11.2633 10.5001 11.9997C10.5001 12.7361 11.097 13.333 11.8334 13.333ZM14.5001 11.9997C14.5001 13.4724 13.3062 14.6663 11.8334 14.6663C10.3607 14.6663 9.16675 13.4724 9.16675 11.9997C9.16675 10.5269 10.3607 9.33301 11.8334 9.33301C13.3062 9.33301 14.5001 10.5269 14.5001 11.9997Z'
                fill='#9DA7BA'
              />
              <path
                fillRule='evenodd'
                clipRule='evenodd'
                d='M5.16667 9.33301C5.90305 9.33301 6.5 8.73605 6.5 7.99967C6.5 7.26329 5.90305 6.66634 5.16667 6.66634C4.43029 6.66634 3.83333 7.26329 3.83333 7.99967C3.83333 8.73605 4.43029 9.33301 5.16667 9.33301ZM7.83333 7.99967C7.83333 9.47243 6.63943 10.6663 5.16667 10.6663C3.69391 10.6663 2.5 9.47243 2.5 7.99967C2.5 6.52692 3.69391 5.33301 5.16667 5.33301C6.63943 5.33301 7.83333 6.52692 7.83333 7.99967Z'
                fill='#9DA7BA'
              />
            </svg>
          </div>
          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Total models submitted</div>
        </div>
        <div style={{ fontSize: 14, fontWeight: 600, display: 'flex', alignItems: 'center' }}>
          <div style={{ fontSize: 14, fontWeight: 600, marginRight: 12, color: '#fff' }}>{myModelSubmitted}</div>
          {/* <div style={{ color: '#84E4A4', display: 'flex', alignItems: 'center' }}>
                                    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M11.1666 4C10.7984 4 10.4999 4.29848 10.4999 4.66667C10.4999 5.03486 10.7984 5.33333 11.1666 5.33333H12.8904L9.16658 9.05719L7.44273 7.33333C6.92203 6.81263 6.07781 6.81263 5.55711 7.33333L2.02851 10.8619C1.76816 11.1303 1.76816 11.5444 2.02851 11.8047C2.28886 12.0651 2.71097 12.0651 2.97132 11.8047L6.49992 8.27614L8.22378 10C8.74447 10.5207 9.58869 10.5207 10.1094 10L13.8333 6.27614V8C13.8333 8.36819 14.1317 8.66667 14.4999 8.66667C14.8681 8.66667 15.1666 8.36819 15.1666 8V4.66667C15.1666 4.29848 14.8681 4 14.4999 4H11.1666Z" fill="#84E4A4" />
                                    </svg>
                                    <div style={{ fontSize: 12, fontWeight: 500, marginLeft: 4 }}>12%</div>
                                </div> */}
        </div>
      </div>
      {/* <div className={cx('heading')}>
        {currentRound && (
          <div className={cx('top')}>
            <p>{ROUND[currentRound['Current round']]}</p>
            <Status active>Opening</Status>
          </div>
        )}
        <div className={cx('bottom')}>Latest Performance</div>
      </div>
      <div className={cx('content-list')}>
        {!isEmptyValue(inputData) ? (
          <>
            <HistoryItem Icon={<ClockOpenIcon />} title='Submission Time'>
              {moment.unix(inputData[0]['Submission Time'] / 1000).format('MM/DD/YYYY')}
            </HistoryItem>
            <HistoryItem Icon={<ClockCloseIcon />} title='Movement Score'>
              {Number(inputData[0]['Movement Score']).toFixed(6)}
            </HistoryItem>
            <HistoryItem Icon={<LinkIcon />} title='Correlation'>
              <p> {Number(inputData[0]['Correlation']).toFixed(6)}</p> */}
      {/* <Trend up value='10%' /> */}
      {/* </HistoryItem>
            <HistoryItem Icon={<ClockCloseIcon />} title='True Contribution'>
              {Number(inputData[0]['True Contribution']).toFixed(6)}
            </HistoryItem>
          </>
        ) : isLoading ? (
          <div className={cx('data-notify')}>
            <ReactLoading type='bubbles' height={32} width={32} />{' '}
          </div>
        ) : (
          <div className={cx('data-notify')}>No data</div>
        )}
      </div>
      <div className={cx('btn')}> */}
      {/* <AppButton
      style={{ width: '100%', background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
    >
      Submit now
    </AppButton> */}
      <AppModal
        className='modalWidthResponsive'
        ref={closeModal}
        // width={477}
        width={maxSm ? '90%' : maxSmBig ? 477 : 477}
        height={650}
        triggerBtn={
          <AppButton style={{ width: '100%', background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}>
            Submit now
          </AppButton>
        }
        contentStyle={{
          borderRadius: '16px',
          padding: '24px',
          border: 'none',
          backgroundColor: '#29384E'
        }}
      >
        <div className={cx('submit-modal')}>
          <h4 className={cx('heading')}>Submit now?</h4>
          <p className={cx('desc')} style={{ textAlign: 'center' }}>
            Are you sure you want to submit this round now?
          </p>
          <p className={cx('desc')} style={{ color: '#fff', fontSize: 16, marginTop: 10, marginBottom: 4 }}>
            Tournament
          </p>
          <DropDownComponent
            currentState={tournamentSearchId}
            setCurrentState={setTournamentSearchId}
            listElement={listTournamentId?.map((i) => i)}
          />
          <p className={cx('desc')} style={{ color: '#fff', fontSize: 16, marginTop: 10, marginBottom: 4 }}>
            Submisstion Type
          </p>
          <DropDownComponent
            currentState={modelType}
            setCurrentState={setModelType}
            listElement={[
              { id: 'Type 1', value: 'File Submission' },
              { id: 'Type 2', value: 'Input Submission' }
            ]}
          />
          <p className={cx('desc')} style={{ color: '#fff', fontSize: 16, marginTop: 10, marginBottom: 4 }}>
            Symbol
          </p>
          <DropDownComponent
            currentState={symbol}
            setCurrentState={setSymbol}
            listElement={listSymbol?.map((i) => i)}
          />
          <p
            className={cx('desc')}
            style={{
              color: '#fff',
              fontSize: 16,
              marginTop: 10,
              marginBottom: 4,
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            <div>Choose model</div>
            <div>
              <span style={{ marginRight: 12 }}>New model</span>
              {type == 'dropDown' ? (
                <svg
                  onClick={() => setType('input')}
                  width='18'
                  height='18'
                  viewBox='0 0 18 18'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <rect width='18' height='18' rx='6' fill='#455066' />
                </svg>
              ) : (
                <svg
                  onClick={() => setType('dropDown')}
                  width='18'
                  height='18'
                  viewBox='0 0 18 18'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <rect width='18' height='18' rx='6' fill='url(#paint0_linear_1966_18888)' />
                  <path
                    fillRule='evenodd'
                    clipRule='evenodd'
                    d='M13.3536 6.14645C13.5488 6.34171 13.5488 6.65829 13.3536 6.85355L9.06066 11.1464C8.47487 11.7322 7.52513 11.7322 6.93934 11.1464L5.14645 9.35355C4.95118 9.15829 4.95118 8.84171 5.14645 8.64645C5.34171 8.45118 5.65829 8.45118 5.85355 8.64645L7.64645 10.4393C7.84171 10.6346 8.15829 10.6346 8.35355 10.4393L12.6464 6.14645C12.8417 5.95118 13.1583 5.95118 13.3536 6.14645Z'
                    fill='white'
                  />
                  <defs>
                    <linearGradient
                      id='paint0_linear_1966_18888'
                      x1='0.529412'
                      y1='2.7'
                      x2='28.5319'
                      y2='5.67489'
                      gradientUnits='userSpaceOnUse'
                    >
                      <stop stopColor='#3953DC' />
                      <stop offset='1' stopColor='#9C9BE6' />
                    </linearGradient>
                  </defs>
                </svg>
              )}
            </div>
          </p>
          {type == 'dropDown' ? (
            <DropDownComponent
              currentState={currentModel}
              setCurrentState={setCurrentModel}
              listElement={listModel?.map((i) => i)}
            />
          ) : (
            <>
              <input
                value={currentModelText}
                onChange={(e) => handleInputModelName(e)}
                placeholder='Input your model...'
                style={{
                  width: '100%',
                  height: 40,
                  padding: '10px 12px',
                  fontSize: 16,
                  fontWeight: 500,
                  borderRadius: 16,
                  background: '#455066',
                  border: 'none',
                  color: 'white'
                }}
              />
              {currentModelText == '' && <div style={{fontSize: 14, color: '#F15058', marginTop: 10}}>Model name can not be empty</div>}
              {exceedModelName && <div style={{fontSize: 14, color: '#F15058', marginTop: 10}}>Model name must be less than 25 characters</div>}
            </>
          )}

          {modelType['id'] == 'Type 1' ? (
            <div className={cx('input')}>
              <label htmlFor='submit-input'>
                <div className={cx('img')}>
                  <UploadImg />
                </div>
                <div className={cx('text')}>{file?.name ?? 'Upload your file'}</div>
                <p>Drag & drop your file here</p>
              </label>
              <input
                id='submit-input'
                type='file'
                onChange={(e) => {
                  setFile(e.target.files[0])
                }}
              />
            </div>
          ) : (
            <>
              {[...Array(numberOfN)].map((item, index) => {
                return (
                  <SliceSubmissionComponent
                    key={index}
                    numberOfN={numberOfN}
                    setNumberOfN={setNumberOfN}
                    index={index}
                    resetStatus={resetStatus}
                    setResetStatus={setResetStatus}
                    startTime={startTime}
                    endTime={endTime}
                  />
                )
              })}
              {/* <AppButton
                  
                  style={{ width: '100%', background: '#455066', marginTop: 20, height: 40 }}
                >
                  
                </AppButton> */}
              <div style={{ display: 'flex', alignItems: 'center', marginTop: 10 }}>
                <div onClick={() => handleAdd()} className={cx('AddIcon')}>
                  <AddIcon />
                </div>
                <div style={{ marginLeft: 10, fontSize: 16, color: 'white' }}>Add new submission</div>
              </div>
            </>
          )}

          <div style={{ paddingBottom: 30, marginTop: modelType['id'] == 'Type 1' ? 40 : 20 }} className={cx('action')}>
            <AppButton
              onClick={() => {
                closeModal.current.click()
                setNumberOfN(1)
                dispatch(setArraySub([]))
              }}
              style={{ width: '100%', background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
            >
              Cancel
            </AppButton>
            <AppButton
              onClick={modelType['id'] == 'Type 1' ? handleSubmitModel : handleSubmitModelType2}
              isLoading={modelType['id'] == 'Type 1' ? isSubmitting : isSubmittingType2}
              style={{ width: '100%', background: (modelType['id'] == 'Type 1' || canSubmit) ? 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' : '#455066' }}
              disabled={modelType['id'] == 'Type 1' ? false : !canSubmit}
            >
              Submit now
            </AppButton>
          </div>
        </div>
      </AppModal>
    </div>
    // </div>
  )
}

function SliceSubmissionComponent({
  index,
  resetStatus,
  setResetStatus,
  startTime,
  endTime,
  // numberOfN,
  // setNumberOfN
}) {
  const dispatch = useDispatch()
  var arraySub = useSelector((state) => state.model.arraySub)

  const maxSm = useMediaQuery('(max-width: 768px)')
  // const maxSmBig = useMediaQuery('(max-width: 992px)')

  // Get the current timezone offset in minutes
  const timezoneOffset = new Date().getTimezoneOffset();

  // Convert the offset to hours and add the "+" sign if needed
  const hours = Math.abs(Math.floor(timezoneOffset / 60));
  const sign = timezoneOffset < 0 ? "+" : "-";

  // Format the timezone offset
  const timezone = parseInt(`${sign}${hours}`);

  var default_time = timezone>=0?timezone:24+timezone

  //Calculate to move into a new day
  // var new_day = false
  // Get the current date and time
  const now = new Date();

  // Get the current local hour
  const currentHour = now.getHours();
  // console.log('END: '+endTime)
  const time_calculate = default_time+12>24 ? default_time+12-24 : default_time+12
  // if(currentHour >= time_calculate){
  //   new_day = true
  // }

  //Get date
  // Get the current date
  const currentDate = new Date(endTime);//Date of the endTime

  const current_date_str = currentDate.toISOString().split('T')[0];
  // Add 1 day to the current date
  // currentDate.setDate(currentDate.getDate() + 1);

  // // Get the ISO string representation of the updated date
  // const next_date_str = currentDate.toISOString().split('T')[0];


  const [date, setDate] = useState(current_date_str)

  //is disable 7h - 0hUTC
  var isDisabled0hUTC = false
  if(date == current_date_str && currentDate.getUTCHours() == 12){
    isDisabled0hUTC = true
  }

  const [hour, setHour] = useState({ id: `${isDisabled0hUTC?time_calculate:default_time}h`, value: `${isDisabled0hUTC?time_calculate:default_time}h`})
  const [min, setMin] = useState('')
  const [center, setCenter] = useState('')
  const [max, setMax] = useState('')
  const [timeStampUTC, setTimeStampUTC] = useState()
  const [mounted, setMounted] = useState(true)
  const [mode, setMode] = useState('')
  const [validationObj, setValidationObj] = useState({min: false, center: false, max: false, minMsg: '', centerMsg: '', maxMsg: ''})  
    

  // const [resultString, setResultString] = useState('')
  const datePickerStyles = {
    container: {
      display: 'inline-block',
      position: 'relative'
    },
    input: {
      width: maxSm ? '140px' : '200px', // Adjust the width as per your preference
      padding: '11px',
      border: '1px solid #ccc',
      borderRadius: '4px',
      fontSize: '14px',
      backgroundColor: '#455066'
      // border: 'none'
    }
  }
  function getCurrentDate() {
    const now = new Date(endTime)
    const year = now.getFullYear()
    const month = String(now.getMonth() + 1).padStart(2, '0')
    const day = String(now.getDate()).padStart(2, '0')

    //check whether time is over 19h
    // var localTime = new Date();
    // var currentTimeUTCTimeStamp = new Date(new Date().getTime() + localTime.getTimezoneOffset() * 60000);
    // console.log('currentTimeUTCTimeStamp: '+currentTimeUTCTimeStamp)
    return `${year}-${month}-${day}`
  }
  // console.log('date: '+date)

  function convertUserChoiceToTimeStamp() {
    var now = new Date(date)
    var h = hour['id'].split('h')
    if (h) now.setHours(h[0])
    // console.log('now: '+now.getTime())
    var utcTime = now.getTime()
    return utcTime
  }

  useEffect(() => {
    if (resetStatus == true) {
      setMin('')
      setMax('')
      setCenter('')
      setTimeStampUTC(0)
      setDate(current_date_str)
      setHour({ id: `${isDisabled0hUTC?time_calculate:default_time}h`, value: `${isDisabled0hUTC?time_calculate:default_time}h`})
      setResetStatus(false)
    }
  }, [])

  useEffect(() => {
    var str = { round_time: convertUserChoiceToTimeStamp(), min: min, max: max, center: center }
    var rs = [...arraySub]
    // console.log('RS: '+rs.length)
    rs[index] = str
    dispatch(setArraySub(rs))
    setTimeStampUTC(convertUserChoiceToTimeStamp())


  }, [hour, date])

  useEffect(() => {
    //Reset hour if the date is date of endTime  
    if(date == new Date(endTime).toISOString().split('T')[0]){
      setHour({ id: `${isDisabled0hUTC?time_calculate:default_time}h`, value: `${isDisabled0hUTC?time_calculate:default_time}h`})
    }
  }, [date])

  useEffect(() => {
    var str = { round_time: convertUserChoiceToTimeStamp(), min: min, max: max, center: center }
    var rs = [...arraySub]
    rs[index] = str
    dispatch(setArraySub(rs))
  }, [min, max, center])

  function handleSetValue(e, mode) {
    var num = e.target.value
    if(mode == 'min'){
      if(num.match(getNumberValidationRegex()) && num!=''){
        setValidationObj({...validationObj, min: false})
      }else{
        setValidationObj({...validationObj, min: true})
      }
      setMin(num)
      setMode('min')
    }
    if(mode == 'center'){
      if(num.match(getNumberValidationRegex()) && num!=''){
        setValidationObj({...validationObj, center: false})
      }else{
        setValidationObj({...validationObj, center: true})
      }
      setCenter(num)
      setMode('center')
    }
    if(mode == 'max'){
      if(num.match(getNumberValidationRegex()) && num!=''){
        setValidationObj({...validationObj, max: false})
      }else{
        setValidationObj({...validationObj, max: true})
      }
      setMax(num)
      setMode('max')
    }

  }
  useEffect(() => {
    var obj = {
      ...validationObj,
      // minMsg: parseFloat(min)>parseFloat(center) ? 'Cannot be larger than center' : parseFloat(min)>parseFloat(max) ? 'Cannot larger than max' : '',
      // centerMsg: parseFloat(center)<parseFloat(min) ? 'Cannot be smaller than min' : parseFloat(center)>parseFloat(max) ? 'Cannot larger than max' : '',
      // maxMsg: parseFloat(max)<parseFloat(min) ? 'Cannot be smaller than min' : parseFloat(max)<parseFloat(center) ? 'Cannot be smaller than center' : '',
    }
    if(mode == 'min') {
      obj = {
        ...obj,
        minMsg: (parseFloat(min)>parseFloat(center) && min.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex())) ? 'Cannot be larger than middle' : (parseFloat(min)>parseFloat(max) && min.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex())) ? 'Cannot be larger than max' : '',
        centerMsg: (parseFloat(center)>parseFloat(max) && center.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex())) ? 'Cannot be larger than max' : (parseFloat(center)>=parseFloat(min) && center.match(getNumberValidationRegex()) && min.match(getNumberValidationRegex()) && validationObj['centerMsg'] == 'Cannot be smaller than min') ? '' : validationObj['centerMsg'],
        maxMsg: (parseFloat(max)>=parseFloat(min) && max.match(getNumberValidationRegex()) && min.match(getNumberValidationRegex()) && validationObj['maxMsg'] == 'Cannot be smaller than min') ? '' : validationObj['maxMsg']
      }
    }else if(mode == 'center'){
      obj = {
        ...obj,
        centerMsg: (parseFloat(center)<parseFloat(min) && min.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex())) ? 'Cannot be smaller than min' : (parseFloat(center)>parseFloat(max) && center.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex())) ? 'Cannot be larger than max' : '',
        minMsg: (parseFloat(min)<=parseFloat(center) && min.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex()) && validationObj['minMsg'] == 'Cannot be larger than middle') ? '' : validationObj['minMsg'],
        maxMsg: (parseFloat(max)>=parseFloat(center) && max.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex()) && validationObj['maxMsg'] == 'Cannot be smaller than middle') ? '' : validationObj['maxMsg']
      }
    }else if(mode == 'max') {
      obj = {
        ...obj,
        maxMsg: (parseFloat(max)<parseFloat(min) && max.match(getNumberValidationRegex()) && min.match(getNumberValidationRegex())) ? 'Cannot be smaller than min' : (parseFloat(max)<parseFloat(center) && max.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex())) ? 'Cannot be smaller than middle' : '',
        minMsg: (parseFloat(min)<=parseFloat(max) && min.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex()) && validationObj['minMsg'] == 'Cannot be larger than max') ? '' : validationObj['minMsg'],
        centerMsg: (parseFloat(center)<=parseFloat(max) && center.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex()) && validationObj['centerMsg'] == 'Cannot be larger than max') ? '' : validationObj['centerMsg']
      }
    }
    setValidationObj(obj)
  }, [min, max, center])
  

  const handleMinusClick = () => {
    var arrCopy = [...arraySub]
    // console.log("STRTRUOC: "+JSON.stringify(arrCopy))
    // console.log('INDEX: '+index)
    arrCopy[index] = null
    dispatch(setArraySub(arrCopy))
    // console.log("STRSAU: "+JSON.stringify(arrCopy))
    setMounted(false)
  }
  if (!mounted) {
    return null // Don't render the component if it's not mounted
  }

  return (
    <div>
      <div style={{ display: 'flex', color: 'white', alignItems: 'end' }}>
        <div style={{ width: '55%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Date</div>
          <div style={datePickerStyles.container}>
            <input
              type='date'
              style={datePickerStyles.input}
              value={date}
              onChange={(e) => setDate(e.target.value)}
              min={getCurrentDate()}
            />
          </div>
        </div>
        <div style={{ width: '33%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Hour</div>
          <DropDownComponent
            width={'30%'}
            currentState={hour}
            setCurrentState={setHour}
            listDisabled={[isDisabled0hUTC ? { id: `${default_time}h`, value: `${default_time}h`} : {}]}
            listElement={[
              { id: `${default_time}h`, value: `${default_time}h`},
              { id: `${time_calculate}h`, value: `${time_calculate}h` }
            ]}
          />
        </div>
        <div onClick={() => handleMinusClick()} className={cx('MinusIcon')}>
          <MinusIcon />
        </div>
      </div>

      <div style={{ display: 'flex', color: 'white' }}>
        <div style={{ width: '34%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Value Min</div>
          <input
            value={min}
            onChange={(e) => handleSetValue(e, 'min')}
            style={{
              width: '90%',
              background: '#455066',
              height: 40,
              padding: '10px 12px',
              borderRadius: 16,
              fontSize: 16,
              fontWeight: 500,
              border: 'none',
              color: 'white'
            }}
          />
          {
          validationObj['min'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            Must be a number
          </div> :
          validationObj['minMsg'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            {validationObj['minMsg']}
          </div>
          :''
          }
        </div>
        <div style={{ width: '34%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8}}>
            Value Middle
          </div>
          <input
            value={center}
            onChange={(e) => handleSetValue(e, 'center')}
            style={{
              width: '90%',
              background: '#455066',
              height: 40,
              padding: '10px 12px',
              borderRadius: 16,
              fontSize: 16,
              fontWeight: 500,
              border: 'none',
              color: 'white'
            }}
          />
          { 
          validationObj['center'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            Must be a number
          </div>:validationObj['centerMsg'] ?
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            {validationObj['centerMsg']}
          </div>
          :''
          }
        </div>
        <div style={{ width: '33%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Value Max</div>
          <input
            value={max}
            onChange={(e) => handleSetValue(e, 'max')}
            style={{
              width: '100%',
              background: '#455066',
              height: 40,
              padding: '10px 12px',
              borderRadius: 16,
              fontSize: 16,
              fontWeight: 500,
              border: 'none',
              color: 'white'
            }}
          />
          {
          validationObj['max'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            Must be a number
          </div>:validationObj['maxMsg']?
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            {validationObj['maxMsg']}
          </div>
          :''
          }
        </div>
      </div>
    </div>
  )
}


export default CurrentRound
