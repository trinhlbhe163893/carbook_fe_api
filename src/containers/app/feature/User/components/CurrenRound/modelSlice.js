import { createSlice } from '@reduxjs/toolkit'

const modelSlice = createSlice({
  name: 'model',
  initialState: { arraySub: [] },
  reducers: {
    setArraySub: (state, action) => {
      state.arraySub = action.payload
    },
  }
})

export const { setArraySub } = modelSlice.actions
export default modelSlice.reducer
