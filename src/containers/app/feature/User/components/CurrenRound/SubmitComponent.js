/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-unused-vars */
/* eslint-disable  react-hooks/rules-of-hooks */
import { /*ClockCloseIcon, ClockOpenIcon, LinkIcon,*/ UploadImg } from '@src/assets/svgs'
import AppButton from '@src/components/AppButton'
import AppModal from '@src/components/AppModal/AppModal'
// import Status from '@src/components/Status'
// import { ROUND } from '@src/configs'
// import { isEmptyValue } from '@src/helpers/check'
import classNames from 'classnames/bind'
// import moment from 'moment/moment'
import { useEffect, /*useRef,*/ useState } from 'react'
import { /*Toaster,*/ toast } from 'react-hot-toast'
// import ReactLoading from 'react-loading'
import { userApi } from '../../userService'
// import HistoryItem from '../HistoryItem'
import styles from './CurrentRound.module.sass'
import DropDownComponent from '../DropDown'
import { getTournamentAll, getSymbolByTournamentId, submitModelInput, getBackTestInfo } from '../../../../../../../src/axios/getApiAll'
import { useNavigate, useSearchParams } from 'react-router-dom'
// import DateTimePicker from 'react-datetime-picker'
import 'react-datepicker/dist/react-datepicker.css'
// import DatePicker from 'react-datepicker'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { AddIcon, MinusIcon } from '@src/assets/svgs'
import { getNumberValidationRegex } from '@src/helpers/validator'
import { useDispatch, useSelector } from 'react-redux'
import { setArraySub } from './modelSlice'

const cx = classNames.bind(styles)

function SubmitComponent({
  isOpen,
  setIsOpen,
  // myModelSubmitted,
  reloadData,
  startTime,
  endTime,
  listModel,
  setLoadListModel,
  typee,
  page,
  setCurrentSymbol
}) {
  //
  const dispatch = useDispatch()
  var arraySub = useSelector((state) => state.model.arraySub)

  const navigate = useNavigate()
  const [queryParameters] = useSearchParams()
  const [file, setFile] = useState()
  const [submitModel, { isLoading: isSubmitting }] = userApi.endpoints.submitModel.useMutation()
  const [isSubmittingType2, setIsSubmittingType2] = useState(false)
  const maxSm = useMediaQuery('(max-width: 768px)')
  const maxSmBig = useMediaQuery('(max-width: 992px)')

  //State dai
  const [listTournamentId, setListTournamentId] = useState([])
  const [listSymbol, setListSymbol] = useState([])

  const [tournamentSearchId, setTournamentSearchId] = useState(null)
  const [modelType, setModelType] = useState({ id: 'Type 2', value: 'Input Submission' })
  const [symbol, setSymbol] = useState()
  const [currentModel, setCurrentModel] = useState('')
  const [currentModelText, setCurrentModelText] = useState()

  const [type, setType] = useState('dropDown')
  const [numberOfN, setNumberOfN] = useState(1)
  const [canSubmit, setCanSubmit] = useState(false)
  // const [arraySub, setArraySub] = useState([])
  const [resetStatus, setResetStatus] = useState(false)

  const [exceedModelName, setExceedModelName] = useState(false)

  const [backTestInfo, setBackTestInfo] = useState({})
  // console.log('listModel: '+listModel)
  // console.log('currentModel: '+currentModel)

  //Get data query
  if (queryParameters.get('id') != null && tournamentSearchId == null && listTournamentId.length != 0) {
    var tournament_search_id1 = queryParameters.get('tournament')

    var findTournament_search_id = listTournamentId?.filter((i) => i['id'] == tournament_search_id1)

    // var submission_type1 = queryParameters.get('type')
    var symbol1 = queryParameters.get('symbol')
    var model_id1 = queryParameters.get('id')
    // var submission_time1 = queryParameters.get('time')
    setTournamentSearchId(findTournament_search_id[0])
    setSymbol({ id: symbol1, value: symbol1 })
    setCurrentModel({ id: model_id1, value: model_id1 })
  } else if (queryParameters.get('id') == null) {
    if (listTournamentId.length > 0 && !tournamentSearchId) {
      setTournamentSearchId(listTournamentId[0])
    }

    if (listSymbol.length > 0 && !symbol) {
      setSymbol(listSymbol[0])
    }

    if (listModel.length > 0 && !currentModel) {
      setCurrentModel(listModel[0])
    }
  }

  useEffect(() => {
    if(typee == 'back_test' && symbol != undefined) {
      getBackTestInfo(symbol?.id)
      .then((data) => {
        setBackTestInfo(data)
      })
    }
  }, [symbol])
  // console.log("LIST: "+JSON.stringify(listTournamentId))

  const handleSubmitModelType2 = async () => {
    //Check error when time is over 7h 19h
    // var userChooseTime = new Date(date);
    // var h = hour['id'].split('h')
    // if(h) userChooseTime.setHours(h[0])

    // var now = new Date()

    // if((now.getTime() - userChooseTime.getTime() > 0) && typee != 'back_test'){
    //   toast.error('Time is not valid. Please try again', styles={backgroundColor:'#2C120A', color:'#FF7373', borderColor:'#FF7373'})
    //   return
    // }

    var data = {
      submission_type: typee == 'back_test' ? 'back_test' : 'live',
      symbol: symbol['id'],
      model_id: type == 'dropDown' ? currentModel['id'] : currentModelText,
      data: [...arraySub.filter(item => item != null).map(item => {return {...item, min: parseFloat(item['min']), max: parseFloat(item['max']), center: parseFloat(item['center'])}})]
    }
    setIsSubmittingType2(true)
    submitModelInput(data)
      .then(() => {
        toast.success('Round submitted successully!')

        // setType('dropDown')
        setIsSubmittingType2(false)
        setResetStatus(true)
        setNumberOfN(1)
        setLoadListModel(true)
        // closeModal.current.click()
        setIsOpen(false)
        if (page == 'modelDetail') {
          navigate('/model')
        }
      })
      .catch((error) => {
        setIsSubmittingType2(false)
        if(error?.response['data']['detail']) {
          toast.error(`${error?.response['data']['detail']}`)
        }
        else if(error['message']){
          toast.error(`${error['message']}`)
        }
        else if(error['status']) {
          toast.error(`${error['status']}`)
        }
        else {
          toast.error(`Error...`)
        }
      })
  }

  // console.log('setItemDetailModel: '+JSON.stringify(itemDetailModel))

  // if(listTournamentId.length > 0 && !tournamentSearchId) {
  //   setTournamentSearchId(listTournamentId[0])
  // }

  // if(listSymbol.length > 0 && !symbol) {
  //   setSymbol(listSymbol[0])
  // }

  // if(listModel.length > 0 && !currentModel) {
  //   setCurrentModel(listModel[0])
  // }

  useEffect(() => {
    getTournamentAll().then((data) => {
      var lis = data.map((i) => {
        return { id: i['search_id'], value: i['name'] }
      })
      // console.log('datatat: '+JSON.stringify(data))
      setListTournamentId(lis)
    })
    getSymbolByTournamentId('return_and_risk_tournament').then((data) => {
      // console.log("SYM: "+JSON.stringify(data))
      var lis = data['Symbols'].map((s) => {
        return { id: s, value: s }
      })
      // console.log("LIS: "+JSON.stringify(lis))
      setListSymbol(lis)
    })
  }, [])

  if (!queryParameters.get('id')) {
    useEffect(() => {
      setCurrentSymbol(symbol)
      if (listModel.length > 0) {
        setCurrentModel(listModel[0])
      }
      if(listModel.length == 0) {
        setCurrentModel('')
        setType('input')
      }else {
        setType('dropDown')
      }
    }, [symbol, listModel])
  }

  useEffect(() => {
    var check = false
    arraySub.map((item) => {
      if(!item) return
      try{
        if(!item['min'] || !item['max'] || !item['center']) {check=false;return}
      }catch(error){check=false;return}
      
      if((''+item['min']).match(getNumberValidationRegex()) && (''+item['max']).match(getNumberValidationRegex()) && (''+item['center']).match(getNumberValidationRegex())){
        if((parseFloat(item['min']) <= parseFloat(item['center'])) && (parseFloat(item['center']) <= parseFloat(item['max'])) && (parseFloat(item['min']) <= parseFloat(item['max'])))
          check = true
      }else{
        check = false
      }
    })

    if(type == 'dropDown' && currentModel == ''){
      check = false
    }else if(type == 'input' && (currentModelText == '' || currentModelText == null)){
      check = false
    }

    setCanSubmit(check)
  }, [arraySub, currentModel, currentModelText, type])



  const handleSubmitModel = async () => {
    const data = new FormData()
    if (file) {
      data.append('file', file)
      const response = await submitModel({
        // url: `/tournament/${params['tournamentSearchId']}/submit/file?submission_type=${params['submissionType']}&symbol=${params['symbol']}&model_id=${params['model']}%201232141241241`,
        params: {
          submissionType: typee == 'back_test' ? 'back_test' : 'live',
          tournamentSearchId: tournamentSearchId['id'],
          symbol: symbol['id'],
          model: type == 'dropDown' ? currentModel['id'] : currentModelText
        },
        body: data
      })

      if (response?.error) {
        toast.error(response?.error?.data?.detail || `File submission error`)
      } else {
        toast.success('Round submitted successully!')
        setTimeout(() => {
          reloadData()
        }, 10000)
        setIsOpen(false)
        setFile('')
        setCurrentModelText('')
        setLoadListModel(true)
        if (page == 'modelDetail') {
          navigate('/model')
        }
      }
    } else {
      toast.error('Please choose a file')
    }
  }

  const handleInputModelName = (e) => {
    var modelName = e.target.value
    if(modelName.length >= 25) {
      setExceedModelName(true)
    }else{
      setCurrentModelText(modelName)
      if(exceedModelName == true) {
        setExceedModelName(false)
      }
    }
  }

  return (
    <div className={cx('curr-round')}>
      {/* <Toaster /> */}
      <AppModal
        width={maxSm ? '90%' : maxSmBig ? 477 : 477}
        height={650}
        isOpen={isOpen}
        contentStyle={{
          borderRadius: '16px',
          padding: '24px',
          border: 'none',
          backgroundColor: '#29384E'
        }}
      >
        <div className={cx('submit-modal')}>
          <h4 className={cx('heading')}>Submit now?</h4>
          <p className={cx('desc')} style={{ textAlign: 'center' }}>
            Are you sure you want to submit this round now?
          </p>
          <p className={cx('desc')} style={{ color: '#fff', fontSize: 16, marginTop: 10, marginBottom: 4 }}>
            Tournament
          </p>
          <DropDownComponent
            currentState={tournamentSearchId}
            setCurrentState={setTournamentSearchId}
            listElement={listTournamentId?.map((i) => i)}
          />
          <p className={cx('desc')} style={{ color: '#fff', fontSize: 16, marginTop: 10, marginBottom: 4 }}>
            Submisstion Type
          </p>
          <DropDownComponent
            currentState={modelType}
            setCurrentState={setModelType}
            listElement={[
              { id: 'Type 1', value: 'File Submission' },
              { id: 'Type 2', value: 'Input Submission' }
            ]}
          />
          <p className={cx('desc')} style={{ color: '#fff', fontSize: 16, marginTop: 10, marginBottom: 4 }}>
            Symbol
          </p>
          <DropDownComponent
            mode={page == 'modelDetail' ? 'noChoose' : 'normal'}
            currentState={symbol}
            setCurrentState={setSymbol}
            listElement={listSymbol?.map((i) => i)}
          />
          <p
            className={cx('desc')}
            style={{
              color: '#fff',
              fontSize: 16,
              marginTop: 10,
              marginBottom: 4,
              display: 'flex',
              justifyContent: 'space-between'
            }}
          >
            <div>Choose model</div>
            {page != 'modelDetail' && (
              <div>
                <span style={{ marginRight: 12 }}>New model</span>
                {type == 'dropDown' ? (
                  <svg
                    onClick={() => setType('input')}
                    width='18'
                    height='18'
                    viewBox='0 0 18 18'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <rect width='18' height='18' rx='6' fill='#455066' />
                  </svg>
                ) : (
                  <svg
                    onClick={() => setType('dropDown')}
                    width='18'
                    height='18'
                    viewBox='0 0 18 18'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <rect width='18' height='18' rx='6' fill='url(#paint0_linear_1966_18888)' />
                    <path
                      fillRule='evenodd'
                      clipRule='evenodd'
                      d='M13.3536 6.14645C13.5488 6.34171 13.5488 6.65829 13.3536 6.85355L9.06066 11.1464C8.47487 11.7322 7.52513 11.7322 6.93934 11.1464L5.14645 9.35355C4.95118 9.15829 4.95118 8.84171 5.14645 8.64645C5.34171 8.45118 5.65829 8.45118 5.85355 8.64645L7.64645 10.4393C7.84171 10.6346 8.15829 10.6346 8.35355 10.4393L12.6464 6.14645C12.8417 5.95118 13.1583 5.95118 13.3536 6.14645Z'
                      fill='white'
                    />
                    <defs>
                      <linearGradient
                        id='paint0_linear_1966_18888'
                        x1='0.529412'
                        y1='2.7'
                        x2='28.5319'
                        y2='5.67489'
                        gradientUnits='userSpaceOnUse'
                      >
                        <stop stopColor='#3953DC' />
                        <stop offset='1' stopColor='#9C9BE6' />
                      </linearGradient>
                    </defs>
                  </svg>
                )}
              </div>
            )}
          </p>
          {type == 'dropDown' ? (
            <DropDownComponent
              mode={page == 'modelDetail' ? 'noChoose' : 'normal'}
              currentState={currentModel}
              setCurrentState={setCurrentModel}
              listElement={listModel?.map((i) => i)}
            />
          ) : (
            <>
              <input
                value={currentModelText}
                onChange={(e) => handleInputModelName(e)}
                placeholder='Input your model...'
                style={{
                  width: '100%',
                  height: 40,
                  padding: '10px 12px',
                  fontSize: 16,
                  fontWeight: 500,
                  borderRadius: 16,
                  background: '#455066',
                  border: 'none',
                  color: 'white'
                }}
              />
              {currentModelText == '' && <div style={{fontSize: 14, color: '#F15058', marginTop: 10}}>Model name can not be empty</div>}
              {exceedModelName && <div style={{fontSize: 14, color: '#F15058', marginTop: 10}}>Model name must be less than 25 characters</div>}
            </>
          )}

          {modelType['id'] == 'Type 1' ? (
            <div className={cx('input')}>
              <label htmlFor='submit-input'>
                <div className={cx('img')}>
                  <UploadImg />
                </div>
                <div className={cx('text')}>{file?.name ?? 'Upload your file'}</div>
                <p>Drag & drop your file here</p>
              </label>
              <input
                id='submit-input'
                type='file'
                onChange={(e) => {
                  setFile(e.target.files[0])
                }}
              />
            </div>
          ) : (
            <>
              {[...Array(numberOfN)].map((item, index) => {
                return (
                  <SliceSubmissionComponent
                    key={index}
                    index={index}
                    resetStatus={resetStatus}
                    setResetStatus={setResetStatus}
                    typee={typee}
                    endTime={endTime}
                    backTestInfo={backTestInfo}
                  />
                )
              })}
              {/* <AppButton
                  onClick={() => setNumberOfN(numberOfN+1)}
                  style={{ width: '100%', background: '#455066', marginTop: 20, height: 40 }}
                >
                  <div style={{fontSize: 30}}>+</div>
                </AppButton> */}
              <div style={{ display: 'flex', alignItems: 'center', marginTop: 10 }}>
                <div onClick={() => setNumberOfN(numberOfN + 1)} className={cx('AddIcon')}>
                  <AddIcon />
                </div>
                <div style={{ marginLeft: 10, fontSize: 16, color: 'white' }}>Add new submission</div>
              </div>
            </>
          )}

          <div style={{ paddingBottom: 30, marginTop: modelType['id'] == 'Type 1' ? 40 : 20 }} className={cx('action')}>
            <AppButton
              onClick={() => {setIsOpen(false); dispatch(setArraySub([]))}}
              style={{ width: '100%', background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
            >
              Cancel
            </AppButton>
            <AppButton
              onClick={modelType['id'] == 'Type 1' ? handleSubmitModel : handleSubmitModelType2}
              isLoading={modelType['id'] == 'Type 1' ? isSubmitting : isSubmittingType2}
              style={{ width: '100%', background: (modelType['id'] == 'Type 1' || canSubmit) ? 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' : '#455066' }}
              disabled={modelType['id'] == 'Type 1' ? false : !canSubmit}
            >
              Submit now
            </AppButton>
          </div>
        </div>
      </AppModal>
    </div>
  )
}

function SliceSubmissionComponent({ index, resetStatus, setResetStatus, typee, endTime, backTestInfo }) {
  // console.log('backTestInfo: '+JSON.stringify(backTestInfo))
  const dispatch = useDispatch()
  var arraySub = useSelector((state) => state.model.arraySub)
  const maxSm = useMediaQuery('(max-width: 768px)')
  // const maxSmBig = useMediaQuery('(max-width: 992px)')
 
  // Get the current timezone offset in minutes
  const timezoneOffset = new Date().getTimezoneOffset();

  // Convert the offset to hours and add the "+" sign if needed
  const hours = Math.abs(Math.floor(timezoneOffset / 60));
  const sign = timezoneOffset < 0 ? "+" : "-";

  // Format the timezone offset
  const timezone = parseInt(`${sign}${hours}`);

  var default_time = timezone>=0?timezone:24+timezone

  //Live...
  //Calculate to move into a new day
  // var new_day = false
  // Get the current date and time
  const now = new Date();

  // Get the current local hour
  const currentHour = now.getHours();

  const time_calculate = default_time+12>24 ? default_time+12-24 : default_time+12
  // if(currentHour >= time_calculate){
  //   new_day = true
  // }

  // Get the current date
  const currentDate = new Date(endTime);//Date of endTime

  const current_date_str = currentDate.toISOString().split('T')[0];
  // Add 1 day to the current date
  // currentDate.setDate(currentDate.getDate() + 1);

  // Get the ISO string representation of the updated date
  // const next_date_str = currentDate.toISOString().split('T')[0];

 

  //BackTest...

  var currentDate_bt = new Date();
  currentDate_bt.setDate(currentDate_bt.getDate() - 1)
  // if(backTestInfo['Close time'] != undefined){
  //   currentDate_bt = new Date(backTestInfo['Close time'])
  // }

  const current_date_str_bt = currentDate_bt.toISOString().split('T')[0];


  const [date, setDate] = typee == 'live' ? useState(current_date_str) : useState(current_date_str_bt)

  //is disable 7h - 0hUTC
  var isDisabled0hUTC = false
  if(date == current_date_str && currentDate.getUTCHours() == 12){
    isDisabled0hUTC = true
  }

  //is disable 19h - 12hUTC
  var isDisabled12hUTC = false
  if(date == current_date_str_bt && currentDate_bt.getUTCHours() == 0){
    isDisabled12hUTC = true
  }

  const [hour, setHour] =  typee == 'live' ? useState({ id: `${isDisabled0hUTC?time_calculate:default_time}h`, value: `${isDisabled0hUTC?time_calculate:default_time}h`}) 
                                            :useState({ id: `${default_time}h`, value: `${default_time}h`})     
  const [min, setMin] = useState('')
  const [center, setCenter] = useState('')
  const [max, setMax] = useState('')
  const [timeStampUTC, setTimeStampUTC] = useState()
  const [mounted, setMounted] = useState(true)

  const [mode, setMode] = useState('')


  const [validationObj, setValidationObj] = useState({min: false, center: false, max: false, minMsg: '', centerMsg: '', maxMsg: ''})  
  // const [resultString, setResultString] = useState('')

  const datePickerStyles = {
    container: {
      display: 'inline-block',
      position: 'relative'
    },
    input: {
      width: maxSm ? '140px' : '200px', // Adjust the width as per your preference
      padding: '11px',
      border: '1px solid #ccc',
      borderRadius: '4px',
      fontSize: '14px',
      backgroundColor: '#455066'
      // border: 'none'
    }
  }

  useEffect(() => {
    if(typee == 'back_test'){
      var currentDate_bt_UE = new Date();
      if(backTestInfo['Close time'] != undefined){
        currentDate_bt_UE = new Date(backTestInfo['Close time'])
      }
      var current_date_str_bt_UE = currentDate_bt_UE.toISOString().split('T')[0];
      setDate(current_date_str_bt_UE)

    }
  }, [backTestInfo])

  function getCurrentDateLive() {
    const now = new Date(endTime)
    const year = now.getFullYear()
    const month = String(now.getMonth() + 1).padStart(2, '0')
    const day = String(now.getDate()).padStart(2, '0')

    //check whether time is over 19h
    // var localTime = new Date();
    // var currentTimeUTCTimeStamp = new Date(new Date().getTime() + localTime.getTimezoneOffset() * 60000);
    // console.log('currentTimeUTCTimeStamp: '+currentTimeUTCTimeStamp)
    return `${year}-${month}-${day}`
  }
  function getCurrentDateBackTest(mode) {
    const now = new Date(mode == 'max' ? backTestInfo['Close time'] : backTestInfo['Start time'])
    const year = now.getFullYear()
    const month = String(now.getMonth() + 1).padStart(2, '0')
    const day = String(now.getDate()).padStart(2, '0')

    //check whether time is over 19h
    // var localTime = new Date();
    // var currentTimeUTCTimeStamp = new Date(new Date().getTime() + localTime.getTimezoneOffset() * 60000);
    // console.log('currentTimeUTCTimeStamp: '+currentTimeUTCTimeStamp)
    return `${year}-${month}-${day}`
  }


  // console.log('date: '+date)

  function convertUserChoiceToTimeStamp() {
    var now = new Date(date)
    var h = hour['id'].split('h')
    if (h) now.setHours(h[0])
    // console.log('now: '+now.getTime())
    var utcTime = now.getTime()
    return utcTime
  }

  useEffect(() => {
    if (resetStatus == true) {
      setMin('')
      setMax('')
      setCenter('')
      setTimeStampUTC(0)
      setDate(typee == 'live' ? (current_date_str) : (current_date_str_bt))
      setHour(typee == 'live' ? { id: `${isDisabled0hUTC?time_calculate:default_time}h`, value: `${isDisabled0hUTC?time_calculate:default_time}h`} : { id: `${default_time}h`, value: `${default_time}h`})
      setResetStatus(false)
    }
  }, [])

  useEffect(() => {
    var str = { round_time: convertUserChoiceToTimeStamp(), min: min, max: max, center: center }
    var rs = [...arraySub]
    // console.log('RS: '+rs.length)
    rs[index] = str
    dispatch(setArraySub(rs))
    setTimeStampUTC(convertUserChoiceToTimeStamp())


  }, [hour, date])

  useEffect(() => {
    //Reset hour if the date is date of endTime  
    if(date == new Date(endTime).toISOString().split('T')[0] && typee == 'live'){
      setHour({ id: `${isDisabled0hUTC?time_calculate:default_time}h`, value: `${isDisabled0hUTC?time_calculate:default_time}h`})
    }

    if(date == current_date_str_bt && typee == 'back_test'){
      setHour({ id: `${default_time}h`, value: `${default_time}h`})
    }
  }, [date])

  useEffect(() => {
    var str = { round_time: convertUserChoiceToTimeStamp(), min: min, max: max, center: center }
    var rs = [...arraySub]
    rs[index] = str
    dispatch(setArraySub(rs))
  }, [min, max, center])

  function handleSetValue(e, mode) {
    var num = e.target.value
    if(mode == 'min'){
      if(num.match(getNumberValidationRegex()) && num!=''){
        setValidationObj({...validationObj, min: false})
      }else{
        setValidationObj({...validationObj, min: true})
      }
      setMin(num)
      setMode('min')
    }
    if(mode == 'center'){
      if(num.match(getNumberValidationRegex()) && num!=''){
        setValidationObj({...validationObj, center: false})
      }else{
        setValidationObj({...validationObj, center: true})
      }
      setCenter(num)
      setMode('center')
    }
    if(mode == 'max'){
      if(num.match(getNumberValidationRegex()) && num!=''){
        setValidationObj({...validationObj, max: false})
      }else{
        setValidationObj({...validationObj, max: true})
      }
      setMax(num)
      setMode('max')
    }

  }

  useEffect(() => {
    var obj = {
      ...validationObj,
      // minMsg: parseFloat(min)>parseFloat(center) ? 'Cannot be larger than center' : parseFloat(min)>parseFloat(max) ? 'Cannot larger than max' : '',
      // centerMsg: parseFloat(center)<parseFloat(min) ? 'Cannot be smaller than min' : parseFloat(center)>parseFloat(max) ? 'Cannot larger than max' : '',
      // maxMsg: parseFloat(max)<parseFloat(min) ? 'Cannot be smaller than min' : parseFloat(max)<parseFloat(center) ? 'Cannot be smaller than center' : '',
    }
    if(mode == 'min') {
      obj = {
        ...obj,
        minMsg: (parseFloat(min)>parseFloat(center) && min.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex())) ? 'Cannot be larger than middle' : (parseFloat(min)>parseFloat(max) && min.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex())) ? 'Cannot be larger than max' : '',
        centerMsg: (parseFloat(center)>parseFloat(max) && center.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex())) ? 'Cannot be larger than max' : (parseFloat(center)>=parseFloat(min) && center.match(getNumberValidationRegex()) && min.match(getNumberValidationRegex()) && validationObj['centerMsg'] == 'Cannot be smaller than min') ? '' : validationObj['centerMsg'],
        maxMsg: (parseFloat(max)>=parseFloat(min) && max.match(getNumberValidationRegex()) && min.match(getNumberValidationRegex()) && validationObj['maxMsg'] == 'Cannot be smaller than min') ? '' : validationObj['maxMsg']
      }
    }else if(mode == 'center'){
      obj = {
        ...obj,
        centerMsg: (parseFloat(center)<parseFloat(min) && min.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex())) ? 'Cannot be smaller than min' : (parseFloat(center)>parseFloat(max) && center.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex())) ? 'Cannot be larger than max' : '',
        minMsg: (parseFloat(min)<=parseFloat(center) && min.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex()) && validationObj['minMsg'] == 'Cannot be larger than middle') ? '' : validationObj['minMsg'],
        maxMsg: (parseFloat(max)>=parseFloat(center) && max.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex()) && validationObj['maxMsg'] == 'Cannot be smaller than middle') ? '' : validationObj['maxMsg']
      }
    }else if(mode == 'max') {
      obj = {
        ...obj,
        maxMsg: (parseFloat(max)<parseFloat(min) && max.match(getNumberValidationRegex()) && min.match(getNumberValidationRegex())) ? 'Cannot be smaller than min' : (parseFloat(max)<parseFloat(center) && max.match(getNumberValidationRegex()) && center.match(getNumberValidationRegex())) ? 'Cannot be smaller than middle' : '',
        minMsg: (parseFloat(min)<=parseFloat(max) && min.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex()) && validationObj['minMsg'] == 'Cannot be larger than max') ? '' : validationObj['minMsg'],
        centerMsg: (parseFloat(center)<=parseFloat(max) && center.match(getNumberValidationRegex()) && max.match(getNumberValidationRegex()) && validationObj['centerMsg'] == 'Cannot be larger than max') ? '' : validationObj['centerMsg']
      }
    }
    setValidationObj(obj)
  }, [min, max, center])


  const handleMinusClick = () => {
    var arrCopy = [...arraySub]
    // console.log("STRTRUOC: "+JSON.stringify(arrCopy))
    arrCopy[index] = null
    dispatch(setArraySub(arrCopy))
    // console.log("STRSAU: "+JSON.stringify(arrCopy))
    setMounted(false)
  }
  if (!mounted) {
    return null // Don't render the component if it's not mounted
  }

  return (
    <div>
      <div style={{ display: 'flex', color: 'white', alignItems: 'end' }}>
        <div style={{ width: '55%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Date</div>
          <div style={datePickerStyles.container}>
            <input
              type='date'
              style={datePickerStyles.input}
              value={date}
              onChange={(e) => setDate(e.target.value)}
              max={typee == 'back_test' ? getCurrentDateBackTest('max') : ''}
              min={typee == 'live' ? getCurrentDateLive() : getCurrentDateBackTest('min')}
            />
          </div>
        </div>
        <div style={{ width: '33%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Hour</div>
          <DropDownComponent
            width={'30%'}
            listDisabled={typee == 'live' ? [isDisabled0hUTC ? { id: `${default_time}h`, value: `${default_time}h`} : {}] : [isDisabled12hUTC ? { id: `${time_calculate}h`, value: `${time_calculate}h`} : {}]}
            currentState={hour}
            setCurrentState={setHour}
            listElement={[
              { id: `${default_time}h`, value: `${default_time}h`},
              { id: `${time_calculate}h`, value: `${time_calculate}h` }
            ]}
          />
        </div>
        <div onClick={() => handleMinusClick()} className={cx('MinusIcon')}>
          <MinusIcon />
        </div>
      </div>

      <div style={{ display: 'flex', color: 'white' }}>
        <div style={{ width: '34%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Value Min</div>
          <input
            value={min}
            onChange={(e) => handleSetValue(e, 'min')}
            style={{
              width: '90%',
              background: '#455066',
              height: 40,
              padding: '10px 12px',
              borderRadius: 16,
              fontSize: 16,
              fontWeight: 500,
              border: 'none',
              color: 'white'
            }}
          />
          {
          validationObj['min'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            Must be a number
          </div> :
          validationObj['minMsg'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            {validationObj['minMsg']}
          </div>
          :''
          }
        </div>
        <div style={{ width: '34%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8}}>
            Value Middle
          </div>
          <input
            value={center}
            onChange={(e) => handleSetValue(e, 'center')}
            style={{
              width: '90%',
              background: '#455066',
              height: 40,
              padding: '10px 12px',
              borderRadius: 16,
              fontSize: 16,
              fontWeight: 500,
              border: 'none',
              color: 'white'
            }}
          />
          { 
          validationObj['center'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            Must be a number
          </div>:validationObj['centerMsg'] ?
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            {validationObj['centerMsg']}
          </div>
          :''
          }
        </div>
        <div style={{ width: '33%' }}>
          <div style={{ fontSize: 14, fontWeight: 600, color: 'white', marginTop: 16, marginBottom: 8 }}>Value Max</div>
          <input
            value={max}
            onChange={(e) => handleSetValue(e, 'max')}
            style={{
              width: '100%',
              background: '#455066',
              height: 40,
              padding: '10px 12px',
              borderRadius: 16,
              fontSize: 16,
              fontWeight: 500,
              border: 'none',
              color: 'white'
            }}
          />
          {
          validationObj['max'] ? 
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            Must be a number
          </div>:validationObj['maxMsg']?
          <div style={{fontSize: 14, fontWeight: 500, marginTop: 6, color: '#FF7373'}}>
            {validationObj['maxMsg']}
          </div>
          :''
          }
        </div>
      </div>
    </div>
  )
}

export default SubmitComponent