/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { Container /*Table*/ } from '@mui/material'
import { /*Button,*/ Col, Row } from 'antd'
import { useState } from 'react'
import { useNavigate /*useSearchParams*/ } from 'react-router-dom'
import AppModal from '@src/components/AppModal/AppModal'
import { deleteSubmission } from '@src/axios/getApiAll'
import { /*Toaster,*/ toast } from 'react-hot-toast'
import './TableModel.scss'
import { InforIcon } from '@src/assets/svgs'
import Hint from '@src/components/Hint'

export default function TableModel({
  // isOpenAddNewModal,
  // setIsOpenAddNewModal,
  // setCurrentRowItem,
  // isShowDetailModal,
  // setIsShowDetailModal,
  setCurrentItemDetailModal,
  listModalSubmission,
  setLoadListModel
}) {
  // console.log('listModalSubmission123: '+JSON.stringify(listModalSubmission))
  const [itemDelete, setItemDelete] = useState()
  const [isShowModelInfor, setIsShowModelInfor] = useState(false)
  const [isShowModalDelete, setIsShowModelDelete] = useState(false)
  const formatTime = (timeStamp) => {
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    var dateFormat = new Date(timeStamp)
    return (
      (dateFormat.getDate() >= 10 ? dateFormat.getDate() : '0' + dateFormat.getDate()) +
      ' ' +
      monthNames[dateFormat.getMonth()] +
      ' ' +
      dateFormat.getFullYear() +
      ' ' +
      (dateFormat.getHours() >= 10 ? dateFormat.getHours() : '0' + dateFormat.getHours()) +
      ':' +
      (dateFormat.getMinutes() >= 10 ? dateFormat.getMinutes() : '0' + dateFormat.getMinutes()) +
      ':' +
      (dateFormat.getSeconds() >= 10 ? dateFormat.getSeconds() : '0' + dateFormat.getSeconds())
    )

    // // Hours part from the timestamp
    // var hours = date.getHours();

    // // Minutes part from the timestamp
    // var minutes = "0" + date.getMinutes();

    // // Seconds part from the timestamp
    // var seconds = "0" + date.getSeconds();

    // // Will display time in 10:30:23 format
    // var formattedTime = hours + 'h' + minutes.substr(-2);

    // return formattedTime
  }

  const handleShowModelInfor = (value) => {
    setIsShowModelInfor(value)
  }
  // const [queryParameters] = useSearchParams()
  const navigate = useNavigate()
  //modelDetail
  const getModelDetails = (tournament, type, symbol, id, time) => {
    tournament = 'return_and_risk_tournament'
    const path = `/model?tournament=${tournament}&type=${type}&symbol=${symbol}&id=${id}&time=${time}`
    navigate(path)
    setCurrentItemDetailModal(true)
  }
  // const handelDeleteModel = (e, item) => {
  //   setItemDelete(item)
  //   setIsShowModelDelete(true)
  //   e.stopPropagation()
  // }
  const performDelete = () => {
    //{"type":"live","symbol":"BTCUSDT","model":"abcdeeeee","submitTime":1698428565000}
    //`${data['tournamentId']}/submission?submission_type=${data['submissionType']}&symbol=${data['symbol']}&model_id=${data['model']}`, {

    deleteSubmission({
      tournamentId: 'return_and_risk_tournament',
      submissionType: itemDelete['type'],
      symbol: itemDelete['symbol'],
      model: itemDelete['model']
    })
      .then(() => {
        toast.success('Delete model successfully...')
      })
      .catch(() => {
        toast.error('Delete model failed...')
      })
      .finally(() => {
        setIsShowModelDelete(false)
        setLoadListModel(true)
      })
  }
  // console.log('LISMDSMSS: '+JSON.stringify(listModalSubmission))
  return (
    <Container style={{ backgroundColor: '#181F38', borderRadius: 15, overflow: 'auto' }}>
      {/* <Toaster /> */}
      <Row style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Col
          style={{ fontSize: 18, fontWeight: 600, marginTop: 20, color: '#fff', display: 'flex', position: 'relative' }}
        >
          <div style={{display: 'flex', alignItems: 'center'}}>
            <div style={{paddingBottom: 8}}>Model</div>
            <Hint content={'Can be understood as a system learning patterns from data or simply your submission'} />
          </div>
        </Col>
        
      </Row>
      <table style={{ marginTop: 20, color: '#fff' }}>
        <thead>
          <tr style={{ color: '#9DA7BA', fontSize: 12, fontWeight: 500 }}>
            <th></th>
            <th>Token</th>
            <th>Name</th>
            <th>Stake</th>
            <th>Submit time</th>
            <th>Tournament</th>
            <th>Status</th>
            {/* <th style={{paddingLeft: 10}}>Action</th> */}
          </tr>
        </thead>
        <tbody style={{ fontSize: 14, fontWeight: 600, marginTop: 50 }}>
          {listModalSubmission &&
            listModalSubmission.length > 0 &&
            listModalSubmission.map((item, index) => {
              return (
                <tr key={index}>
                  <th>{index + 1}</th>
                  <th
                    style={{ cursor: 'pointer' }}
                    onClick={() =>
                      getModelDetails('return_and_risk_tournament', item.type, item.symbol, item.model, item.submitTime)
                    }
                  >
                    {item.symbol}
                  </th>
                  <th
                    style={{ cursor: 'pointer' }}
                    onClick={() =>
                      getModelDetails('return_and_risk_tournament', item.type, item.symbol, item.model, item.submitTime)
                    }
                  >
                    {item.model}
                  </th>
                  <th style={{ color: '#726BD3' }}>----</th>
                  <th style={{ color: '#36A9E1' }}>{formatTime(item.submitTime)}</th>
                  <th style={{ color: 'white' }}>Tournament 1</th>
                  <th>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 26,
                        padding: '5px 10px 5px 10px',
                        borderRadius: 12,
                        background: item.type == 'live' ? '#34503D' : '#482E29',
                        color: item.type == 'live' ? '#84E4A4' : '#FE7272'
                      }}
                    >
                      {item.type == 'live' ? 'Active' : 'Inactive'}
                    </div>
                  </th>
                  {/* <th
                            onClick={(e) => handelDeleteModel(e, item)}  
                          >
                            <p style={{display: 'flex', justifyContent: 'center', alignItems: 'center',
                                       height: 26, padding: '5px 10px 5px 10px', borderRadius: 12, 
                                  background: '#482E29', color:'#FE7272', cursor: 'pointer', marginLeft: 10}}>Delete</p>      
                          </th> */}
                </tr>
              )
            })}
        </tbody>
      </table>
      <AppModal
        width={500}
        height={150}
        contentStyle={{
          borderRadius: '16px',
          padding: '24px',
          border: 'none',
          backgroundColor: '#29384E'
        }}
        isOpen={isShowModalDelete}
      >
        <div
          style={{
            color: 'white',
            fontSize: 18,
            fontWeight: 550,
            textAlign: 'center',
            marginBottom: 30,
            marginTop: 10
          }}
        >
          Do you want to delete model {itemDelete ? itemDelete['model'] : ''} ?
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between', fontSize: 16, fontWeight: 600 }}>
          <div
            onClick={() => {
              setIsShowModelDelete(false)
            }}
            style={{
              cursor: 'pointer',
              width: '45%',
              paddingTop: 10,
              paddingBottom: 10,
              background: '#2F4C78',
              textAlign: 'center',
              borderRadius: 16,
              fontSize: 18,
              fontWeight: 600,
              color: 'white'
            }}
          >
            Cancel
          </div>
          <div
            onClick={() => performDelete()}
            style={{
              cursor: 'pointer',
              width: '45%',
              paddingTop: 10,
              paddingBottom: 10,
              background: '#5768DE',
              textAlign: 'center',
              borderRadius: 16,
              fontSize: 18,
              fontWeight: 600,
              color: 'white'
            }}
          >
            Delete
          </div>
        </div>
      </AppModal>
    </Container>
  )
}
