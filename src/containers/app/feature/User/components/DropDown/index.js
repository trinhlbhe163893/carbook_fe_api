/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { useState, useRef, useEffect } from 'react'
import './DropDown.scss'

const DropDownComponent = ({ listElement, currentState, setCurrentState, width, mode, backGround, listDisabled }) => {
  const [onChoose, setOnChoose] = useState(false)

  const dropDownComponentRef = useRef()
  // console.log('listDisabled: '+JSON.stringify(listDisabled))
  useEffect(() => {
    const handleClickOutSide = (e) => {
      if (dropDownComponentRef.current && !dropDownComponentRef.current.contains(e.target)) {
        setOnChoose(false)
      }
    }
    document.body.addEventListener('click', handleClickOutSide)
  }, [])

  function handleClickChooseBTC() {
    setOnChoose(!onChoose)
  }
  function onClickChooseCoin(mode, id) {
    // setSideBarMode(mode)
    setCurrentState({ id: id, value: mode })
    setOnChoose(false)
  }
  return (
    <div ref={dropDownComponentRef}>
      {/* Initial */}
      <div
        onClick={() => handleClickChooseBTC()}
        style={{
          background: backGround ? backGround : '#455066',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          cursor: 'pointer',
          color: 'white',
          paddingLeft: 12,
          paddingRight: 12,
          borderRadius: 16,
          paddingTop: 6,
          paddingBottom: 6,
          height: 40
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', fontSize: 16, fontWeight: 600, padding: 10 }}>
          <div>{currentState && currentState['value'] ? currentState['value'] : ''}</div>
        </div>
        {mode != 'noChoose' ? (
          <svg
            className={`animate-example ${onChoose ? 'rotate-animation' : ''}`}
            style={onChoose ? { transform: 'rotate(90deg)' } : {}}
            xmlns='http://www.w3.org/2000/svg'
            height='1.5em'
            viewBox='0 0 320 512'
          >
            <path
              fill='#fff'
              d='M310.6 233.4c12.5 12.5 12.5 32.8 0 45.3l-192 192c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L242.7 256 73.4 86.6c-12.5-12.5-12.5-32.8 0-45.3s32.8-12.5 45.3 0l192 192z'
            />
          </svg>
        ) : (
          ''
        )}
      </div>
      {mode != 'noChoose' && (
        <div
          className={`animate-exampleABS ${onChoose ? 'fade-in' : 'fade-out'}`}
          style={{
            position: 'absolute',
            zIndex: 100,
            background: backGround ? backGround : '#455066',
            // display: 'flex',
            // alignItems: 'center',
            // justifyContent: 'space-between',
            flexWrap: 'wrap',
            cursor: 'pointer',
            color: 'white',
            paddingLeft: 12,
            paddingRight: 12,
            borderRadius: 16,
            paddingTop: 8,
            paddingBottom: 8,
            marginTop: 10,
            // display: 'block',
            display: onChoose ? 'block' : 'none',
            width: width ? width : '90%'
            // zIndex: 100
          }}
        >
          { listDisabled ?
              listElement.map((ele, index) => {
                const check = currentState && ele['value'] == currentState['value']
                const checkDisabled = listDisabled.some(item => item['id'] == ele['id'])
                return (
                  <div
                    key={index}
                    className={checkDisabled ? '' : 'hoverDropDown'}
                    onClick={() => checkDisabled ? '' : onClickChooseCoin(ele['value'], ele['id'])}
                    style={{
                      background: check ? '#364061' : '#455066',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 10,
                      paddingRight: 10,
                      borderRadius: 8,
                      width: '100%',
                      paddingTop: 12,
                      paddingBottom: 12,
                      marginBottom: 5,
                      marginTop: 5,
                      cursor: checkDisabled ? 'not-allowed' : ''
                    }}
                  >
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: 16 }}>
                      <div style={check ? { color: '#7A88FE' } : { color: 'white' }}>{ele['value']}</div>
                    </div>
                  </div>
                )
              })
            :
              listElement.map((ele, index) => {
                const check = currentState && ele['value'] == currentState['value']
                return (
                  <div
                    key={index}
                    className='hoverDropDown'
                    onClick={() => onClickChooseCoin(ele['value'], ele['id'])}
                    style={{
                      background: check ? '#364061' : '#455066',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 10,
                      paddingRight: 10,
                      borderRadius: 8,
                      width: '100%',
                      paddingTop: 12,
                      paddingBottom: 12,
                      marginBottom: 5,
                      marginTop: 5
                    }}
                  >
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: 16 }}>
                      <div style={check ? { color: '#7A88FE' } : { color: 'white' }}>{ele['value']}</div>
                    </div>
                  </div>
                )
              })
          }
        </div>
      )}
    </div>
  )
}

export default DropDownComponent
