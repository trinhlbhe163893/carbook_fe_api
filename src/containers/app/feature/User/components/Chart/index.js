import React /*, { Component, useState, useEffect }*/ from 'react'
import Chart from 'react-apexcharts'
import './Chart.scss'
// import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export default function Chartc(props) {
  // useEffect(() => {
  //     axios.get('https://api-dev-v2.nestquant.com/metric/test-data')
  //     .then((data) => {
  //         // console.log(data)
  //         setData(data)
  //     })
  // }, [])

  // constructor(props) {
  var labelFormatter = function (value) {
    var val = value.toFixed(1)
    if (val >= 1000 || val <= -1000) {
      val = (val / 1000).toFixed(1) + 'K'
    } else {
      if (val >= 1000000 || val <= -1000000) {
        val = (val / 1000000).toFixed(1) + 'M'
      }
    }
    return val
  }
  // var labelFormatter2 = function (value) {
  //   var val = Math.abs(value).toFixed(1)
  //   if (val >= 1000) {
  //     val = '$' + (val / 1000).toFixed(1) + 'K'
  //   } else {
  //     if (val >= 1000000) {
  //       val = '$' + (val / 1000000).toFixed(1) + 'M'
  //     }
  //   }
  //   return val
  // }

  const { data } = props

  const options = {
    chart: {
      id: 'basic-bar',
      height: 150,
      parentHeightOffset: -10,
      parentW: -10,
      // background: "#161F2E",
      stacked: false,
      redrawOnParentResize: true,
      redrawOnWindowResize: true,
      type: 'line',
      toolbar: {
        show: true,
        offsetX: 0,
        offsetY: -3,
        tools: {
          download: true,
          selection: true,
          zoom: true,
          zoomin: true,
          zoomout: true,
          pan: true,
          reset: true,
          customIcons: []
        }
      },
      zoom: {
        enabled: true,
        type: 'x',
        autoScaleYaxis: true,
        zoomedArea: {
          fill: {
            color: '#90CAF9',
            opacity: 0.4
          }
        }
      },
      animations: {
        enabled: false //no animations
      }
    },
    dataLabels: {
      enabled: false
    },
    markers: {
      size: 0
    },
    grid: {
      borderColor: '#555',
      clipMarkers: false,
      yaxis: {
        lines: {
          show: true
        }
      }
    },
    stroke: {
      color: '#0D47A1',
      curve: 'smooth',
      opacity: 0.4,
      width: 2
    },

    tooltip: {
      theme: 'dark',
      followCursor: true,
      x: {
        show: true,
        // format: 'dd MMM yyyy HH:mm'
        formatter: (timeStamp) => {
          const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
          var dateFormat = new Date(timeStamp)
          return (
            (dateFormat.getDate() >= 10 ? dateFormat.getDate() : '0' + dateFormat.getDate()) +
            ' ' +
            monthNames[dateFormat.getMonth()] +
            ' ' +
            dateFormat.getFullYear() +
            ' - ' +
            (dateFormat.getHours() >= 10 ? dateFormat.getHours() : '0' + dateFormat.getHours()) +
            ':' +
            (dateFormat.getMinutes() >= 10 ? dateFormat.getMinutes() : '0' + dateFormat.getMinutes())
          )
        }
      },
      y: {
        show: true,
        formatter: function (value) {
          return value
        }
      }
    },

    // subtitle: {
    //   text: 'askjdhakjshdakjsd',
    //   align: 'center'
    // },

    legend: {
      show: true,
      labels: {
        colors: '#9DA7BA', // TEXT COLOR CAN BE CHANGED HERE
        useSeriesColors: false
      }
    },
    // labels: ['1697429611','1697688811','1697775211','1697948011','1697948011', '1698034411', '1698120811', '1698293611', '1698380011'],
    xaxis: {
      // type: 'category',
      labels: {
        // formatter: function (timeStamp) {
        //   const monthNames = ["Jan", "Feb", "Mar", 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        //   var dateFormat = new Date(Number(timeStamp));
        //   return dateFormat.getDate() +
        //     " " + monthNames[(dateFormat.getMonth())] +
        //     " " + dateFormat.getFullYear().toString().substring(2,4)
        //     + " " + dateFormat.getHours() +
        //     ":" + dateFormat.getMinutes()
        // },
        style: {
          colors: '#9DA7BA'
        }
      },
      tickAmount: 15,
      type: 'datetime'
      // tickPlacement: 'between',
      // categories: ['1327359600000','1327446000000','1327532400000','1327618800000', '1327878000000', '1327964400000'],
      // tickPlacement: 'on',

      // datetimeUTC: true,
    },
    // colors: ["#B78145", "#AAA4FA"],
    // fill: {
    //   type: "gradient",
    //   gradient: {
    //     enabled: true,
    //     shadeIntensity: 1,
    //     opacityFrom: 0.7,
    //     opacityTo: 0.9,
    //     stops: [0, 90, 100]
    //   }
    // },
    yaxis: [
      {
        forceNiceScale: false,
        // min: 0,
        // max: 100,
        tickAmount: 3,
        // axisBorder: {
        //   show: true,
        //   color: '#B78145',
        //   offsetX: 0,
        //   offsetY: 0
        // },

        // seriesName: "score",
        labels: {
          formatter: labelFormatter,
          style: {
            colors: '#9DA7BA'
          }
        },
        tooltip: {
          enabled: true
        }
      }
    ]
  }
  const series = data

  // const series = [
  //   {

  //     name: "series-1",
  //     type: "area",
  //     data: [
  //       {
  //         x: "1327359600000",
  //         y: 12.81
  //       },
  //       {
  //         x: "1327446000000",
  //         y: 34.01
  //       },
  //       {
  //         x: "1327532400000",
  //         y: 23.71
  //       },
  //       {
  //         x: "1327618800000",
  //         y: 6
  //       },
  //       {
  //         x: "1327878000000",
  //         y: 34.24
  //       },
  //       {
  //         x: "1327964400000",
  //         y: 12.53
  //       },

  //     ]
  //     // data: data,

  //     // data: [
  //     //   [1327359600000,10.4],
  //     //   [1327446000000,20.34],
  //     //   [1327532400000,10.18],
  //     //   [1327618800000,20.05],
  //     //   [1327878000000,20.8],
  //     //   [1327964400000,30.95],
  //     //   [1328050800000,10.24],
  //     //   [1328137200000,40.29]]
  //   },
  //   {

  //     name: "series-2",
  //     type: "area",
  //     data: [
  //       {
  //         x: "1327359600000",
  //         y: 11.81
  //       },
  //       {
  //         x: "1327446000000",
  //         y: 12.01
  //       },
  //       {
  //         x: "1327532400000",
  //         y: 20.71
  //       },
  //       {
  //         x: "1327618800000",
  //         y: 14
  //       },
  //       {
  //         x: "1327878000000",
  //         y: 30.24
  //       },
  //       {
  //         x: "1327964400000",
  //         y: 14.53
  //       },

  //     ]
  //     // data: data,

  //     // data: [
  //     //   [1327359600000,10.4],
  //     //   [1327446000000,20.34],
  //     //   [1327532400000,10.18],
  //     //   [1327618800000,20.05],
  //     //   [1327878000000,20.8],
  //     //   [1327964400000,30.95],
  //     //   [1328050800000,10.24],
  //     //   [1328137200000,40.29]]
  //   },
  // ]
  // const series = [{
  //   name: 'Income',
  //   type: 'column',
  //   data: [1.4, 2, 2.5, 1.5, 2.5, 2.8, 3000.8, 4000.6]
  // }, {
  //   name: 'Cashflow',
  //   type: 'column',
  //   data: [1.1, 3, 3.1, 4, 4.1, 4.9, 6000.5, 8000.5]
  // }, {
  //   name: 'Revenue',
  //   type: 'line',
  //   data: [20, 29, 37, 36, 44, 45, 5000, 5008]
  // }]
  // };
  // }

  // render() {
  // const width = window.innerWidth;

  // console.log(width);
  // const ptype = (props) =>{

  // console.log(chartStyle)
  // }

  return (
    // <div className="app">
    <div className='row'>
      <div className='mixed-chart'>
        <Chart
          options={options}
          // options={this.state.optionsLine}
          series={series}
          height='250'
        // type="line"
        // width= "800"
        />
      </div>
    </div>
    // </div>
  )
  // }
}

// export default Chartc;
