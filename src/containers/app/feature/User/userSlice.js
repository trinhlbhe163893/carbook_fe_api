import { createSlice } from '@reduxjs/toolkit'
// import { connectWallet, getUSDTBalance, deposit, withdraw, getStaked, disconnect } from '@src/redux/reducer/action'

const initialState = {
  user: {},
  team: {}
  // haveMetamask: false,
  // accountAddress: '',
  // accountBalance: '',
  // isConnected: true,
  // USDTBalance: '',
  // deposit: { type: '', mess: '' },
  // withdraw: { amount: '', address: '' },
  // staked: '',
  // value: 0
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setchangeUser: (state, action) => {
      state.user = { ...action.payload }
    },
    setPassWord: (state, action) => {
      state.user = action.payload
    },
    setchageTeam: (state, action) => {
      state.team = action.payload
    }
  }
  // extraReducers: (builder) => {
  //   builder
  //     .addCase(connectWallet.fulfilled, (state, action) => {
  //       state.haveMetamask = true
  //       state.accountAddress = action.payload
  //     })
  //     .addCase(disconnect.fulfilled, (state, action) => {
  //       state.USDTBalance = action.payload
  //       state.accountBalance = action.payload
  //       state.isConnected = false
  //       state.staked = action.payload
  //       state.withdraw = { type: '', mess: '' }
  //       state.deposit = { type: '', mess: '' }
  //       state.accountAddress = ''
  //     })

  //     .addCase(connectWallet.rejected, (state) => {
  //       state.haveMetamask = false
  //     })
  //     .addCase(getUSDTBalance.fulfilled, (state, action) => {
  //       state.USDTBalance = action.payload
  //     })
  //     .addCase(deposit.fulfilled, (state, action) => {
  //       // state.deposit.type = action.payload.type +new Date().getTime();
  //       // state.deposit.mess = action.payload.mess;
  //     })
  //     .addCase(withdraw.fulfilled, (state, action) => {
  //       // state.withdraw.type = action.payload.type +new Date().getTime();
  //       // state.withdraw.mess = action.payload.mess;
  //     })
  //     .addCase(getStaked.fulfilled, (state, action) => {
  //       state.staked = action.payload
  //     })
  // }
})

export const { increment, decrement, incrementByAmount } = userSlice.actions

export default userSlice.reducer
