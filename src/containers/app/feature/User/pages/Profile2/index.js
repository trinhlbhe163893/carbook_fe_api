/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { OverviewIcon /*UserIcon*/ } from '@src/assets/svgs'
import classNames from 'classnames/bind'
import { Link, useLocation } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid'
import styles from './Profile2.module.sass'
import { useDispatch, useSelector } from 'react-redux'
import Avatar from '@src/components/Avatar'
import { useChangeAvatarMutation } from '../../userService'
import { authApi } from '@src/containers/authentication/feature/Auth/authService'
import { setUser } from '@src/containers/authentication/feature/Auth/authSlice'
import { getImage, getUserProfile } from '@src/axios/getApiAll'
import { toast } from 'react-hot-toast'
import Resizer from 'react-image-file-resizer'
const cx = classNames.bind(styles)

const menu = [
  {
    icon: <OverviewIcon />,
    title: 'Overview',
    link: '/overview/user'
  }

  // {
  //   icon: <UserIcon />,
  //   title: 'Team',
  //   link: '/teams/user'
  // }
]

function Profile2() {
  const dispatch = useDispatch()
  const userInfo = useSelector((state) => state.auth.user)
  const [getProfile] = authApi.endpoints.getProfile.useLazyQuery()
  const [updateAvatar /*{ isLoading: isUpdating }*/] = useChangeAvatarMutation()
  const location = useLocation()

  const handleChangeAvatar = () => {
    const data = new FormData()
    var input = document.createElement('input')
    input.type = 'file'
    input.accept = 'image/png, image/jpeg, image/jpg'

    input.onchange = (e) => {
      var file = e.target.files[0]
      // var fileSubmit
      const maxWidth = 800
      const maxHeight = 600

      // Call the resizeImageFile function to compress the image
      Resizer.imageFileResizer(
        file,
        maxWidth,
        maxHeight,
        'png',
        70,
        0,
        (resizedImage) => {
          if (resizedImage) {
            data.append('image', resizedImage)
            updateAvatar(data).then((updateResponse) => {
              if (!updateResponse?.error) {
                toast.success('Update avatar successfully!')
                getProfile('a', false).then((response) => {
                  dispatch(setUser({ ...response.data }))
                })
              } else {
                toast.error('Something went wrong, please try again')
              }
            })
          }
          // Do something with the compressed image (e.g., upload or display)
          // return resizedImage
        },
        'file'
      )
    }

    input.click()
  }

  return (
    <div className={cx('profile-wrapper')}>
      {/* <Toaster position='top-center' /> */}
      <div className={cx('user-card')}>
        <div className={cx('head')}>
          <div onClick={handleChangeAvatar} className={cx('avatar')}>
            <Avatar username={userInfo?.userName} img={userInfo?.profile_image_url} />
          </div>
          <div className={cx('info')}>
            <div className={cx('__name')}>{`${userInfo?.first_name} ${userInfo?.last_name}`}</div>
            <div className={cx('__position')}>{userInfo?.team_name}</div>
          </div>
        </div>
        <ul className={cx('menu')}>
          {menu.map((item, index) => {
            return (
              <Link key={uuidv4(item.link)} to={item.link}>
                <li className={cx(location.pathname === item.link ? 'active' : '')} key={uuidv4(index)}>
                  <div className={cx('icon')}>{item.icon}</div>
                  <p>{item.title}</p>
                </li>
              </Link>
            )
          })}
        </ul>
      </div>
    </div>
  )
}

export default Profile2
