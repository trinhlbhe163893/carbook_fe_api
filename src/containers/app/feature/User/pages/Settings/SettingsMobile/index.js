/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import classNames from 'classnames/bind'
import styles from './SettingsMobile.module.sass'
import { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import { authApi } from '@src/containers/authentication/feature/Auth/authService'
import { setUser } from '@src/containers/authentication/feature/Auth/authSlice'
import { EyeClose, EyeShow } from '@src/assets/svgs'
import { Toaster, toast } from 'react-hot-toast'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'
import moment from 'moment'
import {
  useChangeTeamInformationMutation,
  useChangeUserInformationMutation,
  useChangeUserPassWordMutation
  // useLazyGetTeamInformationQuery
} from '../../../userService'
import { putChangeAPIKey, getImage, getUserProfile } from '@src/axios/getApiAll'
const cx = classNames.bind(styles)

const registerSchema = Yup.object({
  new_password: Yup.string().min(8).required(),
  rePassword: Yup.string()
    .min(8)
    .test('passwords-match', 'Passwords must match', function (value) {
      return this.parent.new_password === value
    })
    .required()
})

function SettingsMobile() {
  const [changeShow, setChangeShow] = useState(false)
  const [eyeShow, setEyeshow] = useState(false)
  const userInfo = useSelector((state) => state.auth.user)
  const [updateUser, { isLoading: isUpdating }] = useChangeUserInformationMutation()
  const [updatePassWord, { isLoading: isUpdatepassword }] = useChangeUserPassWordMutation()
  const {
    register: registerFormPwd,
    handleSubmit: handleSubmitFormPwd,
    formState: { errors: errorsPwd }
  } = useForm({ resolver: yupResolver(registerSchema) })
  const [updateTeamName] = useChangeTeamInformationMutation()
  // const [getTeamName, { data: teamif }] = useLazyGetTeamInformationQuery({})

  const {
    register,
    handleSubmit
    // formState: { errors }
  } = useForm()
  const formInput = useRef()

  const dispatch = useDispatch()
  const [getProfile] = authApi.endpoints.getProfile.useLazyQuery()
  const onSubmit = async (data, e) => {
    data = { ...data, date_of_birth: moment(date).format('DD/MM/YYYY') }
    const updateResponse = await updateUser(data)
    e.preventDefault()
    const updateTeamNameResponse = await updateTeamName({
      // team_name: teamif?.team_name,
      new_team_name: data.team_name
    })

    if (!updateResponse?.error) {
      toast.success('Update information successfully!')
      const response = await getProfile('a', false)
      if (!response?.error) {
        dispatch(setUser({ ...response.data }))
      }
      
    } else {
      toast.error('Something went wrong, please try again')
    }
  }
  const onUpdatePassword = async (data, e) => {
    e.preventDefault()
    const updatepasswordresponse = await updatePassWord({
      // username: userInfo?.username,
      old_password: data.old_password,
      new_password: data.new_password
    })

    if (!updatepasswordresponse?.error) {
      toast.success('Changes updated successully!')
      setChangeShow(!changeShow)
    } else {
      toast.error('Password went wrong, please try again')
    }
  }
  // useEffect(() => {
  //   getTeamName()
  // }, [getTeamName])
  // useEffect(() => {
  //   if (teamif) {
  //     console.log('teamif:: ')
  //   }
  // }, [teamif])

  const handleChangeAPI = () => {
    putChangeAPIKey()
      .then((data) => {
        toast.success('New API key has been sent via your email')
      })
      .catch((error) => {
        console.log('error : ' + error)
        toast.error('Failed to change API key')
      })
  }
  const datePickerStyles = {
    container: {
      display: 'inline-block',
      position: 'relative',
      color: 'white'
    },
    input: {
      width: '200px', // Adjust the width as per your preference
      padding: '8px',
      border: '1px solid #ccc',
      borderRadius: '4px',
      fontSize: '14px',
      backgroundColor: '#455066',
      // border: 'none',
      marginTop: 4
    }
  }
  const [date, setDate] = useState()
  // console.log('DATEEEEE: '+moment(new Date()).format('YYYY-MM-DD'))
  const handleDateChange = (event) => {
    setDate(event.target.value)
  }

  try {
    var arrStr = userInfo?.date_of_birth?.split('/')
    arrStr?.reverse()
    var str = ''
    arrStr?.map((it) => {
      str = str + it + '-'
    })
    str = str?.slice(0, str.length - 1)
    // console.log('str: '+str)
  } catch (error) {
    console.log('error: nodate')
  }

  return (
    <div>
      {/* <Toaster/> */}
      <div className={cx('profile-wrapper-mobile')}>
        <div className={cx('content-mobile')}>
          <div className={cx('head-mobile')}>
            <h3>Settings</h3>
          </div>
          <form onSubmit={handleSubmit(onSubmit)} className={cx('details-mobile')}>
            <div className={cx('__head-mobile')}>Profile details</div>
            <div className={cx('__info-mobile')}>
              <div className={cx('item-mobile')}>
                <p>First name</p>
                <input
                  className={cx('inputsettings-mobile')}
                  placeholder='Add First name'
                  type='text'
                  {...register('first_name')}
                  defaultValue={userInfo?.first_name}
                  style={{
                    color: '#FFFFFF'
                  }}
                ></input>
              </div>
              <div className={cx('item-mobile')}>
                <p>Last name</p>
                <input
                  className={cx('inputsettings-mobile')}
                  placeholder='Add Last name'
                  type='text'
                  {...register('last_name')}
                  defaultValue={userInfo?.last_name}
                  style={{
                    color: '#FFFFFF'
                  }}
                ></input>
              </div>
              {/* <div className={cx('item-mobile')}>
                <p>Team Name</p>
                <input
                  className={cx('inputsettings-mobile')}
                  id='team_name'
                  placeholder='Add Team name'
                  type='text'
                  {...register('team_name')}
                  defaultValue={teamif?.team_name}
                  style={{
                    color: '#FFFFFF'
                  }}
                ></input>
              </div> */}
              <div className={cx('item-mobile')}>
                <p>Location</p>
                <input
                  className={cx('inputsettings-mobile')}
                  type='text'
                  placeholder='Add a location'
                  {...register('country')}
                  defaultValue={userInfo?.country}
                  style={{
                    color: '#FFFFFF'
                  }}
                ></input>
              </div>
              <div className={cx('item-mobile')}>
                <p>Birthday</p>
                <div style={datePickerStyles.container}>
                  <input
                    type='date'
                    style={datePickerStyles.input}
                    onChange={handleDateChange}
                    value={date ? date : str ? str : ''}
                    // {...register('date_of_birth')}
                  />
                </div>
                {/* <input
                  className={cx('inputsettings-mobile')}
                  type='text'
                  placeholder='MM/DD/YYYY'
                  {...register('date_of_birth')}
                  defaultValue={userInfo?.date_of_birth}
                  style={{
                    color: '#FFFFFF'
                  }}
                ></input> */}
              </div>
            </div>
            <button ref={formInput} type='submit' style={{ display: 'none' }}></button>
          </form>
          <div
            className={cx('details-mobile')}
            style={{
              marginTop: '43px'
            }}
          >
            <div className={cx('__head-mobile')}>Sign in method</div>
            <div className={cx('__info-mobile')}>
              <div style={{ display: 'flex', alignItems: 'center' }} className={cx('item-mobile')}>
                <p>Email</p>
                <div className={cx('textInfor')}>{userInfo?.email}</div>{' '}
              </div>
              <div className={cx('item-mobile')}>
                {changeShow ? (
                  <form
                    className={cx('change-pwd-form-mobile')}
                    id='change-pwd'
                    onSubmit={handleSubmitFormPwd(onUpdatePassword)}
                  >
                    <div className={cx('content-input-mobile')}>
                      <div
                        style={{
                          width: '100%',
                          position: 'relative'
                        }}
                      >
                        <div className={cx('title-input-mobile')}>Current password</div>
                        <input
                          id='old_password'
                          {...registerFormPwd('old_password', { required: 'old_password is required' })}
                          placeholder='Enter your password'
                          type={eyeShow === true ? 'text' : 'password'}
                          // onChange={(e) => setPassword(e.target.value)}
                          className={cx('input-changepassword-mobile')}
                        />

                        <div className={cx('eye-icon-mobile')} onClick={() => setEyeshow(!eyeShow)}>
                          {eyeShow ? <EyeShow /> : <EyeClose />}
                        </div>
                        {errorsPwd.password && (
                          <p
                            style={{
                              fontFamily: 'Montserrat',
                              color: '#ff0000',
                              fontSize: '12px'
                            }}
                            role='alert'
                          >
                            {errorsPwd.password?.message}
                          </p>
                        )}
                      </div>
                      <div
                        style={{
                          width: '100%',
                          position: 'relative'
                        }}
                      >
                        <div className={cx('title-input-mobile')}>New password</div>
                        <input
                          id='new_password'
                          {...registerFormPwd('new_password', { required: 'new_password is required' })}
                          placeholder='Enter new password'
                          type={eyeShow === true ? 'text' : 'password'}
                          className={cx('input-changepassword-mobile')}
                          // onChange={(e) => setNewPassword(e.target.value)}
                        />

                        <div className={cx('eye-icon-mobile')} onClick={() => setEyeshow(!eyeShow)}>
                          {eyeShow ? <EyeShow /> : <EyeClose />}
                        </div>
                        {errorsPwd.password && (
                          <p
                            style={{
                              fontFamily: 'Montserrat',
                              color: '#ff0000',
                              fontSize: '12px'
                            }}
                            role='alert'
                          >
                            {errorsPwd.password?.message}
                          </p>
                        )}
                      </div>
                      <div
                        style={{
                          width: '100%',
                          position: 'relative'
                        }}
                      >
                        <div className={cx('title-input-mobile')}>Confirm new password</div>
                        <input
                          id='rePassword'
                          {...registerFormPwd('rePassword', { required: 'rePassword is required' })}
                          placeholder='Enter new password'
                          type={eyeShow === true ? 'text' : 'password'}
                          className={cx('input-changepassword-mobile')}
                          // onChange={(e) => setNewPassword(e.target.value)}
                        />
                        <div className={cx('eye-icon-mobile')} onClick={() => setEyeshow(!eyeShow)}>
                          {eyeShow ? <EyeShow /> : <EyeClose />}
                        </div>{' '}
                        {errorsPwd.rePassword && (
                          <p
                            style={{
                              fontFamily: 'Montserrat',
                              color: '#ff0000',
                              fontSize: '12px'
                            }}
                            role='alert'
                          >
                            {errorsPwd.rePassword?.message}
                          </p>
                        )}
                      </div>
                    </div>
                    <div
                      className={cx('buttonchanges-mobile')}
                      style={{
                        // marginTop: '14px',
                        marginBottom: '5px',
                        justifyContent: 'flex-start'
                      }}
                    >
                      <button
                        type='submit'
                        disabled={isUpdatepassword}
                        className={cx('buttonsave-mobile')}
                        form='change-pwd'
                      >
                        Update password
                      </button>
                      <div className={cx('buttondiscard-mobile')} onClick={() => setChangeShow(!changeShow)}>
                        Cancel
                      </div>
                    </div>
                  </form>
                ) : (
                  <div style={{ display: 'flex', alignItems: 'center' }} className={cx('item-mobile')}>
                    <p>Password</p>
                    <p
                      style={{
                        display: 'flex',
                        margin: '0'
                      }}
                    >
                      <div
                        style={{
                          display: 'flex'
                        }}
                      >
                        <div
                          style={{
                            marginTop: '10px'
                          }}
                        >
                          <div
                            className={cx('inputsettings-mobile')}
                            style={{
                              color: '#FFFF'
                            }}
                          >
                            *********
                          </div>

                          <button className={cx('changeif-mobile')} onClick={() => setChangeShow(!changeShow)}>
                            Change password
                          </button>
                        </div>
                      </div>
                    </p>
                  </div>
                )}
              </div>
              <div style={{ display: 'fleFx', alignItems: 'center' }} className={cx('item-mobile')}>
                <p>API key</p>
                <p
                  style={{
                    display: 'flex',
                    margin: '0'
                  }}
                >
                  <div
                    style={{
                      display: 'flex'
                    }}
                  >
                    <div
                      style={{
                        marginTop: '10px'
                      }}
                    >
                      <button className={cx('changeif-mobile')} onClick={() => handleChangeAPI()}>
                        Change API key
                      </button>
                    </div>
                  </div>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className={cx('buttonchanges-mobile')}>
          <div>
            <button
              onClick={() => {
                formInput.current.click()
              }}
              type='submit'
              disabled={isUpdating}
              className={cx('buttonsave-mobile')}
            >
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SettingsMobile
