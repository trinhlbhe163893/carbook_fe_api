import { Col } from 'antd'
import SettingsMobile from '../SettingsMobile'
import SettingsPC from '../SettingsPC'
// import { Toaster, toast } from 'react-hot-toast'

function Settings() {
  return (
    <div>
      {/* <Toaster /> */}
      <Col lg={24} md={0} xs={0} sm={0}>
        <SettingsPC />
      </Col>
      <Col lg={0} md={24} xs={24} sm={24}>
        <SettingsMobile />
      </Col>
    </div>
  )
}
export default Settings
