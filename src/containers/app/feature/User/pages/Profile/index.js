/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import classNames from 'classnames/bind'
import styles from './Profile.module.sass'
import { v4 as uuidv4 } from 'uuid'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { OverviewIcon, SettingIcon, SignOutIcon /*UserIcon*/ } from '@src/assets/svgs'
import { useDispatch, useSelector } from 'react-redux'
import { logout, setUser } from '@src/containers/authentication/feature/Auth/authSlice'
import Cookies from 'universal-cookie'
import Avatar from '@src/components/Avatar'
import { toast } from 'react-hot-toast'
import { useChangeAvatarMutation } from '../../userService'
import { authApi } from '@src/containers/authentication/feature/Auth/authService'

import Resizer from 'react-image-file-resizer'
import { getUserProfile } from '@src/axios/getApiAll'
const cookies = new Cookies()

const cx = classNames.bind(styles)

const menu = [
  {
    icon: <OverviewIcon />,
    title: 'Overview',
    link: '/overview'
  },
  {
    icon: <SettingIcon />,
    title: 'Settings',
    link: '/settings'
  }
  // {
  //   icon: <UserIcon />,
  //   title: 'Teams',
  //   link: '/teams'
  // }
]

function Profile() {
  const location = useLocation()
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const userInfo = useSelector((state) => state.auth.user)
  const [getProfile] = authApi.endpoints.getProfile.useLazyQuery()
  const [updateAvatar /*{ isLoading: isUpdating }*/] = useChangeAvatarMutation()
  const handleLogout = () => {
    dispatch(logout())
    cookies.remove('access_token',{
      path: '/',
      domain: process.env.DOMAIN
    })
    cookies.remove('access_token_signal', {
      path: '/',
      domain: process.env.DOMAIN
    })
    
    try {
      if (cookies.get('current_page') == process.env.SIGNAL_PAGE) {
        cookies.remove('current_page', {
          path: '/',
          domain: process.env.DOMAIN
        })
        navigate(`//${process.env.SIGNAL_PAGE}`)
      } else {
        navigate('/')
      }
    } catch (error) {
      console.log("errorGetCurrentPage: " + error)
      navigate('/')
    }
  }

  // const compressImage = (file) => {
  //   // const file = e.target.files[0];

  //   // Define the desired width and height for the compressed image
  //   const maxWidth = 800
  //   const maxHeight = 600

  //   // Call the resizeImageFile function to compress the image
  //   Resizer.imageFileResizer(
  //     file,
  //     maxWidth,
  //     maxHeight,
  //     'JPEG',
  //     70,
  //     0,
  //     (resizedImage) => {
  //       console.log(resizedImage)
  //       // Do something with the compressed image (e.g., upload or display)
  //       // return resizedImage
  //     },
  //     'blob'
  //   )
  // }

  const handleChangeAvatar = () => {
    const data = new FormData()
    var input = document.createElement('input')
    input.type = 'file'
    input.accept = 'image/png, image/jpeg, image/jpg'

    input.onchange = (e) => {
      var file = e.target.files[0]
      // var fileSubmit
      const maxWidth = 800
      const maxHeight = 600

      // Call the resizeImageFile function to compress the image
      Resizer.imageFileResizer(
        file,
        maxWidth,
        maxHeight,
        'png',
        70,
        0,
        (resizedImage) => {
          if (resizedImage) {
            data.append('image', resizedImage)
            updateAvatar(data).then((updateResponse) => {
              if (!updateResponse?.error) {
                toast.success('Update avatar successfully!')
                getProfile('a', false).then((response) => {
                  dispatch(setUser({ ...response.data }))
                })
              } else {
                toast.error('Something went wrong, please try again')
              }
            })
          }
          // Do something with the compressed image (e.g., upload or display)
          // return resizedImage
        },
        'file'
      )
    }

    input.click()
  }
  return (
    <div className={cx('profile-wrapper')}>
      <div className={cx('user-card')}>
        <div className={cx('head')}>
          <div onClick={handleChangeAvatar} className={cx('avatar')}>
            <Avatar username={userInfo?.userName} img={userInfo?.profile_image_url} />
          </div>
          {/* <div className={cx('avatar')}>{userInfo?.username && userInfo?.username[0]?.toUpperCase()}</div> */}
          <div className={cx('info')}>
            <div className={cx('__name')}>{userInfo?.userName}</div>
            {userInfo?.last_name ? (
              <div className={cx('__position')}>
                {userInfo?.first_name} {userInfo?.last_name}
              </div>
            ) : null}
          </div>
        </div>
        <ul className={cx('menu')}>
          {menu.map((item, index) => {
            return (
              <Link key={uuidv4(item.link)} to={item.link}>
                <li className={cx(location.pathname === item.link ? 'active' : '')} key={uuidv4(index)}>
                  <div className={cx('icon')}>{item.icon}</div>
                  <p>{item.title}</p>
                </li>
              </Link>
            )
          })}
        </ul>
        <button onClick={handleLogout} className={cx('sign-out')}>
          <p>Sign out</p>
          <SignOutIcon />
        </button>
      </div>
    </div>
  )
}

export default Profile
