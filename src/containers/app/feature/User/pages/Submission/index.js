/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import 'bootstrap/dist/css/bootstrap.min.css'
import { Col, Row } from 'antd'
import classNames from 'classnames/bind'
import Analyze from '../../components/Analyze'
import History from '../../components/History'
import ModelTable from '../../components/ModelTable/ModelTable'
import ScoreChart from '../../components/ScoreChart'
import styles from './Submission.module.sass'
import CurrentRound from '../../components/CurrenRound'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { userApi } from '../../userService'
import { useEffect, useRef, useState } from 'react'
import { SUBMISSION_TYPE } from '@src/configs'
import { isEmptyValue } from '@src/helpers/check'
import { Container } from '@mui/material'
import Modal from 'react-bootstrap/Modal'
import AppButton from '@src/components/AppButton'
import AppModal from '@src/components/AppModal/AppModal'

const cx = classNames.bind(styles)
const tournamentId = process.env.TOURNAMENT_ID

function Submission() {
  const maxSm = useMediaQuery('(max-width: 992px)')

  // const { data: currentRound } = userApi.endpoints.getCurrentRound.useQuery(tournamentId)
  const [getModelPerformance, { data: modelPerformance, isLoading: isGettingModelPerformance }] =
    userApi.endpoints.getModelPerformance.useLazyQuery()
  const [getSubmissionMeanScores, { data: submissionMeanScores, isLoading: isGettingSubmissionMeanScores }] =
    userApi.endpoints.getSubmissionMeanScores.useLazyQuery()

  const [
    getSubmissionMeanScoresBacktest,
    { data: submissionMeanScoresBacktest, isLoading: isGettingSubmissionMeanScoresBacktest }
  ] = userApi.endpoints.getSubmissionMeanScores.useLazyQuery()

  // console.log('currentRound::', currentRound)

  const reloadBacktest = () => {
    getSubmissionMeanScoresBacktest(
      {
        path: { tournamentId: tournamentId, submission: SUBMISSION_TYPE.BACKTEST },
        params: {}
      },
      false
    )
  }
  // const reloadData = () => {
  //   if (currentRound)
  //     getSubmissionMeanScores(
  //       {
  //         path: { tournamentId: tournamentId, submission: currentRound['Current round'] },
  //         params: {}
  //       },
  //       false
  //     )
  // }

  // useEffect(() => {
  //   if (currentRound)
  //     getSubmissionMeanScores(
  //       {
  //         path: { tournamentId: tournamentId, submission: currentRound['Current round'] },
  //         params: {}
  //       },
  //       false
  //     )

  //   getSubmissionMeanScoresBacktest(
  //     {
  //       path: { tournamentId: tournamentId, submission: SUBMISSION_TYPE.BACKTEST },
  //       params: {}
  //     },
  //     false
  //   )
  // }, [])

  // useEffect(() => {
  //   if (!isEmptyValue(submissionMeanScores) && currentRound) {
  //     getModelPerformance(
  //       {
  //         path: { tournamentId: process.env.TOURNAMENT_ID, submission: currentRound['Current round'] },
  //         params: { submission_time: submissionMeanScores[0]['Submission Time'] }
  //       },
  //       false
  //     )
  //   }
  // }, [submissionMeanScores])

  // var da = [{ 'Submission Time': '12h dem', 'Submission Time': '12h trua' }]

  return (
    <>
      {/* {
        isShowDetailModal ? 
        <div style={{position: 'absolute', top: 81, width: '100%', height: 60, background: 'rgba(54, 169, 225, 0.30)', padding: '0 86px', }}>
          <div className='hide' style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', height: '100%', fontSize: 18, fontWeight: 600, color: 'white'}}>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <div>Get ready! Unprecedented signals are coming to NestQuant version 2!</div>
              <div style={{width: 6, height: 6, borderRadius: 3, background: '#9DA7BA', margin: '0 14px'}}></div>
              <div style={{marginRight: 24}}>Get ready! Unprecedented signals are coming</div>
            </div>
            <div style={{width: 170, height: 40, display: 'flex', justifyContent: 'center', alignItems: 'center',padding: '10px 16px', fontWeight: 600, height: 40, width: 170, background: '#5668DF',borderRadius: 16,cursor: 'pointer'}}> 
              <div style={{fontSize: 16, marginRight: 4}}>Set reminder</div>
              <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fillRule="evenodd" clipRule="evenodd" d="M2 16.5959C2 16.2151 2.15471 15.8506 2.42864 15.586L3.45759 14.5922C3.84928 14.2139 4.06977 13.6922 4.06814 13.1476L4.05867 9.9946C4.04543 5.58319 7.61789 2 12.0293 2C16.4314 2 20 5.56859 20 9.97067L20 13.1716C20 13.702 20.2107 14.2107 20.5858 14.5858L21.5858 15.5858C21.851 15.851 22 16.2107 22 16.5858C22 17.3668 21.3668 18 20.5858 18H16C16 20.2091 14.2091 22 12 22C9.79086 22 8 20.2091 8 18H3.40408C2.62863 18 2 17.3714 2 16.5959ZM10 18C10 19.1046 10.8954 20 12 20C13.1046 20 14 19.1046 14 18H10ZM18 13.1716C18 14.2324 18.4214 15.2499 19.1716 16L4.87851 16C5.64222 15.246 6.07136 14.2161 6.06813 13.1416L6.05867 9.9886C6.04875 6.6841 8.7248 4 12.0293 4C15.3268 4 18 6.67316 18 9.97067L18 13.1716Z" fill="white"/>
              </svg>
            </div>
          </div>
        </div>
        :null
      } */}

      <div className={cx('submission-wrapper')}>
        <Container>
          {maxSm ? (
            <Row gutter={[24, 24]}>
              <Col xs={24}>
                <CurrentRound
                  reloadData={{}}
                  isLoading={isGettingSubmissionMeanScores}
                  inputData={submissionMeanScores}
                  // currentRound={currentRound}
                />
              </Col>
              <Col xs={24}>
                <ScoreChart isLoading={isGettingModelPerformance} data={modelPerformance} />
              </Col>
              <Col xs={24}>
                <Analyze data={submissionMeanScores} />
              </Col>
              <Col xs={24}>
                <ModelTable
                  reloadBacktest={reloadBacktest}
                  isLoading={isGettingSubmissionMeanScoresBacktest}
                  data={submissionMeanScoresBacktest}
                />
              </Col>
              <Col xs={24}>
                <History
                  // currentRound={currentRound}
                  reloadData={{}}
                  // data={submissionMeanScores}
                  data={{}}
                  isLoading={isGettingSubmissionMeanScores}
                />
              </Col>
            </Row>
          ) : (
            <Row gutter={32}>
              <Col xs={16}>
                <ScoreChart isLoading={isGettingModelPerformance} data={modelPerformance} />
                <ModelTable
                  reloadBacktest={reloadBacktest}
                  isLoading={isGettingSubmissionMeanScoresBacktest}
                  data={submissionMeanScoresBacktest}
                />
              </Col>
              <Col xs={8}>
                <Analyze
                  // currentRound={currentRound}
                  data={submissionMeanScores}
                />
                <CurrentRound
                  reloadData={{}}
                  isLoading={isGettingSubmissionMeanScores}
                  inputData={submissionMeanScores}
                  // currentRound={currentRound}
                />
                <div className={cx('history')}>
                  <History
                    // currentRound={currentRound}
                    reloadData={{}}
                    data={submissionMeanScores}
                    isLoading={isGettingSubmissionMeanScores}
                  />
                </div>
              </Col>
            </Row>
          )}
        </Container>
      </div>
    </>
  )
}

export default Submission
