/* eslint-disable no-undef */
/* eslint-disable no-empty */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { Col, Row } from 'antd'
import { Button, Container, Table } from '@mui/material'
import TableModel from '../../components/TableModel'
import CurrentRound from '../../components/CurrenRound'
import { userApi } from '../../userService'
import History from '../../components/History'
import Analyze from '../../components/Analyze'
import { useEffect, useRef, useState } from 'react'
import AppModal from '@src/components/AppModal/AppModal'
import tronlink_wallet from '../../../../../../../src/assets/images/tronlink_wallet.png'
import { connectWallet /*getUSDTBalance, staked*/ } from '@src/redux/reducer/action'
import { useSelector, useDispatch } from 'react-redux'
import DropDownComponent from '../../components/DropDown'
// import axios from 'axios'
import SubmitComponent from '../../components/CurrenRound/SubmitComponent'
import Hint from '@src/components/Hint'

import {
  getCurrentRound,
  getNextRound,
  getSubmissionTimeAll,
  getDataChart,
  // getSymbolByTournamentId,
  // getTournamentAll,
  deleteSubmission,
  checkRegiterTournament,
  regiserTournament
} from '@src/axios/getApiAll'
// import moment from 'moment'
// import Chart from '../../components/Chart'
// import './Model.module.scss'
// import styles from './Model.module.sass'
// import Chart from '../../components/Chart'
import './Model.scss'
import { /*json,*/ useNavigate, useSearchParams } from 'react-router-dom'
import TableRsuite from '../../components/TableRsuite'
import Chartc from '../../components/Chart'
import AppButton from '@src/components/AppButton'
import { toast } from 'react-hot-toast'
import useMediaQuery from '@src/hooks/useMediaQuery'
import ReactLoading from 'react-loading'
// import { object } from 'yup'

export default function Model() {
  const navigate = useNavigate()
  const maxSm = useMediaQuery('(max-width: 992px)')
  // const tournamentId = process.env.TOURNAMENT_ID

  const dispatch = useDispatch()
  const [queryParameters] = useSearchParams()

  // const { data: currentRound } = userApi.endpoints.getCurrentRound.useQuery(tournamentId)
  const [getSubmissionMeanScores, { data: submissionMeanScores, isLoading: isGettingSubmissionMeanScores }] =
    userApi.endpoints.getSubmissionMeanScores.useLazyQuery()
  // const cx = classNames.bind(styles)
  const [isOpenAddNewModal, setIsOpenAddNewModal] = useState(false)
  const [isOpenConnectWalletModal, setIsOpenConnectWalletModal] = useState(false)
  const [newModalName, setNewModalName] = useState('')
  const [currentRowItem, setCurrentRowItem] = useState(null)

  const [isClickedConnectWallet, setIsClickedConnectWallet] = useState(false)

  const [currentItemDetailModal, setCurrentItemDetailModal] = useState(false)

  const [isShowModalDelete, setIsShowModelDelete] = useState(false)

  //State dai
  const [isShowDetailModal, setIsShowDetailModal] = useState(true)
  // const [isCorrelation, setIsCorrelation] = useState(false)
  // const [correlationMode, setCorrelationMode] = useState('correlation')
  // const [tabStakeRelease, setTabStakeRelease] = useState('stake')
  // const [isOpenStakeModal, setIsOpenStakeModal] = useState(false)
  // const [stakeAmount, setStakeAmount] = useState('')
  // const [releaseAmount, setReleaseAmount] = useState('')

  const [competition, setCompetition] = useState('Tournament 1')
  const [isShowHistory, setIsShowHistory] = useState(false)

  const [startTime, setStartTime] = useState('')
  const [roundNumber, setRoundNumber] = useState('')
  const [endTime, setEndTime] = useState('')

  const [listModalSubmission, setListModalSubmission] = useState([])
  const [myModelSubmitted, setMyModelSubmitted] = useState('')
  const [listModel, setListModel] = useState([])
  const [listModelBackTest, setListModelBackTest] = useState([])
  const [loadListModel, setLoadListModel] = useState(false)
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn)
  //dataDetailsModel
  const [dataDetailsModel, setDataDetailsModel] = useState([])
  const [dataDetailsModelChart, setDataDetailsModelChart] = useState([])

  const [isOpenSubmitModelInDetailModel, setIsOpenSubmitModelInDetailModel] = useState(false)
  const [isOpenSubmitModelInDetailModelAnalyze, setIsOpenSubmitModelInDetailModelAnalyze] = useState(false)
  const [dataLastModelDetailsChart, setDataLastModelDetailsChart] = useState([])
  const [dataPredictedResult, setDataPredictedResult] = useState([])
  const [averageMovementScore, setAverageMovementScore] = useState(0)
  const [modelDetailsAverageMovementScore, setModelDetailsAverageMovementScore] = useState(0)

  const [currentSymbol, setCurrentSymbol] = useState('')
  const [currentSymbolBackTest, setCurrentSymbolBackTest] = useState('')

  const [loadingScore, setLoadingScore] = useState(false)
  const [loadingChartModelDetail, setLoadingChartModelDetail] = useState(false)
  const [loadingDelete, setLoadingDelete] = useState(false)
  const [loadingSubmitNow, setLoadingSubmitNow] = useState(false)
  const [loadingTableModel, setLoadingTableModel] = useState(false)
  //Ref
  const correlationRef = useRef()
  // const closeModal = useRef(null)
  //regiser tournament
  useEffect(() => {
    if (isLoggedIn)
      checkRegiterTournament({ tournament_search_id: 'return_and_risk_tournament' })
        .then((data) => {
          if (data['registered_status'] === false) {
            regiserTournament({ tournament_search_id: 'return_and_risk_tournament' })
          }
        })
        .catch((error) => {
          console.log('errorcheckRegiterTournament: ' + error)
        })
  }, [])
  try {
    if (!queryParameters.get('time') && isShowModalDelete == true) {
      setIsShowModelDelete(false)
    }
  } catch (error) {}

  // Effect: Use to close modal when click outside of it
  useEffect(() => {
    const closeDropDown = (e) => {
      //resolutionRef.current.contains(e.target)
      if (correlationRef.current && !correlationRef.current.contains(e.target)) {
        setIsCorrelation(false)
      }
    }

    document.body.addEventListener('click', closeDropDown)

    return () => document.body.removeEventListener('click', closeDropDown)
  }, [])
  // console.log('cookie: '+token)
  const formatTime = (timeStamp) => {
    var dateFormat = new Date(timeStamp)
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    var minute =
      dateFormat.getMinutes().toString().length == 1 ? '0' + dateFormat.getMinutes() : dateFormat.getMinutes()
    return (
      dateFormat.getHours() +
      'h' +
      minute +
      ' ' +
      (dateFormat.getDate() >= 10 ? dateFormat.getDate() : '0' + dateFormat.getDate()) +
      ' ' +
      monthNames[dateFormat.getMonth()]
    )
  }
  //Call Api
  useEffect(() => {
    var listSub = []

    // var listMs = []
    var listCorr15 = []
    var listMs15 = []

    var dataLastModelDetailsChart = []
    var lastModel = {}
    var averageMovementScore = 0
    var count = 0
    //Start Time
    getNextRound({ tournament_search_id: 'return_and_risk_tournament' })
      .then((data) => {
        // console.log('data123: '+JSON.stringify(data))
        setStartTime(data['Start time'])
      })
      .catch((error) => {
        console.log('errorgetNextRound: ' + error)
      })
    //Close Time
    getNextRound({ tournament_search_id: 'return_and_risk_tournament' })
      .then((data) => {
        // console.log('data123: '+JSON.stringify(data))
        setEndTime(data['Close time'])
        setRoundNumber(data['Round number'])
      })
      .catch((error) => {
        console.log('errorgetNextRound: ' + error)
      })
    //submission time all
    setLoadingTableModel(true)
    getSubmissionTimeAll({ tournament_search_id: 'return_and_risk_tournament' })
      .then((data) => {
        // console.log('data123: '+JSON.stringify(data))
        var keys = Object.keys(data)
        var obj
        var four
        var listSubmit
        var listModel
        var keys2
        keys.map((item2) => {
          keys2 = Object.keys(data[item2])

          // console.log('1: '+item2)
          // console.log('2' +keys2)
          // console.log('lintinh: '+JSON.stringify(data[item2][keys2]))

          // console.log('3: '+listModel)
          // console.log('type 3: '+ typeof listModel)
          keys2.map((keys2Item) => {
            // console.log('keys2Item: '+JSON.stringify(keys2Item))
            listModel = Object.keys(data[item2][keys2Item])
            // console.log('listModel: '+listModel)
            listModel.map((item3) => {
              // console.log('item3: '+item3)
              listSubmit = Object.keys(data[item2][keys2Item][item3])
              // console.log('listSubmit: '+listSubmit)
              listSubmit.map((item4) => {
                four = data[item2][keys2Item][item3][item4]
                // var last = data[item2][keys2Item][item3][four]

                // console.log('4: '+four)
                obj = { type: item2, symbol: keys2Item, model: item3, submitTime: four }
                // console.log('obj = '+JSON.stringify(obj))
                // setListModalSubmission([...listModalSubmission, obj])
                // console.log('listModalSubmissionSet = '+JSON.stringify(listModalSubmission))

                if ((four > lastModel.submitTime && item2 === 'live') || Object.keys(lastModel) == 0) {
                  // console.log("four: " + four + "  listS: " + JSON.stringify(listSub[listSub.length - 1]))
                  lastModel = obj
                }
                listSub.push(obj)
                setListModalSubmission(listSub)
              })
            })
          })
        })
        setAverageMovementScore(0)
        setDataLastModelDetailsChart([])

        setLoadingScore(true)
        getDataChart({
          tournament_search_id: 'return_and_risk_tournament',
          submission_type: lastModel.type,
          symbol: lastModel.symbol,
          model_id: lastModel.model,
          submission_time: lastModel.submitTime
        })
          .then((data) => {
            var keys = Object.keys(data)
            var ms15
            var corr15
            var five
            var four
            var listSubmit
            var listModel
            var keys2
            keys.map((item2) => {
              keys2 = Object.keys(data[item2])
              listModel = Object.keys(data[item2][keys2])
              listModel.map((item3) => {
                listSubmit = Object.keys(data[item2][keys2][item3])
                listSubmit.map((item4) => {
                  four = Object.keys(data[item2][keys2][item3][item4].Data)
                  four.map((item5) => {
                    five = data[item2][keys2][item3][item4].Data[item5]
                    if (five[1]) {
                      averageMovementScore += five[1]
                    }
                    count++
                    if (five[2] != null) {
                      corr15 = { x: five[0], y: five[2].toFixed(4) }
                      listCorr15.push(corr15)
                    }
                    if (five[3] != null) {
                      ms15 = { x: five[0], y: five[3].toFixed(4) }
                      listMs15.push(ms15)
                    }
                  })
                })
              })
            })
            var dataChart = {}
            dataChart = { name: 'CORR15', type: 'line', data: listCorr15 }
            dataLastModelDetailsChart.push(dataChart)
            dataChart = { name: 'MS15', type: 'line', data: listMs15 }
            dataLastModelDetailsChart.push(dataChart)
            if (averageMovementScore != 0) {
              setAverageMovementScore((averageMovementScore / count).toFixed(4))
            }
            setDataLastModelDetailsChart(dataLastModelDetailsChart)
            setLoadingScore(false)
          })
          .catch((error) => {
            console.log('DataChartError: ' + error)
            setLoadingScore(false)
          })
          setLoadingTableModel(false)
      })
      .catch((error) => {
        setLoadingTableModel(false)
        setLoadingScore(false)
      })
    setLoadListModel(false)
  }, [loadListModel])
  // console.log('lisSUB: '+JSON.stringify(listModalSubmission))
  //get data chart
  useEffect(() => {
    if (queryParameters.get('time') != null) {
      const formatTime = (timeStamp) => {
        const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        var dateFormat = new Date(timeStamp)
        return (
          (dateFormat.getDate() >= 10 ? dateFormat.getDate() : '0' + dateFormat.getDate()) +
          ' ' +
          monthNames[dateFormat.getMonth()] +
          ' ' +
          dateFormat.getFullYear() +
          ' ' +
          (dateFormat.getHours() >= 10 ? dateFormat.getHours() : '0' + dateFormat.getHours()) +
          ':' +
          (dateFormat.getMinutes() >= 10 ? dateFormat.getMinutes() : '0' + dateFormat.getMinutes()) +
          ':' +
          (dateFormat.getSeconds() >= 10 ? dateFormat.getSeconds() : '0' + dateFormat.getSeconds())
        )
      }
      // const dateFormat = (timeStamp) => {
      //   const monthNames = ["Jan", "Feb", "Mar", 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      //   var dateFormat = new Date(Number(timeStamp));
      //   return (dateFormat.getDate() > 10 ? dateFormat.getDate() : "0" + dateFormat.getDate()) +
      //     " " + monthNames[(dateFormat.getMonth() + 1)] +
      //     " " + dateFormat.getFullYear().toString()
      // }
      var averageMovementScore = 0
      var count = 0
      var listCorr15 = []
      var listMs15 = []
      var listPrediction = []
      var listGroundTruth = []
      var dataModelDetailsChart = []
      var dataModelDetailsChartPredictedResult = []
      var dataModelDetails = []
      setLoadingChartModelDetail(true)
      setModelDetailsAverageMovementScore(0)
      setDataPredictedResult([])
      setDataDetailsModel([])
      setDataDetailsModelChart([])

      getDataChart({
        tournament_search_id: queryParameters.get('tournament'),
        submission_type: queryParameters.get('type'),
        symbol: queryParameters.get('symbol'),
        model_id: queryParameters.get('id'),
        submission_time: queryParameters.get('time')
      })
        .then((data) => {
          var keys = Object.keys(data)
          var keys2
          var listModel
          var listSubmit
          var four
          var five
          var obj
          var corr15
          var ms15
          var prediction
          var groundTruth
          keys.map((item2) => {
            keys2 = Object.keys(data[item2])
            listModel = Object.keys(data[item2][keys2])
            listModel.map((item3) => {
              listSubmit = Object.keys(data[item2][keys2][item3])
              listSubmit.map((item4) => {
                four = Object.keys(data[item2][keys2][item3][item4].Data)
                four.map((item5) => {
                  five = data[item2][keys2][item3][item4].Data[item5]
                  //  time = new Date(five[0])
                  obj = {
                    round: Number(item5) + 1,
                    date: formatTime(five[0]),
                    prediction: five[4] ? five[4].toFixed(4) : five[4],
                    movementScore: five[1],
                    corr15: five[2] ? five[2].toFixed(6) : five[2],
                    ms15: five[3] ? five[3].toFixed(6) : five[3]
                  }
                  if (five[2] != null) {
                    corr15 = { x: five[0], y: five[2].toFixed(4) }
                    listCorr15.push(corr15)
                  }
                  if (five[3] != null) {
                    ms15 = { x: five[0], y: five[3].toFixed(4) }
                    listMs15.push(ms15)
                  }
                  if (five[5]) {
                    prediction = { x: five[0], y: five[4].toFixed(4) }
                    listPrediction.push(prediction)
                  }
                  if (five[5]) {
                    groundTruth = { x: five[0], y: five[5].toFixed(4) }
                    listGroundTruth.push(groundTruth)
                  }
                  if (five[1]) {
                    averageMovementScore += five[1]
                  }
                  count++

                  dataModelDetails.push(obj)
                })
              })
            })
          })

          var dataChart = {}
          var dataChartPredictedResult = {}
          dataChartPredictedResult = { name: 'Ground Truth', type: 'line', data: listGroundTruth }
          dataModelDetailsChartPredictedResult.push(dataChartPredictedResult)
          dataChartPredictedResult = { name: 'Prediction', type: 'line', data: listPrediction }
          dataModelDetailsChartPredictedResult.push(dataChartPredictedResult)
          setDataPredictedResult(dataModelDetailsChartPredictedResult)

          dataChart = { name: 'CORR15', type: 'line', data: listCorr15 }
          dataModelDetailsChart.push(dataChart)
          dataChart = { name: 'MS15', type: 'line', data: listMs15 }
          dataModelDetailsChart.push(dataChart)
          setDataDetailsModel(dataModelDetails.reverse())
          if (averageMovementScore != 0) {
            setModelDetailsAverageMovementScore((averageMovementScore / count).toFixed(4))
          }
          setDataDetailsModelChart(dataModelDetailsChart)
          setLoadingChartModelDetail(false)
        })
        .catch((error) => {
          console.log('DataChartError: ' + error)
          setLoadingChartModelDetail(false)
        })
    }
  }, [queryParameters.get('time')])

  // console.log("CheckDataOut: " + JSON.stringify(listModalSubmission[listModalSubmission.length - 1]))
  // console.log("LastList: " + JSON.stringify(dataLastModelSubmit))

  // console.log("ChartDaTa123:" + JSON.stringify(dataLastModelDetailsChart))

  // console.log("ChartDaTa:"+JSON.stringify(dataDetailsModelChart))

  // const handleChooseCorrelation = (value) => {
  //   setCorrelationMode(value)
  // }
  // function isNumber(input) {
  //   // Check if input is a number or a string that can be parsed as a number
  //   if (!isNaN(input)) {
  //     // Check if input is an integer or a float
  //     if (Number.isInteger(parseFloat(input)) || input.toString().indexOf('.') !== -1) {
  //       return true
  //     }
  //   }
  //   return false
  // }
  // const handleSetStakeAmount = (e) => {
  //   var num = e.target.value
  //   //input value is a number or float num
  //   if (isNumber(num)) {
  //     setStakeAmount(num)
  //   }
  //   if (num == '') {
  //     setStakeAmount('')
  //   }
  // }
  // const handleSetReleaseAmount = (e) => {
  //   var num = e.target.value
  //   //input value is a number or float num
  //   if (isNumber(num)) {
  //     setReleaseAmount(num)
  //   }
  //   if (num == '') {
  //     setReleaseAmount('')
  //   }
  // }
  // const reloadData = () => {
  //   if (currentRound)
  //     getSubmissionMeanScores(
  //       {
  //         path: { tournamentId: tournamentId, submission: currentRound['Current round'] },
  //         params: {}
  //       },
  //       false
  //     )
  // }
  //modelDetail

  //wallet
  // const acc = useSelector((state) => state.user.accountAddress)
  // console.log(acc)
  // useEffect(() => {
  //   if (acc != '') {
  //     setIsClickedConnectWallet(false)
  //   } else {
  //     setIsClickedConnectWallet(true)
  //   }
  // }, [acc])
  const handleConnectWallet = () => {
    dispatch(connectWallet())
  }

  var da = [{ 'Submission Time': 'Toi nay' }, { 'Submission Time': 'Trua mai' }]

  //Calculate number of model submitted (live model)
  useEffect(() => {
    var listLiveSubmit = listModalSubmission?.filter((i) => i['type'] == 'live')
    setMyModelSubmitted(listLiveSubmit.length)
    var listModel = listModalSubmission
      .filter((ite) => {
        if (currentSymbol != undefined) {
          return ite['type'] == 'live' && ite['symbol'] == currentSymbol['id']
        }
        return ite['type'] == 'live'
      })
      .map((i) => {
        // console.log('listModalSubmission: '+JSON.stringify(listModalSubmission))

        return { id: i['model'], value: i['model'] }
      })

    var listModelBT = listModalSubmission
      .filter((ite) => {
        if (currentSymbolBackTest != undefined) {
          return ite['type'] == 'back_test' && ite['symbol'] == currentSymbolBackTest['id']
        }
        return ite['type'] == 'back_test'
      })
      .map((i) => {
        // console.log('listModalSubmission: '+JSON.stringify(listModalSubmission))

        return { id: i['model'], value: i['model'] }
      })
    setListModel(listModel)
    setListModelBackTest(listModelBT)
  }, [listModalSubmission, currentSymbol, currentSymbolBackTest])

  // console.log('currentSymbol: '+JSON.stringify(currentSymbol))
  // console.log('currentSymbolBackTest: '+JSON.stringify(currentSymbolBackTest))
  // console.log('listModelNgoai: '+JSON.stringify(listModel))
  // console.log('listModelSMS: '+JSON.stringify(listModalSubmission))

  const [currentItem, setCurrentItem] = useState('')
  //{'tournamentId':'return_and_risk_tournament', 'submissionType':itemDelete['type'], 'symbol':itemDelete['symbol'], 'model':itemDelete['model']}
  if (queryParameters.get('id') != null && currentItem == '') {
    var tournament_search_id1 = queryParameters.get('tournament')
    var submission_type1 = queryParameters.get('type')
    var symbol1 = queryParameters.get('symbol')
    var model_id1 = queryParameters.get('id')
    var submission_time1 = queryParameters.get('time')
    var obj = {
      tournamentId: tournament_search_id1,
      submissionType: submission_type1,
      symbol: symbol1,
      model: model_id1,
      submitTime: submission_time1
    }
    setCurrentItem(obj)
  }
  // console.log('Curr: '+JSON.stringify(currentItem))

  const handelDeleteModel = (e) => {
    setIsShowModelDelete(true)
    e.stopPropagation()
  }

  const performDelete = () => {
    //{"type":"live","symbol":"BTCUSDT","model":"abcdeeeee","submitTime":1698428565000}
    //`${data['tournamentId']}/submission?submission_type=${data['submissionType']}&symbol=${data['symbol']}&model_id=${data['model']}`, {
    // console.log('vaoaooaoaoaoa')
    setLoadingDelete(true)
    var tournament_search_id1 = queryParameters.get('tournament')
    var submission_type1 = queryParameters.get('type')
    var symbol1 = queryParameters.get('symbol')
    var model_id1 = queryParameters.get('id')
    var submission_time1 = queryParameters.get('time')

    deleteSubmission({
      tournamentId: tournament_search_id1,
      submissionType: submission_type1,
      symbol: symbol1,
      model: model_id1,
      submitTime: submission_time1
    })
      .then(() => {
        setLoadingDelete(false)
        toast.success('Delete model successfully...')
        navigate('/model')
      })
      .catch(() => {
        setLoadingDelete(false)
        toast.error('Delete model failed...')
      })
      .finally(() => {
        setIsShowModelDelete(false)
        setLoadListModel(true)
        setLoadingDelete(false)
      })
  }

  return (
    <>
      {/* <Toaster /> */}
      {
        // isShowDetailModal
        isShowHistory ? (
          <Container>
            <div style={{ margin: 0, width: '100%', paddingTop: 130, color: 'white' }}>
              <Row style={{ width: '100%', justifyContent: 'space-between' }}>
                <div style={{ fontSize: 18, fontWeight: 600 }}>History</div>
              </Row>
              <Row style={{ width: '100%', justifyContent: 'space-between' }}>
                <div
                  onClick={() => setIsShowHistory(false)}
                  style={{ marginTop: 12, padding: '10px 20px', background: '#9DA7BA', borderRadius: 10 }}
                >
                  Exit
                </div>
                <div style={{ position: 'relative' }}>
                  <svg
                    style={{ position: 'absolute', top: 10, left: 10, color: '#181F38' }}
                    width='24'
                    height='24'
                    viewBox='0 0 24 24'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      fillRule='evenodd'
                      clipRule='evenodd'
                      d='M18 11C18 14.866 14.866 18 11 18C7.13401 18 4 14.866 4 11C4 7.13401 7.13401 4 11 4C14.866 4 18 7.13401 18 11ZM18.0319 16.6177C19.2635 15.078 20 13.125 20 11C20 6.02944 15.9706 2 11 2C6.02944 2 2 6.02944 2 11C2 15.9706 6.02944 20 11 20C13.125 20 15.078 19.2635 16.6177 18.0319L19.2929 20.7071C19.6834 21.0976 20.3166 21.0976 20.7071 20.7071C21.0976 20.3166 21.0976 19.6834 20.7071 19.2929L18.0319 16.6177Z'
                      fill='#9DA7BA'
                    />
                  </svg>
                  <input
                    style={{
                      width: 282,
                      height: 40,
                      background: '#29384E',
                      borderRadius: 16,
                      border: 'none',
                      padding: '6px 18px 3px 40px',
                      color: 'white'
                    }}
                    placeholder='Search...'
                  />
                </div>
              </Row>
              <Row style={{ width: '100%', justifyContent: 'space-between', marginTop: 40 }}>
                <div style={{ overflow: 'auto', width: '100%' }}>
                  <div style={{ backgroundColor: '#181F38', borderRadius: 15, overflow: 'auto', padding: '24px' }}>
                    <Table style={{ marginTop: 20, color: '#fff' }}>
                      <thead style={{ height: 50 }}>
                        <tr style={{ color: '#9DA7BA', fontSize: 12, fontWeight: 500 }}>
                          <th></th>
                          <th>Name</th>
                          <th>Date</th>
                          <th>Round</th>
                          <th>Competition</th>
                        </tr>
                      </thead>
                      <tbody style={{ fontSize: 14, fontWeight: 600, marginTop: 50 }}>
                        {/* <tr style={{ height: 50 }}>
                                    <th>1</th>
                                    <th>Nesquant 1</th>
                                    <th>-0.0137</th>
                                    <th style={{ color: '#726BD3' }}>0.0036</th>
                                    <th style={{ color: "#36A9E1" }}>0.0036</th>
                                    <th style={{ color: '#FF7373' }}>
                                        <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.5858 10H14C13.4477 10 13 10.4477 13 11C13 11.5523 13.4477 12 14 12H19C19.5523 12 20 11.5523 20 11V6C20 5.44772 19.5523 5 19 5C18.4477 5 18 5.44772 18 6V8.58579L12.4142 3C11.6332 2.21895 10.3668 2.21895 9.58579 3L7 5.58579L1.70711 0.292894C1.31658 -0.0976312 0.683418 -0.0976312 0.292893 0.292894C-0.0976311 0.683418 -0.0976311 1.31658 0.292893 1.70711L5.58579 7C6.36683 7.78105 7.63316 7.78105 8.41421 7L11 4.41421L16.5858 10Z" fill="#FF7373" />
                                        </svg>
                                        0.04
                                    </th>
                                    <th style={{display: 'flex', justifyContent: 'center', alignItems: 'center' ,
                                            height: 26, padding: '5px 10px 5px 10px', borderRadius: 12, 
                                            background: '#34503D', color: '#84E4A4'}}>Active</th>
                                </tr> */}
                        <tr style={{ height: 50 }}>
                          <th>1</th>
                          <th>Name raw</th>
                          <th>Date raw</th>
                          <th>Round raw</th>
                          <th>Copetition raw</th>
                        </tr>
                      </tbody>
                    </Table>
                  </div>
                </div>
              </Row>
            </div>
          </Container>
        ) : queryParameters.get('id') != null ? (
          <Container>
            <div style={{ margin: 0, color: 'white', width: '100%', paddingTop: 130 }}>
              <Row style={{ width: '100%', justifyContent: 'space-between' }}>
                <Col md={6} xs={24}>
                  <div style={{ width: '100%', height: 287, padding: 16, background: '#181F38', borderRadius: 16 }}>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        borderBottomColor: '#29384E',
                        borderBottomWidth: 1,
                        borderBottomStyle: 'solid',
                        paddingBottom: 20
                      }}
                    >
                      <div>
                        <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', marginBottom: 12 }}>
                          Detail model
                        </div>
                        <div style={{ fontSize: 18, fontWeight: 600 }}>{queryParameters.get('id')}</div>
                      </div>
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                          height: 26,
                          padding: '5px 10px 5px 10px',
                          borderRadius: 12,
                          background: queryParameters.get('type') == 'live' ? '#34503D' : '#482E29',
                          color: queryParameters.get('type') == 'live' ? '#84E4A4' : '#FE7272'
                        }}
                      >
                        {queryParameters.get('type') == 'live' ? 'Active' : 'Inactive'}
                      </div>
                    </div>
                    <div
                      style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 22 }}
                    >
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                          <svg
                            width='17'
                            height='16'
                            viewBox='0 0 17 16'
                            fill='none'
                            xmlns='http://www.w3.org/2000/svg'
                          >
                            <path
                              fillRule='evenodd'
                              clipRule='evenodd'
                              d='M8.68664 13.2144C11.0921 13.2144 13.0421 11.2644 13.0421 8.859C13.0421 6.45356 11.0921 4.50357 8.68664 4.50357C6.2812 4.50357 4.33121 6.45356 4.33121 8.859C4.33121 11.2644 6.2812 13.2144 8.68664 13.2144ZM8.68664 14.6663C11.8939 14.6663 14.4939 12.0663 14.4939 8.859C14.4939 5.65175 11.8939 3.05176 8.68664 3.05176C5.47939 3.05176 2.87939 5.65175 2.87939 8.859C2.87939 12.0663 5.47939 14.6663 8.68664 14.6663Z'
                              fill='#9DA7BA'
                            />
                            <path
                              d='M5.3955 2.3879C5.21408 2.03118 4.77568 1.88634 4.43686 2.09934C3.4259 2.73489 2.57019 3.58931 1.93311 4.5993C1.71959 4.93779 1.86377 5.37642 2.22022 5.55838C2.57667 5.74034 3.00989 5.59611 3.23017 5.26198C3.71934 4.52 4.35485 3.88545 5.09758 3.3974C5.43204 3.17763 5.57692 2.74463 5.3955 2.3879Z'
                              fill='#9DA7BA'
                            />
                            <path
                              d='M11.9778 2.3879C12.1592 2.03118 12.5976 1.88634 12.9364 2.09934C13.9474 2.73489 14.8031 3.58931 15.4402 4.5993C15.6537 4.93779 15.5095 5.37642 15.1531 5.55838C14.7966 5.74034 14.3634 5.59611 14.1431 5.26198C13.654 4.52 13.0185 3.88545 12.2757 3.3974C11.9413 3.17763 11.7964 2.74463 11.9778 2.3879Z'
                              fill='#9DA7BA'
                            />
                            <path
                              fillRule='evenodd'
                              clipRule='evenodd'
                              d='M8.6866 5.22949C9.08751 5.22949 9.4125 5.55449 9.4125 5.9554V8.55834L10.6517 9.79754C10.9352 10.081 10.9352 10.5406 10.6517 10.8241C10.3682 11.1076 9.9086 11.1076 9.62512 10.8241L8.38592 9.58493C8.11365 9.31266 7.96069 8.94339 7.96069 8.55834V5.9554C7.96069 5.55449 8.28569 5.22949 8.6866 5.22949Z'
                              fill='#9DA7BA'
                            />
                          </svg>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Open time</div>
                      </div>
                      <div style={{ fontSize: 14, fontWeight: 600 }}>{formatTime(startTime)}</div>
                    </div>
                    <div
                      style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 12 }}
                    >
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                          <svg
                            width='17'
                            height='16'
                            viewBox='0 0 17 16'
                            fill='none'
                            xmlns='http://www.w3.org/2000/svg'
                          >
                            <path
                              fillRule='evenodd'
                              clipRule='evenodd'
                              d='M3.36185 2.19526C3.6222 1.93491 4.04431 1.93491 4.30466 2.19526C4.56501 2.45561 4.56501 2.87772 4.30466 3.13807L2.97133 4.47141C2.71097 4.73175 2.28887 4.73175 2.02851 4.47141C1.76817 4.21105 1.76817 3.78895 2.02851 3.52859L3.36185 2.19526ZM12.6952 2.19526C12.9555 1.93491 13.3777 1.93491 13.638 2.19526L14.9713 3.52859C15.2317 3.78895 15.2317 4.21105 14.9713 4.47141C14.711 4.73175 14.2889 4.73175 14.0285 4.47141L12.6952 3.13807C12.4349 2.87772 12.4349 2.45561 12.6952 2.19526ZM8.49992 4C5.92259 4 3.83325 6.08934 3.83325 8.66667C3.83325 11.244 5.92259 13.3333 8.49992 13.3333C11.0773 13.3333 13.1666 11.244 13.1666 8.66667C13.1666 6.08934 11.0773 4 8.49992 4ZM2.49992 8.66667C2.49992 5.35296 5.18621 2.66667 8.49992 2.66667C11.8137 2.66667 14.4999 5.35296 14.4999 8.66667C14.4999 11.9804 11.8137 14.6667 8.49992 14.6667C5.18621 14.6667 2.49992 11.9804 2.49992 8.66667ZM6.69518 6.86193C6.95553 6.60158 7.37765 6.60158 7.63799 6.86193L8.49992 7.72387L9.36185 6.86193C9.62219 6.60158 10.0443 6.60158 10.3047 6.86193C10.565 7.12227 10.565 7.5444 10.3047 7.80473L9.44272 8.66667L10.3047 9.5286C10.565 9.78893 10.565 10.2111 10.3047 10.4714C10.0443 10.7317 9.62219 10.7317 9.36185 10.4714L8.49992 9.60947L7.63799 10.4714C7.37765 10.7317 6.95553 10.7317 6.69518 10.4714C6.43483 10.2111 6.43483 9.78893 6.69518 9.5286L7.55712 8.66667L6.69518 7.80473C6.43483 7.5444 6.43483 7.12227 6.69518 6.86193Z'
                              fill='#9DA7BA'
                            />
                          </svg>
                        </div>

                        <div style={{display: 'flex', alignItems: 'center'}}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', }}>Close time</div>
                          <Hint content={'The submission time value in your submission corresponds to the closing time, not the opening time'} />
                        </div>

                      </div>
                      <div style={{ fontSize: 14, fontWeight: 600 }}>{formatTime(endTime)}</div>
                    </div>
                    <div
                      style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 12 }}
                    >
                      <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                          <svg
                            width='17'
                            height='16'
                            viewBox='0 0 17 16'
                            fill='none'
                            xmlns='http://www.w3.org/2000/svg'
                          >
                            <path
                              d='M9.27559 4.75684C9.40526 5.19556 9.64474 5.58716 9.9622 5.89979L7.72421 7.24258C7.59454 6.80386 7.35506 6.41227 7.0376 6.09963L9.27559 4.75684Z'
                              fill='#9DA7BA'
                            />
                            <path
                              d='M7.0376 9.89979L9.27558 11.2426C9.40525 10.8039 9.64473 10.4123 9.96218 10.0996L7.72421 8.75684C7.59454 9.19556 7.35506 9.58715 7.0376 9.89979Z'
                              fill='#9DA7BA'
                            />
                            <path
                              fillRule='evenodd'
                              clipRule='evenodd'
                              d='M11.8334 5.33301C12.5698 5.33301 13.1667 4.73605 13.1667 3.99967C13.1667 3.26329 12.5698 2.66634 11.8334 2.66634C11.097 2.66634 10.5001 3.26329 10.5001 3.99967C10.5001 4.73605 11.097 5.33301 11.8334 5.33301ZM14.5001 3.99967C14.5001 5.47243 13.3062 6.66634 11.8334 6.66634C10.3607 6.66634 9.16675 5.47243 9.16675 3.99967C9.16675 2.52691 10.3607 1.33301 11.8334 1.33301C13.3062 1.33301 14.5001 2.52691 14.5001 3.99967Z'
                              fill='#9DA7BA'
                            />
                            <path
                              fillRule='evenodd'
                              clipRule='evenodd'
                              d='M11.8334 13.333C12.5698 13.333 13.1667 12.7361 13.1667 11.9997C13.1667 11.2633 12.5698 10.6663 11.8334 10.6663C11.097 10.6663 10.5001 11.2633 10.5001 11.9997C10.5001 12.7361 11.097 13.333 11.8334 13.333ZM14.5001 11.9997C14.5001 13.4724 13.3062 14.6663 11.8334 14.6663C10.3607 14.6663 9.16675 13.4724 9.16675 11.9997C9.16675 10.5269 10.3607 9.33301 11.8334 9.33301C13.3062 9.33301 14.5001 10.5269 14.5001 11.9997Z'
                              fill='#9DA7BA'
                            />
                            <path
                              fillRule='evenodd'
                              clipRule='evenodd'
                              d='M5.16667 9.33301C5.90305 9.33301 6.5 8.73605 6.5 7.99967C6.5 7.26329 5.90305 6.66634 5.16667 6.66634C4.43029 6.66634 3.83333 7.26329 3.83333 7.99967C3.83333 8.73605 4.43029 9.33301 5.16667 9.33301ZM7.83333 7.99967C7.83333 9.47243 6.63943 10.6663 5.16667 10.6663C3.69391 10.6663 2.5 9.47243 2.5 7.99967C2.5 6.52692 3.69391 5.33301 5.16667 5.33301C6.63943 5.33301 7.83333 6.52692 7.83333 7.99967Z'
                              fill='#9DA7BA'
                            />
                          </svg>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Total models submitted</div>
                      </div>
                      <div style={{ fontSize: 14, fontWeight: 600, display: 'flex', alignItems: 'center' }}>
                        <div style={{ fontSize: 14, fontWeight: 600, marginRight: 12 }}>{myModelSubmitted}</div>
                        {/* <div style={{ color: '#84E4A4', display: 'flex', alignItems: 'center' }}>
                            <svg width='17' height='16' viewBox='0 0 17 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
                              <path
                                fillRule='evenodd'
                                clipRule='evenodd'
                                d='M11.1666 4C10.7984 4 10.4999 4.29848 10.4999 4.66667C10.4999 5.03486 10.7984 5.33333 11.1666 5.33333H12.8904L9.16658 9.05719L7.44273 7.33333C6.92203 6.81263 6.07781 6.81263 5.55711 7.33333L2.02851 10.8619C1.76816 11.1223 1.76816 11.5444 2.02851 11.8047C2.28886 12.0651 2.71097 12.0651 2.97132 11.8047L6.49992 8.27614L8.22378 10C8.74447 10.5207 9.58869 10.5207 10.1094 10L13.8333 6.27614V8C13.8333 8.36819 14.1317 8.66667 14.4999 8.66667C14.8681 8.66667 15.1666 8.36819 15.1666 8V4.66667C15.1666 4.29848 14.8681 4 14.4999 4H11.1666Z'
                                fill='#84E4A4'
                              />
                            </svg>
                            <div style={{ fontSize: 12, fontWeight: 500, marginLeft: 4 }}>12%</div>
                          </div> */}
                      </div>
                    </div>
                    <AppButton
                      isLoading={loadingSubmitNow}
                      onClick={() => setIsOpenSubmitModelInDetailModel(true)}
                      className='bg_submit'
                      style={{
                        width: '100%',
                        height: 40,
                        padding: '10px 16px 10px 16px',
                        borderRadius: 16,
                        fontSize: 16,
                        fontWeight: 600,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 24,
                        background: '#586ADF',
                        cursor: 'pointer'
                      }}
                    >
                      <div>Submit now</div>
                    </AppButton>
                  </div>

                  <div
                    style={{
                      opacity: 0.6,
                      height: 195,
                      padding: 16,
                      background: '#181F38',
                      borderRadius: 16,
                      marginTop: 24
                    }}
                  >
                    <div style={{ fontSize: 20, fontWeight: 600, marginTop: 2, marginBottom: 16, color: '#fff' }}>
                      Stake{' '}
                    </div>
                    {isClickedConnectWallet ? (
                      <div>
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 22
                          }}
                        >
                          <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                              <svg
                                width='16'
                                height='16'
                                viewBox='0 0 16 16'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                              >
                                <path
                                  d='M12.6667 13.3327H3.33333C2.97971 13.3327 2.64057 13.1922 2.39052 12.9422C2.14048 12.6921 2 12.353 2 11.9993V5.99935C2 5.64573 2.14048 5.30659 2.39052 5.05654C2.64057 4.80649 2.97971 4.66602 3.33333 4.66602H12.6667C13.0203 4.66602 13.3594 4.80649 13.6095 5.05654C13.8595 5.30659 14 5.64573 14 5.99935V11.9993C14 12.353 13.8595 12.6921 13.6095 12.9422C13.3594 13.1922 13.0203 13.3327 12.6667 13.3327Z'
                                  stroke='#9DA7BA'
                                  strokeWidth='1.5'
                                />
                                <path
                                  d='M11.0003 9.33268C10.9119 9.33268 10.8271 9.29756 10.7646 9.23505C10.7021 9.17254 10.667 9.08775 10.667 8.99935C10.667 8.91094 10.7021 8.82616 10.7646 8.76365C10.8271 8.70113 10.9119 8.66602 11.0003 8.66602C11.0887 8.66602 11.1735 8.70113 11.236 8.76365C11.2985 8.82616 11.3337 8.91094 11.3337 8.99935C11.3337 9.08775 11.2985 9.17254 11.236 9.23505C11.1735 9.29756 11.0887 9.33268 11.0003 9.33268Z'
                                  fill='#9DA7BA'
                                  stroke='#9DA7BA'
                                  strokeWidth='1.5'
                                  strokeLinecap='round'
                                  strokeLinejoin='round'
                                />
                                <path
                                  d='M12 4.66664V3.73531C11.9999 3.53097 11.9529 3.32939 11.8626 3.14612C11.7722 2.96285 11.641 2.80279 11.4789 2.67831C11.3169 2.55384 11.1284 2.46827 10.928 2.42821C10.7277 2.38816 10.5208 2.39469 10.3233 2.44731L2.99 4.40264C2.70604 4.47831 2.45503 4.64568 2.27599 4.87872C2.09696 5.11177 1.99993 5.39743 2 5.69131V5.99997'
                                  stroke='#9DA7BA'
                                  strokeWidth='1.5'
                                />
                              </svg>
                            </div>
                            <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Open time</div>
                          </div>
                          <div style={{ fontSize: 14, fontWeight: 600, color: '#fff' }}>$30,748.21</div>
                        </div>
                        <div
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            marginTop: 12
                          }}
                        >
                          <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                              <svg
                                width='16'
                                height='16'
                                viewBox='0 0 16 16'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                              >
                                <g clipPath='url(#clip0_3291_65234)'>
                                  <path
                                    d='M8 15.5C5.99573 15.5 4.11391 14.7205 2.69668 13.3033C1.27945 11.8861 0.5 10.0043 0.5 8M8 15.5C10.0043 15.5 11.8861 14.7206 13.3033 13.3033C14.7205 11.8861 15.5 10.0043 15.5 8M8 15.5C12.136 15.5 15.5 12.136 15.5 8M8 15.5C3.86404 15.5 0.5 12.136 0.5 8M15.5 8C15.5 5.99573 14.7205 4.11391 13.3033 2.69668C11.8861 1.27945 10.0043 0.5 8 0.5M15.5 8C15.5 3.86404 12.136 0.5 8 0.5M8 0.5C5.99572 0.5 4.11387 1.27942 2.69669 2.69667C1.27948 4.11394 0.5 5.99574 0.5 8M8 0.5C3.86404 0.5 0.5 3.86404 0.5 8'
                                    fill='white'
                                    stroke='#9DA7BA'
                                  />
                                  <path
                                    d='M8 4.45556L8.34225 4.53356C8.23154 4.51154 8.1171 4.5 8 4.5C7.8829 4.5 7.76846 4.51154 7.65775 4.53356L8 4.45556ZM8 11.5444L7.65774 11.4664C7.76845 11.4885 7.88289 11.5 8 11.5C8.11711 11.5 8.23155 11.4885 8.34226 11.4664L8 11.5444Z'
                                    fill='#9DA7BA'
                                    stroke='#9DA7BA'
                                  />
                                </g>
                                <defs>
                                  <clipPath id='clip0_3291_65234'>
                                    <rect width='16' height='16' fill='white' />
                                  </clipPath>
                                </defs>
                              </svg>
                            </div>
                            <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Close time</div>
                          </div>
                          <div style={{ fontSize: 14, fontWeight: 600, color: '#fff' }}>$20,42.38</div>
                        </div>
                      </div>
                    ) : (
                      <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', lineHeight: '150%' }}>
                        Connect your wallet to deposit funds into the platform before you want to invest in your
                        forecast
                      </div>
                    )}

                    <Button
                      onClick={() => setIsOpenConnectWalletModal(true)}
                      disabled={true}
                      style={{
                        width: '100%',
                        height: 40,
                        padding: '10px 16px 10px 16px',
                        borderRadius: 16,
                        fontSize: 14,
                        fontWeight: 600,
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 24,
                        background: '#586ADF',
                        cursor: 'pointer',
                        color: '#fff'
                      }}
                    >
                      {isClickedConnectWallet ? <div>Disconnect</div> : <div>Connect Wallet</div>}
                    </Button>
                  </div>
                  <div style={{ margin: '20px 0px', padding: '0px 16px' }}>
                    <p
                      onClick={(e) => handelDeleteModel(e)}
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 40,
                        padding: '5px 10px 5px 10px',
                        borderRadius: 16,
                        background: '#482E29',
                        color: '#FE7272',
                        cursor: 'pointer',
                        fontSize: 16,
                        fontWeight: 600
                      }}
                    >
                      Delete
                    </p>
                  </div>

                  <AppModal
                    width={maxSm ? 360 : 500}
                    height={150}
                    contentStyle={{
                      borderRadius: '16px',
                      padding: '24px',
                      border: 'none',
                      backgroundColor: '#29384E'
                    }}
                    isOpen={isShowModalDelete}
                  >
                    <div
                      style={{
                        color: 'white',
                        fontSize: 18,
                        fontWeight: 550,
                        textAlign: 'center',
                        marginBottom: 30,
                        marginTop: 10
                      }}
                    >
                      Do you want to delete model ?
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between', fontSize: 16, fontWeight: 600 }}>
                      <div
                        onClick={() => {
                          setIsShowModelDelete(false)
                        }}
                        style={{
                          cursor: 'pointer',
                          width: '45%',
                          paddingTop: 10,
                          paddingBottom: 10,
                          background: '#2F4C78',
                          textAlign: 'center',
                          borderRadius: 16,
                          fontSize: 18,
                          fontWeight: 600,
                          color: 'white'
                        }}
                      >
                        Cancel
                      </div>
                      <AppButton
                        onClick={() => performDelete()}
                        // onClick={modelType['id'] == 'Type 1' ? handleSubmitModel : handleSubmitModelType2}
                        isLoading={loadingDelete}
                        style={{
                          cursor: 'pointer',
                          width: '45%',
                          paddingTop: 10,
                          paddingBottom: 10,
                          background: '#5768DE',
                          textAlign: 'center',
                          borderRadius: 16,
                          fontSize: 18,
                          fontWeight: 600,
                          color: 'white'
                        }}
                      >
                        Delete
                      </AppButton>
                    </div>
                  </AppModal>

                  {/* Do not delete!!!!! */}
                  {/* deposit, withdraw */}
                  {/* <div
                      style={{
                        width: '100%',
                        height: 287,
                        padding: 16,
                        background: '#181F38',
                        borderRadius: 16,
                        marginTop: 24,
                        marginBottom: 24
                      }}
                    >
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          borderBottomColor: '#29384E',
                          borderBottomWidth: 1,
                          borderBottomStyle: 'solid',
                          paddingBottom: 20
                        }}
                      >
                        <div style={{ width: '50%' }}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', marginBottom: 12 }}>
                            Total stake
                          </div>
                          <div style={{ fontSize: 18, fontWeight: 600 }}>$1,112,344</div>
                        </div>
                        <div style={{ width: '50%' }}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', marginBottom: 12 }}>My staked</div>
                          <div style={{ fontSize: 18, fontWeight: 600 }}>$112,34</div>
                        </div>
                      </div>
                      <div
                        style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 22 }}
                      >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>1 Day Return</div>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 600 }}>-</div>
                      </div>
                      <div
                        style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 12 }}
                      >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>3 Month Return</div>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 600 }}>-</div>
                      </div>
                      <div
                        style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 12 }}
                      >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>1 Year Return</div>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 600 }}>-</div>
                      </div>
                      <div
                        style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', marginTop: 12 }}
                      >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>All Time Return</div>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 600, display: 'flex', alignItems: 'center' }}>
                          {/* <div style={{ color: '#84E4A4', display: 'flex', alignItems: 'center' }}>
                            <svg width='20' height='21' viewBox='0 0 20 21' fill='none' xmlns='http://www.w3.org/2000/svg'>
                              <path
                                fillRule='evenodd'
                                clipRule='evenodd'
                                d='M13.3334 5.5C12.8732 5.5 12.5001 5.8731 12.5001 6.33333C12.5001 6.79357 12.8732 7.16667 13.3334 7.16667H15.4882L10.8334 11.8215L8.67859 9.66667C8.02772 9.01579 6.97244 9.01579 6.32157 9.66667L1.91083 14.0774C1.58539 14.4028 1.58539 14.9305 1.91083 15.2559C2.23626 15.5814 2.7639 15.5814 3.08934 15.2559L7.50008 10.8452L9.6549 13C10.3058 13.6509 11.3611 13.6509 12.0119 13L16.6667 8.34518V10.5C16.6667 10.9602 17.0398 11.3333 17.5001 11.3333C17.9603 11.3333 18.3334 10.9602 18.3334 10.5V6.33333C18.3334 5.8731 17.9603 5.5 17.5001 5.5H13.3334Z'
                                fill='#84E4A4'
                              />
                            </svg>
                            <div style={{ fontSize: 14, fontWeight: 500, marginLeft: 4 }}>12%</div>
                          </div> */}
                  {/* </div> */}
                  {/* </div> */}
                  {/* <div
                        onClick={() => setIsOpenStakeModal(true)}
                        className='bg_submit'
                        style={{
                          width: '100%',
                          height: 40,
                          padding: '10px 16px 10px 16px',
                          borderRadius: 16,
                          fontSize: 16,
                          fontWeight: 600,
                          display: 'flex',
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginTop: 24,
                          background: '#586ADF',
                          cursor: 'pointer'
                        }}
                      >
                        Stake
                      </div>
                      <AppModal
                        ref={closeModal}
                        width={416}
                        height={301}
                        contentStyle={{
                          borderRadius: '16px',
                          padding: '24px',
                          border: 'none',
                          backgroundColor: '#29384E'
                        }}
                        isOpen={isOpenStakeModal}
                      >
                        <div style={{ color: 'white' }}>
                          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <div style={{ fontSize: 18, fontWeight: 600, textAlign: 'center', width: '100%' }}>
                              NestQuant 1
                            </div>ers
                            <div
                              onClick={() => {
                                setIsOpenStakeModal(false)
                                setStakeAmount('')
                                setReleaseAmount('')
                              }}
                              style={{ cursor: 'pointer' }}
                            >
                              <svg
                                width='24'
                                height='24'
                                viewBox='0 0 24 24'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'
                              >
                                <path
                                  d='M6.7072 5.29289C6.31668 4.90237 5.68351 4.90237 5.29299 5.29289C4.90246 5.68342 4.90246 6.31658 5.29299 6.70711L10.5858 12L5.29289 17.2929C4.90237 17.6834 4.90237 18.3166 5.29289 18.7071C5.68342 19.0976 6.31658 19.0976 6.70711 18.7071L12 13.4142L17.293 18.7071C17.6835 19.0976 18.3167 19.0976 18.7072 18.7071C19.0977 18.3166 19.0977 17.6834 18.7072 17.2929L13.4143 12L18.7071 6.70711C19.0976 6.31658 19.0976 5.68342 18.7071 5.29289C18.3166 4.90237 17.6834 4.90237 17.2929 5.29289L12 10.5857L6.7072 5.29289Z'
                                  fill='white'
                                />
                              </svg>
                            </div>
                          </div>
                          <div
                            style={{
                              display: 'flex',
                              width: '100%',
                              fontSize: 16,
                              fontWeight: 600,
                              marginTop: 16,
                              marginBottom: 24
                            }}
                          >
                            <div
                              onClick={() => setTabStakeRelease('stake')}
                              className={tabStakeRelease == 'stake' ? '' : ''}
                              style={{
                                padding: '0 12px',
                                paddingBottom: 4,
                                display: 'flex',
                                justifyContent: 'center',
                                width: '50%',
                                cursor: 'pointer',
                                color: tabStakeRelease == 'stake' ? 'white' : '#5E687C',
                                borderBottomColor: tabStakeRelease == 'stake' ? '#5467DE' : '#29384E',
                                borderBottomWidth: 4,
                                borderBottomStyle: 'solid',
                                paddingBottom: 10
                              }}
                            >
                              Stake
                            </div>
                            <div
                              onClick={() => setTabStakeRelease('release')}
                              className={tabStakeRelease == 'release' ? '' : ''}
                              style={{
                                padding: '0 12px',
                                paddingBottom: 4,
                                display: 'flex',
                                justifyContent: 'center',
                                width: '50%',
                                cursor: 'pointer',
                                color: tabStakeRelease == 'release' ? 'white' : '#5E687C',
                                borderBottomColor: tabStakeRelease == 'release' ? '#5467DE' : '#29384E',
                                borderBottomWidth: 4,
                                borderBottomStyle: 'solid',
                                paddingBottom: 10
                              }}
                            >
                              Release
                            </div>
                          </div>
                          {tabStakeRelease == 'stake' ? (
                            <>
                              <div
                                style={{
                                  fontSize: 14,
                                  fontWeight: 600,
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                  marginBottom: 8
                                }}
                              >
                                <div>Stake Amount</div>
                                <div>
                                  <span style={{ color: '#9DA7BA' }}>Available to stake: </span>0
                                </div>
                              </div>
                              <input
                                value={stakeAmount}
                                onChange={(e) => handleSetStakeAmount(e)}
                                placeholder='0.00'
                                style={{
                                  marginTop: 12,
                                  width: '100%',
                                  height: 40,
                                  borderRadius: 16,
                                  padding: '10px 12px 10px 12px',
                                  background: 'rgba(41, 56, 78, 1)',
                                  border: '1px solid #455066',
                                  fontWeight: 500,
                                  color: 'white',
                                  marginBottom: 40,
                                  fontSize: 16
                                }}
                              />
                            </>
                          ) : (
                            <div>
                              <div
                                style={{
                                  fontSize: 14,
                                  fontWeight: 600,
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'space-between',
                                  marginBottom: 8
                                }}
                              >
                                <div>Release Amount</div>
                                <div>
                                  <span style={{ color: '#9DA7BA' }}>Staked: </span>$112
                                </div>
                              </div>
                              <input
                                value={releaseAmount}
                                onChange={(e) => handleSetReleaseAmount(e)}
                                placeholder='0.00'
                                style={{
                                  marginTop: 12,
                                  width: '100%',
                                  height: 40,
                                  borderRadius: 16,
                                  padding: '10px 12px 10px 12px',
                                  background: 'rgba(41, 56, 78, 1)',
                                  border: '1px solid #455066',
                                  fontWeight: 500,
                                  color: 'white',
                                  marginBottom: 40,
                                  fontSize: 16
                                }}
                              />
                            </div>
                          )}
                          <div style={{ display: 'flex', justifyContent: 'space-between', fontSize: 16, fontWeight: 600 }}>
                            <div
                              onClick={() => {
                                setIsOpenStakeModal(false)
                                setStakeAmount('')
                                setReleaseAmount('')
                              }}
                              style={{
                                cursor: 'pointer',
                                width: '45%',
                                paddingTop: 10,
                                paddingBottom: 10,
                                background: '#2F4C78',
                                textAlign: 'center',
                                borderRadius: 16,
                                fontSize: 18,
                                fontWeight: 600
                              }}
                            >
                              Cancel
                            </div>
                            <div
                              style={{
                                cursor: 'pointer',
                                width: '45%',
                                paddingTop: 10,
                                paddingBottom: 10,
                                background: '#5768DE',
                                textAlign: 'center',
                                borderRadius: 16,
                                fontSize: 18,
                                fontWeight: 600
                              }}
                            >
                              Stake
                            </div>
                          </div>
                        </div>
                      </AppModal>
                    </div> */}
                </Col>
                <Col md={17} xs={24} style={{ marginBottom: 60 }}>
                  <div
                    className='scoreContener'
                    style={{ width: '100%', background: '#181F38', padding: 24, borderRadius: 20 }}
                  >
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                      <div style={{ fontSize: 18, fontWeight: 600 }}>Scoring</div>
                    </div>
                    <div style={{ fontSize: 17, fontWeight: 500, color: '#3953DC', marginTop: 10, marginBottom: 10 }}>
                      MS all Model: {modelDetailsAverageMovementScore}
                    </div>
                    {loadingChartModelDetail ? (
                      <div style={{ height: 250, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <ReactLoading type='spin' height={32} width={32} />
                      </div>
                    ) : (
                      <Chartc data={dataDetailsModelChart} />
                    )}
                  </div>
                  {/* do not delete!!!!! */}
                  {/* <div
                      style={{
                        width: '100%',
                        height: 345,
                        background: '#181F38',
                        padding: 24,
                        borderRadius: 20,
                        marginTop: 34
                      }}
                    >
                      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <div style={{ fontSize: 18, fontWeight: 600 }}>Stake</div>
                      </div> */}
                  {/* Chart here */}
                  {/* </div> */}

                  <div
                    className='predictedResult'
                    style={{
                      width: '100%',

                      background: '#181F38',
                      padding: 24,
                      borderRadius: 20,
                      marginTop: 34
                    }}
                  >
                    <div style={{ fontSize: 18, fontWeight: 600 }}>Predicted Result</div>
                    {loadingChartModelDetail ? (
                      <div style={{ height: 250, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <ReactLoading type='spin' height={32} width={32} />
                      </div>
                    ) : (
                      <Chartc data={dataPredictedResult} />
                    )}
                  </div>

                  <div
                    className='tableDetails'
                    style={{
                      width: '100%',
                      background: '#181F38',
                      padding: 24,
                      borderRadius: 20,
                      marginTop: 34
                    }}
                  >
                    <div style={{ fontSize: 18, fontWeight: 600 }}>Submitted rounds</div>
                    {loadingChartModelDetail ? (
                      <div style={{ height: 250, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <ReactLoading type='spin' height={32} width={32} />
                      </div>
                    ) : (
                      <TableRsuite dataDetailsModel={dataDetailsModel} />
                    )}
                  </div>
                </Col>
              </Row>
              {isOpenSubmitModelInDetailModel ? (
                <SubmitComponent
                  setCurrentSymbol={setCurrentSymbol}
                  typee={'live'}
                  isOpen={isOpenSubmitModelInDetailModel}
                  setIsOpen={setIsOpenSubmitModelInDetailModel}
                  setLoadListModel={setLoadListModel}
                  listModel={listModel}
                  listModalSubmission={listModalSubmission}
                  startTime={startTime}
                  endTime={endTime}
                  myModelSubmitted={myModelSubmitted}
                  page={'modelDetail'}
                />
              ) : null}
            </div>
          </Container>
        ) : (
          <Container style={{ paddingTop: 114 }}>
            <Row style={{ width: '100%', justifyContent: 'space-between' }}>
              <Col xs={24} md={16} style={{ marginRight: 20, marginBottom: 24 }}>
                <div
                  className='chartContainer'
                  style={{
                    width: '100%',
                    background: '#181F38',
                    padding: 24,
                    borderRadius: 20,
                    marginBottom: 30
                  }}
                >
                  <div style={{ fontSize: 20, fontWeight: 500, color: '#fff' }}>Score</div>
                  <div style={{ fontSize: 17, fontWeight: 500, color: '#3953DC', marginTop: 10, marginBottom: 10 }}>
                    MS all Model: {averageMovementScore}
                  </div>
                  {loadingScore ? (
                    <div style={{ height: 250, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                      <ReactLoading type='spin' height={32} width={32} />
                    </div>
                  ) : (
                    <Chartc data={dataLastModelDetailsChart} />
                  )}
                </div>
                <div style={{ overflow: 'auto' }}>
                  {loadingTableModel ?
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: 350 }}>
                      <ReactLoading type='spin' height={32} width={32} />
                    </div> : 
                    <TableModel
                      setCurrentItemDetailModal={setCurrentItemDetailModal}
                      setCurrentRowItem={setCurrentRowItem}
                      isShowDetailModal={isShowDetailModal}
                      setIsShowDetailModal={setIsShowDetailModal}
                      isOpenAddNewModal={isOpenAddNewModal}
                      setIsOpenAddNewModal={setIsOpenAddNewModal}
                      listModalSubmission={listModalSubmission}
                      setLoadListModel={setLoadListModel}
                    />
                  }
                </div>
              </Col>
              <Col xs={24} md={7} style={{}}>
                <Analyze
                  data={listModalSubmission}
                  setIsOpenSubmitModelInDetailModelAnalyze={setIsOpenSubmitModelInDetailModelAnalyze}
                />

                {isOpenSubmitModelInDetailModelAnalyze ? (
                  <SubmitComponent
                    setCurrentSymbol={setCurrentSymbolBackTest}
                    typee={'back_test'}
                    isOpen={isOpenSubmitModelInDetailModelAnalyze}
                    setIsOpen={setIsOpenSubmitModelInDetailModelAnalyze}
                    setLoadListModel={setLoadListModel}
                    listModel={listModelBackTest}
                    listModalSubmission={listModalSubmission}
                    startTime={startTime}
                    endTime={endTime}
                    myModelSubmitted={myModelSubmitted}
                  />
                ) : null}

                <CurrentRound
                  setCurrentSymbol={setCurrentSymbol}
                  setLoadListModel={setLoadListModel}
                  listModel={listModel}
                  listModalSubmission={listModalSubmission}
                  startTime={startTime}
                  endTime={endTime}
                  roundNumber={roundNumber}
                  myModelSubmitted={myModelSubmitted}
                />
                <br></br>
                <br></br>
                <History
                  setIsShowHistory={setIsShowHistory}
                  // currentRound={currentRound}
                  // reloadData={reloadData}N
                  // data={submissionMeanScores}
                  data={da}
                  isLoading={isGettingSubmissionMeanScores}
                  listModalSubmission={listModalSubmission}
                  setLoadListModel={setLoadListModel}
                />

                {/* Connect Wallet */}
                <div
                  style={{
                    opacity: 0.6,
                    height: 195,
                    padding: 16,
                    background: '#181F38',
                    borderRadius: 16,
                    marginTop: 24
                  }}
                >
                  <div style={{ fontSize: 20, fontWeight: 600, marginTop: 2, marginBottom: 16, color: '#fff' }}>
                    Stake{' '}
                  </div>
                  {isClickedConnectWallet ? (
                    <div>
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          marginTop: 22
                        }}
                      >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                            <svg
                              width='16'
                              height='16'
                              viewBox='0 0 16 16'
                              fill='none'
                              xmlns='http://www.w3.org/2000/svg'
                            >
                              <path
                                d='M12.6667 13.3327H3.33333C2.97971 13.3327 2.64057 13.1922 2.39052 12.9422C2.14048 12.6921 2 12.353 2 11.9993V5.99935C2 5.64573 2.14048 5.30659 2.39052 5.05654C2.64057 4.80649 2.97971 4.66602 3.33333 4.66602H12.6667C13.0203 4.66602 13.3594 4.80649 13.6095 5.05654C13.8595 5.30659 14 5.64573 14 5.99935V11.9993C14 12.353 13.8595 12.6921 13.6095 12.9422C13.3594 13.1922 13.0203 13.3327 12.6667 13.3327Z'
                                stroke='#9DA7BA'
                                strokeWidth='1.5'
                              />
                              <path
                                d='M11.0003 9.33268C10.9119 9.33268 10.8271 9.29756 10.7646 9.23505C10.7021 9.17254 10.667 9.08775 10.667 8.99935C10.667 8.91094 10.7021 8.82616 10.7646 8.76365C10.8271 8.70113 10.9119 8.66602 11.0003 8.66602C11.0887 8.66602 11.1735 8.70113 11.236 8.76365C11.2985 8.82616 11.3337 8.91094 11.3337 8.99935C11.3337 9.08775 11.2985 9.17254 11.236 9.23505C11.1735 9.29756 11.0887 9.33268 11.0003 9.33268Z'
                                fill='#9DA7BA'
                                stroke='#9DA7BA'
                                strokeWidth='1.5'
                                strokeLinecap='round'
                                strokeLinejoin='round'
                              />
                              <path
                                d='M12 4.66664V3.73531C11.9999 3.53097 11.9529 3.32939 11.8626 3.14612C11.7722 2.96285 11.641 2.80279 11.4789 2.67831C11.3169 2.55384 11.1284 2.46827 10.928 2.42821C10.7277 2.38816 10.5208 2.39469 10.3233 2.44731L2.99 4.40264C2.70604 4.47831 2.45503 4.64568 2.27599 4.87872C2.09696 5.11177 1.99993 5.39743 2 5.69131V5.99997'
                                stroke='#9DA7BA'
                                strokeWidth='1.5'
                              />
                            </svg>
                          </div>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Open time</div>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 600, color: '#fff' }}>$30,748.21</div>
                      </div>
                      <div
                        style={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          marginTop: 12
                        }}
                      >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                          <div style={{ padding: 4, borderRadius: 32, background: '#29384E', marginRight: 8 }}>
                            <svg
                              width='16'
                              height='16'
                              viewBox='0 0 16 16'
                              fill='none'
                              xmlns='http://www.w3.org/2000/svg'
                            >
                              <g clipPath='url(#clip0_3291_65234)'>
                                <path
                                  d='M8 15.5C5.99573 15.5 4.11391 14.7205 2.69668 13.3033C1.27945 11.8861 0.5 10.0043 0.5 8M8 15.5C10.0043 15.5 11.8861 14.7206 13.3033 13.3033C14.7205 11.8861 15.5 10.0043 15.5 8M8 15.5C12.136 15.5 15.5 12.136 15.5 8M8 15.5C3.86404 15.5 0.5 12.136 0.5 8M15.5 8C15.5 5.99573 14.7205 4.11391 13.3033 2.69668C11.8861 1.27945 10.0043 0.5 8 0.5M15.5 8C15.5 3.86404 12.136 0.5 8 0.5M8 0.5C5.99572 0.5 4.11387 1.27942 2.69669 2.69667C1.27948 4.11394 0.5 5.99574 0.5 8M8 0.5C3.86404 0.5 0.5 3.86404 0.5 8'
                                  fill='white'
                                  stroke='#9DA7BA'
                                />
                                <path
                                  d='M8 4.45556L8.34225 4.53356C8.23154 4.51154 8.1171 4.5 8 4.5C7.8829 4.5 7.76846 4.51154 7.65775 4.53356L8 4.45556ZM8 11.5444L7.65774 11.4664C7.76845 11.4885 7.88289 11.5 8 11.5C8.11711 11.5 8.23155 11.4885 8.34226 11.4664L8 11.5444Z'
                                  fill='#9DA7BA'
                                  stroke='#9DA7BA'
                                />
                              </g>
                              <defs>
                                <clipPath id='clip0_3291_65234'>
                                  <rect width='16' height='16' fill='white' />
                                </clipPath>
                              </defs>
                            </svg>
                          </div>
                          <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>Close time</div>
                        </div>
                        <div style={{ fontSize: 14, fontWeight: 600, color: '#fff' }}>$20,42.38</div>
                      </div>
                    </div>
                  ) : (
                    <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA', lineHeight: '150%' }}>
                      Connect your wallet to deposit funds into the platform before you want to invest in your forecast
                    </div>
                  )}

                  <Button
                    onClick={() => setIsOpenConnectWalletModal(true)}
                    disabled={true}
                    style={{
                      width: '100%',
                      height: 40,
                      padding: '10px 16px 10px 16px',
                      borderRadius: 16,
                      fontSize: 14,
                      fontWeight: 600,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginTop: 24,
                      background: '#586ADF',
                      cursor: 'pointer',
                      color: '#fff'
                    }}
                  >
                    {isClickedConnectWallet ? <div>Disconnect</div> : <div>Connect Wallet</div>}
                  </Button>
                </div>
              </Col>
            </Row>

            <AppModal
              width={395}
              height={330}
              contentStyle={{
                borderRadius: '16px',
                padding: '24px',
                border: 'none',
                backgroundColor: '#29384E'
              }}
              isOpen={isOpenAddNewModal}
            >
              <div style={{ color: 'white' }}>
                <div style={{ textAlign: 'center' }}>
                  <div
                    style={{
                      fontSize: 18,
                      fontWeight: 600,
                      textAlign: 'center',
                      width: '100%',
                      marginTop: 10,
                      marginBottom: 24
                    }}
                  >
                    Create new model
                  </div>
                  <div style={{ fontSize: 14, fontWeight: 500, color: '#9DA7BA' }}>You can create up to 50 models</div>
                </div>
                <div style={{ fontSize: 14, fontWeight: 600, marginBottom: 8, marginTop: 18 }}>Choose competition</div>
                <DropDownComponent
                  currentState={competition}
                  setCurrentState={setCompetition}
                  listElement={[
                    { id: '1', value: 'Tournament 1' },
                    { id: '2', value: 'Tournament 2' }
                  ]}
                />

                <input
                  value={newModalName}
                  onChange={(e) => setNewModalName(e.target.value)}
                  placeholder='Your model name...'
                  style={{
                    width: 347,
                    height: 40,
                    padding: '10px 12px',
                    fontSize: 16,
                    fontWeight: 500,
                    borderRadius: 16,
                    background: '#455066',
                    marginTop: 18,
                    marginBottom: 40,
                    border: 'none',
                    color: 'white'
                  }}
                />

                <div style={{ display: 'flex', justifyContent: 'space-between', fontSize: 16, fontWeight: 600 }}>
                  <div
                    onClick={() => {
                      setIsOpenAddNewModal(false)
                      setNewModalName('')
                    }}
                    style={{
                      cursor: 'pointer',
                      width: '45%',
                      paddingTop: 10,
                      paddingBottom: 10,
                      background: '#2F4C78',
                      textAlign: 'center',
                      borderRadius: 16,
                      fontSize: 18,
                      fontWeight: 600
                    }}
                  >
                    Cancel
                  </div>
                  <div
                    style={{
                      cursor: 'pointer',
                      width: '45%',
                      paddingTop: 10,
                      paddingBottom: 10,
                      background: '#5768DE',
                      textAlign: 'center',
                      borderRadius: 16,
                      fontSize: 18,
                      fontWeight: 600
                    }}
                  >
                    Stake
                  </div>
                </div>
              </div>
            </AppModal>
          </Container>
        )
      }
      <AppModal
        width={600}
        height={429}
        contentStyle={{
          borderRadius: '16px',
          padding: '24px',
          border: 'none',
          backgroundColor: '#29384E'
        }}
        isOpen={isOpenConnectWalletModal}
      >
        <div style={{ textAlign: 'center', position: 'relative' }}>
          <div style={{ fontSize: 18, fontWeight: 600, textAlign: 'center', color: 'white', marginBottom: 20 }}>
            Connect wallet
          </div>
          <svg
            onClick={() => setIsOpenConnectWalletModal(false)}
            style={{ position: 'absolute', top: 0, right: 0, cursor: 'pointer' }}
            width='20'
            height='20'
            viewBox='0 0 20 20'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              d='M5.58917 4.41009C5.26373 4.08466 4.7361 4.08466 4.41066 4.41009C4.08522 4.73553 4.08522 5.26317 4.41066 5.5886L8.82136 9.99931L4.41058 14.4101C4.08514 14.7355 4.08514 15.2632 4.41058 15.5886C4.73602 15.914 5.26366 15.914 5.58909 15.5886L9.99988 11.1778L14.4107 15.5886C14.7361 15.9141 15.2637 15.9141 15.5892 15.5886C15.9146 15.2632 15.9146 14.7355 15.5892 14.4101L11.1784 9.99931L15.5891 5.58861C15.9145 5.26317 15.9145 4.73553 15.5891 4.41009C15.2637 4.08466 14.736 4.08466 14.4106 4.41009L9.99988 8.8208L5.58917 4.41009Z'
              fill='white'
            />
          </svg>
        </div>

        <Row style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', color: 'white' }}>
          {/* <Row style={{ columnGap: 30, display: 'flex', alignItems: 'center', justifyContent: 'center' }}> */}
          <Row
            className=' nameWallet'
            style={{
              margin: 20,
              flexDirection: 'column',
              alignItems: 'center',
              width: 120,
              height: 120,
              borderRadius: 12,
              opacity: 0.1
            }}
          >
            {/* <img src={owallet_icon} style={{ width: 80, height: 60 }} /> */}
            <svg
              className='svgWallet'
              width='80'
              height='60'
              viewBox='0 0 51 49'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                opacity='0.51'
                d='M44.1755 16.2317H28.9584C28.2138 16.2317 27.5964 15.6271 27.5964 14.8697V8.35904C27.5964 7.61441 28.201 6.99707 28.9584 6.99707H44.8565C45.2383 6.99707 45.5374 7.29619 45.5374 7.67805V14.8825C45.5247 15.6271 44.9201 16.2317 44.1755 16.2317Z'
                fill='#AD83FF'
              />
              <path
                opacity='0.5'
                d='M40.3698 13.0818H26.3047C25.8846 13.0818 25.5601 12.7381 25.5601 12.3372V4.59814C25.5601 4.1781 25.9037 3.85352 26.3047 3.85352H40.3826C40.8026 3.85352 41.1272 4.19719 41.1272 4.59814V12.3372C41.1335 12.7572 40.7899 13.0818 40.3698 13.0818Z'
                fill='#AD83FF'
              />
              <path
                opacity='0.9'
                d='M49.6427 19.765H33.1401C32.7455 19.765 32.4209 19.4531 32.4209 19.0458V11.0395C32.4209 10.6449 32.7327 10.3203 33.1401 10.3203H49.6427C50.0373 10.3203 50.3619 10.6322 50.3619 11.0395V19.0394C50.3683 19.434 50.0373 19.765 49.6427 19.765Z'
                fill='#AD83FF'
              />
              <path
                d='M44.2335 37.9908C39.8803 44.1069 32.7332 48.0973 24.6569 48.0973C11.3936 48.0973 0.637939 37.3416 0.637939 24.0784C0.637939 11.3943 10.4772 1.00136 22.9385 0.12308C23.0403 0.12308 23.1358 0.110352 23.2376 0.110352C24.0268 0.135809 24.6569 0.72769 24.6569 1.45959V10.1214C16.937 10.1214 10.6872 16.3775 10.6872 24.0911C10.6872 31.8047 16.9433 38.0608 24.6569 38.0608C32.3704 38.0608 38.6265 31.8047 38.6265 24.0911C38.6265 24.0911 46.4356 27.3178 44.2335 37.9908Z'
                fill='#925AFF'
              />
              <path
                d='M48.6118 25.9307C48.4909 24.8933 47.5617 24.0914 46.4225 24.0914H26.2794C25.3884 24.0914 24.6629 23.3723 24.6629 22.4749V1.4599C24.6629 0.728008 24.0328 0.136126 23.2437 0.110669C23.3328 0.110669 23.4282 0.0979394 23.5173 0.0979394C24.542 0.0852107 25.0893 0.505256 25.2357 0.638906L25.2484 0.651636L25.2739 0.677093L46.9062 22.3094C48.7391 24.1551 48.6118 25.9307 48.6118 25.9307Z'
                fill='#AD83FF'
              />
              <path
                opacity='0.91'
                d='M48.5996 25.912C48.5869 26.1793 48.5614 26.4402 48.5359 26.6884C48.0841 30.8698 46.5566 34.7329 44.2337 37.9915C46.4357 27.3121 38.6267 24.0918 38.6267 24.0918H46.4166C47.5622 24.0918 48.4914 24.8937 48.5996 25.912Z'
                fill='url(#paint0_linear_2418_5006)'
              />
              <defs>
                <linearGradient
                  id='paint0_linear_2418_5006'
                  x1='43.6131'
                  y1='38.5397'
                  x2='43.6131'
                  y2='24.1525'
                  gradientUnits='userSpaceOnUse'
                >
                  <stop offset='8.52446e-08' stopColor='#925AFF' />
                  <stop offset='1' stopColor='#713BBB' />
                </linearGradient>
              </defs>
            </svg>

            <div style={{ textAlign: 'center' }}>Owallet</div>
          </Row>
          <Row
            onClick={() => handleConnectWallet()}
            className=' nameWalletmetaMask'
            style={{
              margin: 20,
              cursor: 'pointer',
              flexDirection: 'column',
              width: 120,
              alignItems: 'center',
              height: 120,
              borderRadius: 12
            }}
          >
            <svg
              className='svgWallet'
              width='60'
              height='60'
              viewBox='0 0 49 49'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M46.1301 1.89648L27.3782 15.7716L30.8653 7.59453L46.1301 1.89648Z'
                fill='#E17726'
                stroke='#E17726'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M2.87354 1.89648L21.4585 15.9011L18.1384 7.59451L2.87354 1.89648Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M39.3758 34.0684L34.3865 41.6904L45.07 44.632L48.1305 34.235L39.3758 34.0684Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M0.888672 34.235L3.9305 44.632L14.5956 41.6904L9.62469 34.0684L0.888672 34.235Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M14.0198 21.191L11.0522 25.668L21.6245 26.149L21.2721 14.7715L14.0198 21.191Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M34.9827 21.1926L27.6193 14.6436L27.3782 26.1506L37.9504 25.6696L34.9827 21.1926Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M14.5989 41.6901L20.9978 38.6006L15.4892 34.3086L14.5989 41.6901Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M28.0046 38.6006L34.385 41.6901L33.5132 34.3086L28.0046 38.6006Z'
                fill='#E27625'
                stroke='#E27625'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M34.385 41.6891L28.0046 38.5996L28.5239 42.7436L28.4682 44.5011L34.385 41.6891Z'
                fill='#D5BFB2'
                stroke='#D5BFB2'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M14.5989 41.6891L20.5342 44.5011L20.4971 42.7436L20.9978 38.5996L14.5989 41.6891Z'
                fill='#D5BFB2'
                stroke='#D5BFB2'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M20.6421 31.5714L15.3374 30.0174L19.084 28.2969L20.6421 31.5714Z'
                fill='#233447'
                stroke='#233447'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M28.3591 31.5714L29.9172 28.2969L33.6824 30.0174L28.3591 31.5714Z'
                fill='#233447'
                stroke='#233447'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M14.5941 41.6904L15.5215 34.0684L9.62329 34.235L14.5941 41.6904Z'
                fill='#CC6228'
                stroke='#CC6228'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M33.4763 34.0684L34.3852 41.6904L39.3745 34.235L33.4763 34.0684Z'
                fill='#CC6228'
                stroke='#CC6228'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M37.9504 25.6699L27.3782 26.1509L28.3612 31.5716L29.9192 28.2969L33.6844 30.0175L37.9504 25.6699Z'
                fill='#CC6228'
                stroke='#CC6228'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M15.3368 30.0175L19.0834 28.2969L20.6415 31.5716L21.6245 26.1509L11.0522 25.6699L15.3368 30.0175Z'
                fill='#CC6228'
                stroke='#CC6228'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M11.0569 25.6699L15.4898 34.3095L15.3414 30.0175L11.0569 25.6699Z'
                fill='#E27525'
                stroke='#E27525'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M33.6798 30.0175L33.5129 34.3095L37.9459 25.6699L33.6798 30.0175Z'
                fill='#E27525'
                stroke='#E27525'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M21.6274 26.1494L20.6443 31.5701L21.887 37.971L22.1652 29.535L21.6274 26.1494Z'
                fill='#E27525'
                stroke='#E27525'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M27.3763 26.1494L26.8569 29.5166L27.1166 37.971L28.3593 31.5701L27.3763 26.1494Z'
                fill='#E27525'
                stroke='#E27525'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M28.3601 31.5707L27.1174 37.9716L28.0078 38.6007L33.5165 34.3086L33.6834 30.0166L28.3601 31.5707Z'
                fill='#F5841F'
                stroke='#F5841F'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M15.3374 30.0166L15.4859 34.3086L20.9945 38.6007L21.8848 37.9716L20.6421 31.5707L15.3374 30.0166Z'
                fill='#F5841F'
                stroke='#F5841F'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M28.4727 44.5035L28.5282 42.7459L28.0461 42.3389H20.9608L20.4971 42.7459L20.5342 44.5035L14.5989 41.6914L16.6762 43.3934L20.8866 46.298H28.1016L32.3305 43.3934L34.3893 41.6914L28.4727 44.5035Z'
                fill='#C0AC9D'
                stroke='#C0AC9D'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M28.0077 38.5996L27.1174 37.9707H21.8869L20.9967 38.5996L20.4958 42.7437L20.9596 42.3367H28.0448L28.5271 42.7437L28.0077 38.5996Z'
                fill='#161616'
                stroke='#161616'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M46.9233 16.678L48.4999 9.01903L46.1258 1.89648L28.0046 15.3091L34.9785 21.1921L44.8274 24.0597L46.9976 21.5252L46.0517 20.8406L47.5539 19.4717L46.404 18.5836L47.9064 17.4366L46.9233 16.678Z'
                fill='#763E1A'
                stroke='#763E1A'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M0.5 9.01903L2.09511 16.678L1.07499 17.4366L2.5959 18.5836L1.44594 19.4717L2.94831 20.8406L2.00237 21.5252L4.17247 24.0597L14.0214 21.1921L20.9953 15.3091L2.87412 1.89648L0.5 9.01903Z'
                fill='#763E1A'
                stroke='#763E1A'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M44.8271 24.0598L34.9782 21.1924L37.9459 25.6694L33.5129 34.3089L39.374 34.235H48.1286L44.8271 24.0598Z'
                fill='#F5841F'
                stroke='#F5841F'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M14.0205 21.1924L4.17163 24.0598L0.888672 34.235H9.62469L15.4857 34.3089L11.0529 25.6694L14.0205 21.1924Z'
                fill='#F5841F'
                stroke='#F5841F'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
              <path
                d='M27.3774 26.1504L28.0081 15.3092L30.8644 7.59473H18.1406L20.9969 15.3092L21.6276 26.1504L21.8687 29.5544L21.8873 37.9719H27.1177L27.1363 29.5544L27.3774 26.1504Z'
                fill='#F5841F'
                stroke='#F5841F'
                strokeWidth='0.25'
                strokeLinecap='round'
                strokeLinejoin='round'
              />
            </svg>

            <div style={{ textAlign: 'center' }}>Metamask</div>
          </Row>
          <Row
            className=' nameWallet'
            style={{
              margin: 20,
              flexDirection: 'column',
              alignItems: 'center',
              width: 120,
              height: 120,
              borderRadius: 12,
              opacity: 0.1
            }}
          >
            <img alt='' src={tronlink_wallet} style={{ width: 60, height: 60 }} />
            <div style={{ textAlign: 'center' }}>Tronlink</div>
          </Row>
          {/* </Row> */}
          {/* <Row style={{ columnGap: 30, display: 'flex', alignItems: 'center', justifyContent: 'center' }}> */}
          <Row
            className=' nameWallet'
            style={{
              margin: 20,
              flexDirection: 'column',
              alignItems: 'center',
              width: 120,
              height: 120,
              borderRadius: 12,
              opacity: 0.1
            }}
          >
            {/* <img src={phantom} style={{ width: 80, height: 60 }} /> */}
            <svg
              className='svgWallet'
              width='60'
              height='60'
              viewBox='0 0 49 49'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <g clipPath='url(#clip0_2418_5052)'>
                <path
                  d='M6.17052 44.0501C12.2858 44.0501 16.8815 38.7318 19.6242 34.5293C19.2906 35.4591 19.1052 36.3888 19.1052 37.2814C19.1052 39.736 20.5137 41.4839 23.2933 41.4839C27.1107 41.4839 31.1875 38.1368 33.3001 34.5293C33.1518 35.05 33.0778 35.5335 33.0778 35.9797C33.0778 37.6905 34.0413 38.769 36.0057 38.769C42.1951 38.769 48.4215 27.7978 48.4215 18.2026C48.4215 10.7273 44.6411 4.14453 35.1532 4.14453C18.4752 4.14453 0.5 24.525 0.5 37.6905C0.5 42.86 3.27967 44.0501 6.17052 44.0501ZM29.4085 17.3844C29.4085 15.5249 30.4463 14.2232 31.9658 14.2232C33.4483 14.2232 34.4861 15.5249 34.4861 17.3844C34.4861 19.244 33.4483 20.5828 31.9658 20.5828C30.4463 20.5828 29.4085 19.244 29.4085 17.3844ZM37.3399 17.3844C37.3399 15.5249 38.3777 14.2232 39.8972 14.2232C41.3797 14.2232 42.4174 15.5249 42.4174 17.3844C42.4174 19.244 41.3797 20.5828 39.8972 20.5828C38.3777 20.5828 37.3399 19.244 37.3399 17.3844Z'
                  fill='#AB9FF2'
                />
              </g>
              <defs>
                <clipPath id='clip0_2418_5052'>
                  <rect width='48' height='39.9056' fill='white' transform='translate(0.5 4.14453)' />
                </clipPath>
              </defs>
            </svg>

            <div style={{ textAlign: 'center' }}>Phantom</div>
          </Row>
          <Row
            className=' nameWallet'
            style={{
              margin: 20,
              flexDirection: 'column',
              alignItems: 'center',
              width: 120,
              height: 120,
              borderRadius: 12,
              opacity: 0.1
            }}
          >
            {/* <img src={keplr_icon} style={{ width: 80, height: 60 }} /> */}
            <svg
              className='svgWallet'
              width='60'
              height='60'
              viewBox='0 0 49 49'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <g clipPath='url(#clip0_2418_5058)'>
                <mask id='mask0_2418_5058' maskUnits='userSpaceOnUse' x='0' y='0' width='60' height='60'>
                  <path d='M48.5 0.0976562H0.5V48.0977H48.5V0.0976562Z' fill='white' />
                </mask>
                <g mask='url(#mask0_2418_5058)'>
                  <path
                    d='M37.5909 0.0976562H11.4091C5.38417 0.0976562 0.5 4.98183 0.5 11.0067V37.1885C0.5 43.2135 5.38417 48.0977 11.4091 48.0977H37.5909C43.6159 48.0977 48.5 43.2135 48.5 37.1885V11.0067C48.5 4.98183 43.6159 0.0976562 37.5909 0.0976562Z'
                    fill='url(#paint0_linear_2418_5058)'
                  />
                  <path
                    d='M37.5909 0.0976562H11.4091C5.38417 0.0976562 0.5 4.98183 0.5 11.0067V37.1885C0.5 43.2135 5.38417 48.0977 11.4091 48.0977H37.5909C43.6159 48.0977 48.5 43.2135 48.5 37.1885V11.0067C48.5 4.98183 43.6159 0.0976562 37.5909 0.0976562Z'
                    fill='url(#paint1_radial_2418_5058)'
                  />
                  <path
                    d='M37.5909 0.0976562H11.4091C5.38417 0.0976562 0.5 4.98183 0.5 11.0067V37.1885C0.5 43.2135 5.38417 48.0977 11.4091 48.0977H37.5909C43.6159 48.0977 48.5 43.2135 48.5 37.1885V11.0067C48.5 4.98183 43.6159 0.0976562 37.5909 0.0976562Z'
                    fill='url(#paint2_radial_2418_5058)'
                  />
                  <path
                    d='M37.5909 0.0976562H11.4091C5.38417 0.0976562 0.5 4.98183 0.5 11.0067V37.1885C0.5 43.2135 5.38417 48.0977 11.4091 48.0977H37.5909C43.6159 48.0977 48.5 43.2135 48.5 37.1885V11.0067C48.5 4.98183 43.6159 0.0976562 37.5909 0.0976562Z'
                    fill='url(#paint3_radial_2418_5058)'
                  />
                  <path
                    d='M20.2172 36.968V25.8341L31.0354 36.968H37.0541V36.6783L24.6101 23.9975L36.0964 11.9673V11.8252H30.0387L20.2172 22.4562V11.8252H15.3398V36.968H20.2172Z'
                    fill='white'
                  />
                </g>
              </g>
              <defs>
                <linearGradient
                  id='paint0_linear_2418_5058'
                  x1='24.5'
                  y1='0.0976562'
                  x2='24.5'
                  y2='48.0977'
                  gradientUnits='userSpaceOnUse'
                >
                  <stop stopColor='#1FD1FF' />
                  <stop offset='1' stopColor='#1BB8FF' />
                </linearGradient>
                <radialGradient
                  id='paint1_radial_2418_5058'
                  cx='0'
                  cy='0'
                  r='1'
                  gradientUnits='userSpaceOnUse'
                  gradientTransform='translate(2.79283 46.2789) rotate(-45.1556) scale(76.9768 78.1285)'
                >
                  <stop stopColor='#232DE3' />
                  <stop offset='1' stopColor='#232DE3' stopOpacity='0' />
                </radialGradient>
                <radialGradient
                  id='paint2_radial_2418_5058'
                  cx='0'
                  cy='0'
                  r='1'
                  gradientUnits='userSpaceOnUse'
                  gradientTransform='translate(45.9147 47.8236) rotate(-138.45) scale(48.1299 73.3847)'
                >
                  <stop stopColor='#8B4DFF' />
                  <stop offset='1' stopColor='#8B4DFF' stopOpacity='0' />
                </radialGradient>
                <radialGradient
                  id='paint3_radial_2418_5058'
                  cx='0'
                  cy='0'
                  r='1'
                  gradientUnits='userSpaceOnUse'
                  gradientTransform='translate(24.1001 0.453654) rotate(90) scale(37.844 91.8198)'
                >
                  <stop stopColor='#24D5FF' />
                  <stop offset='1' stopColor='#1BB8FF' stopOpacity='0' />
                </radialGradient>
                <clipPath id='clip0_2418_5058'>
                  <rect width='48' height='48' fill='white' transform='translate(0.5 0.0976562)' />
                </clipPath>
              </defs>
            </svg>

            <div style={{ textAlign: 'center' }}>Keplr</div>
          </Row>
          <Row
            className=' nameWallet'
            style={{
              margin: 20,
              flexDirection: 'column',
              alignItems: 'center',
              width: 120,
              height: 120,
              borderRadius: 12,
              opacity: 0.1
            }}
          >
            {/* <img src={ledger} style={{ width: 80, height: 60 }} /> */}
            <svg
              className='svgWallet'
              width='60'
              height='60'
              viewBox='0 0 49 49'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <g clipPath='url(#clip0_2418_5072)'>
                <path
                  d='M48.5 24.0977C48.5 10.8428 37.7548 0.0976562 24.5 0.0976562C11.2452 0.0976562 0.5 10.8428 0.5 24.0977C0.5 37.3525 11.2452 48.0977 24.5 48.0977C37.7548 48.0977 48.5 37.3525 48.5 24.0977Z'
                  fill='#EFE6F6'
                />
                <path
                  fillRule='evenodd'
                  clipRule='evenodd'
                  d='M22.068 19.3437H23.423V27.5977H28.182V28.8477H22.068V19.3437ZM12.75 35.0937V28.8487H14.106V33.7087H22.054V35.0947H12.75V35.0937ZM12.75 19.3437V13.0977H22.054V14.4827H14.106V19.3427L12.75 19.3437ZM28.179 13.0977V14.4827H36.127V19.3427H37.483V13.0977H28.179ZM28.179 33.7087V35.0937H37.483V28.8487H36.127V33.7087H28.179Z'
                  fill='black'
                />
              </g>
              <defs>
                <clipPath id='clip0_2418_5072'>
                  <rect width='48' height='48' fill='white' transform='translate(0.5 0.0976562)' />
                </clipPath>
              </defs>
            </svg>

            <div style={{ textAlign: 'center' }}>Ledger</div>
          </Row>
          {/* </Row> */}
        </Row>
        <div
          style={{ fontSize: 14, fontWeight: 500, textAlign: 'center', textDecoration: 'underline', color: '#798FFF' }}
        >
          I don’t have a wallet?
        </div>
      </AppModal>
    </>
  )
}
