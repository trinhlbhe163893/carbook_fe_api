/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/jsx-key */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import data3 from '@src/assets/images/data3.png'
import { useSelector, useDispatch } from 'react-redux'
import { ChevronDown, CloseIcon, StarDivider } from '@src/assets/svgs'
import AppButton from '@src/components/AppButton'
import AppModal from '@src/components/AppModal'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { Col, Row } from 'antd'
import classNames from 'classnames/bind'
import { useEffect, useRef, useState } from 'react'
import { toast, Toaster } from 'react-hot-toast'
import { Link } from 'react-router-dom'
import {
  useCreateApiKeyMutation,
  useLazyGetApiKeyQuery,
  useLazyGetDataQuery
  // useLazyGetSymbolByCategoryQuery
  // useLazyGetVerifyCompetitionQuery
} from '../../userService'
import styles from './Data.module.sass'
import { SYMBOL_OPTIONS } from '@src/configs'
import { Container } from '@mui/material'
import axios from 'axios'
import { saveAs } from 'file-saver'
import {
  downloadDataZip,
  checkRegiterTournament,
  regiserTournament,
  getImage,
  getMetricInfo
} from '@src/axios/getApiAll'
import DropDownComponent from '../../components/DropDown'
// import Cookies from 'universal-cookie'

// const cookies = new Cookies()

const cx = classNames.bind(styles)
const options = SYMBOL_OPTIONS

// var token = document.cookie.split("; ").find((row) => row.startsWith("access_token="))
// ?.split("=")[1];

function Data() {
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isSubmittingFirst, setIsSubmittingFirst] = useState(false)
  const [showOptions, setShowOptions] = useState(false)
  const [showOptionsCoinChild, setShowOptionsCoinChild] = useState(false)
  const [showSuggest, setShowSuggest] = useState(false)
  const [selectedOption, setSelectedOption] = useState(options[0])
  const [verifyClick, setVerifyClick] = useState(false)
  const [openModal, setOpenModal] = useState(false)

  const [isShowTableMatric, setIsShowTableMatric] = useState(false)

  const [showSymbolDropDown, setShowSymbolDropDown] = useState(false)
  const [showSymbolDropDownLatest, setShowSymbolDropDownLatest] = useState(false)
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn)
  const symbolInput = useRef()
  const closeModal = useRef(null)
  const line1FirstRef = useRef()
  const line1SecondRef = useRef()
  const line1ThirdRef = useRef()
  const line2FirstRef = useRef()
  const line2SecondRef = useRef()
  const line2ThirdRef = useRef()

  const [getData, { isFetching: isLoading }] = useLazyGetDataQuery()
  const [getApiKey, { data: apiKey, isLoading: isGettingAPIKey }] = useLazyGetApiKeyQuery()
  const [createApiKey, { data: newApiKey, isLoading: isCreatingAPIKey }] = useCreateApiKeyMutation()
  // const [getSymbols, { data: symbols }] = useLazyGetSymbolByCategoryQuery()
  // const [getRegistered, { data: verifyCompetition }] = useLazyGetVerifyCompetitionQuery()

  const maxSm = useMediaQuery('(max-width: 992px)')

  //regiser tournament
  useEffect(() => {
    if (isLoggedIn)
      checkRegiterTournament({ tournament_search_id: 'return_and_risk_tournament' })
        .then((data) => {
          if (data['registered_status'] === false) {
            regiserTournament({ tournament_search_id: 'return_and_risk_tournament' })
          }
        })
        .catch((error) => {
          console.log('errorcheckRegiterTournament: ' + error)
        })
  }, [])

  useEffect(() => {
    const closeDropDown = (e) => {
      if (line1FirstRef.current && !line1FirstRef.current.contains(e.target)) {
        setShowOptions(false)
      }
      if (line1SecondRef.current && !line1SecondRef.current.contains(e.target)) {
        setShowOptionsCoinChild(false)
      }
      if (line2FirstRef.current && !line2FirstRef.current.contains(e.target)) {
        setShowOptionsLatest(false)
      }
      if (line2SecondRef.current && !line2SecondRef.current.contains(e.target)) {
        setShowOptionsCoinChildLatest(false)
      }
      if (line1ThirdRef.current && !line1ThirdRef.current.contains(e.target)) {
        setShowSymbolDropDown(false)
      }
      if (line2ThirdRef.current && !line2ThirdRef.current.contains(e.target)) {
        setShowSymbolDropDownLatest(false)
      }
    }

    document.body.addEventListener('click', closeDropDown)

    return () => document.body.removeEventListener('click', closeDropDown)
  }, [])

  // useEffect(() => {
  //   setOpenModal(!verifyCompetition)
  // }, [verifyCompetition])

  // useEffect(() => {
  //   // getRegistered(null, false)
  //   getSymbols({ category: options[0].toLowerCase() }, false)
  // }, [])

  const handleOptionClick = async (index) => {
    setSideBarMode(index)
    setCoinChild('')
    setShowOptionsCoinChild(true)

    setTextSearch('')
    // setSelectedOption(options[index])
    setShowOptions(false)
    setData('')
    // await getSymbols({ category: options[index].toLowerCase() })
  }

  const handleOptionClickLatest = async (index) => {
    setSideBarModeLatest(index)
    setCoinChildLatest('')
    setShowOptionsCoinChildLatest(true)
    setTextSearchLatest('')
    // setSelectedOption(options[index])
    setShowOptionsLatest(false)
    setDataLatest('')
    // await getSymbols({ category: options[index].toLowerCase() })
  }

  const handleOutsideClick = () => {
    setShowOptions(false)
  }

  const handleOutsideClickLatest = () => {
    setShowOptionsLatest(false)
  }

  const handleGetData = async () => {
    const response = await getData({ category: selectedOption.toLowerCase(), symbol: symbol.toUpperCase() }, false)

    if (response?.error) {
      toast.error(response.error.data.detail || `Request failed with status code ${response.error.status}`)
    } else {
      const href = response.data

      // create "a" HTML element with href to file & click
      const link = document.createElement('a')
      link.href = href
      link.setAttribute('download', `${symbol}.zip`) //or any other extension
      document.body.appendChild(link)
      link.click()

      // clean up "a" element & remove ObjectURL
      document.body.removeChild(link)
      URL.revokeObjectURL(href)
    }
  }

  const handleDownLoadZipData = () => {
    if (!data) {
      toast.error('Please choose a metric to download...')
      return
    }
    setIsSubmittingFirst(true)
    let url
    if (symbol['id'] != undefined) {
      url = `${process.env.BASE_API_URL}/metric/${data}/download?symbol=${symbol['id'] + '&'}resolution=1h`
    } else {
      url = `${process.env.BASE_API_URL}/metric/${data}/download?resolution=1h`
    }
    // if (data.includes("tournament") | data.includes("usd_price")){
    //   url += '&symbol=BTC';
    // }
    // axios({
    //   method: 'GET',
    //   url: url,
    //   responseType: 'blob',
    //   headers: {
    //     Authorization: `Bearer ${token}`,
    //   },
    // })
    downloadDataZip(url)
      .then((response) => {
        // console.log("RES: "+JSON.stringify(response))
        // const contentDispositionHeader = response.headers['content-disposition'];
        // const fileName = contentDispositionHeader
        //   ? contentDispositionHeader.split('filename=')[1].trim()
        //   : 'download.zip';
        const fileName = data ? data + '.zip' : 'download.zip'
        const blob = new Blob([response], { type: response.type })
        saveAs(blob, fileName)
        setIsSubmittingFirst(false)
      })
      .catch((error) => {
        if (error.response.status == 401) {
          toast.error(`Unauthorized: invalid credentials`)
          setIsSubmittingFirst(false)
        } else {
          toast.error(response.error.data.detail || `Request failed with status code ${response.error.status}`)
          setIsSubmittingFirst(false)
        }
      })
  }

  const handleDownLoadZipDataLatest = () => {
    if (!dataLatest) {
      toast.error('Please choose a metric to download...')
      return
    }
    setIsSubmitting(true)
    // console.log('symbolLatest[id]: '+symbolLatest['id'])
    // console.log(symbolLatest['id'] != undefined)
    let url
    if (symbolLatest['id'] != undefined) {
      url = `${process.env.BASE_API_URL}/metric/${dataLatest}/download/latest?symbol=${
        symbolLatest['id'] + '&'
      }resolution=1h`
    } else {
      url = `${process.env.BASE_API_URL}/metric/${dataLatest}/download/latest?resolution=1h`
    }

    // if (dataLatest.includes("tournament") | dataLatest.includes("usd_price")){
    //   url += '&symbol=BTC';
    // }
    downloadDataZip(url)
      .then((response) => {
        // const contentDispositionHeader = response.headers['content-disposition'];
        // const fileName = contentDispositionHeader
        //   ? contentDispositionHeader.split('filename=')[1].trim()
        //   : 'download.zip';
        const fileName = dataLatest ? dataLatest + '.csv' : 'download.csv'
        const blob = new Blob([response], { type: response.type })
        saveAs(blob, fileName)
        setIsSubmitting(false)
      })
      .catch((error) => {
        if (error.response.status == 401) {
          toast.error(`Unauthorized: invalid credentials`)
          setIsSubmitting(false)
        } else {
          toast.error(response.error.data.detail || `Request failed with status code ${response.error.status}`)
          setIsSubmitting(false)
        }
      })
  }

  const handleGetAPIKey = async () => {
    // if (!verifyCompetition) {
    //   toast.error('You must subscribe to competition to get API key!')
    // } else {
    const response = await getApiKey(null, false)
    if (response.error?.status === 404) {
      //   const result = await createApiKey()
      // } else if (response.error) {
        toast.error(response.error.data.detail || `Request failed with status code ${response.error.status}`)
      //other error\
    }
    // }
  }

  //Code Dai
  const [dataSideBar, setDataSideBar] = useState([])
  const [sideBarMode, setSideBarMode] = useState('Blockchain')
  const [coinChild, setCoinChild] = useState('')
  const [allMetric, setAllMetric] = useState([])
  const [coinChildDetail, setCoinChildDetail] = useState('')
  const [metric, setMetric] = useState('')
  const [data, setData] = useState('')
  const [textSearch, setTextSearch] = useState('')

  const [showOptionsLatest, setShowOptionsLatest] = useState(false)
  const [showOptionsCoinChildLatest, setShowOptionsCoinChildLatest] = useState(false)
  const [isShowTableMatricLatest, setIsShowTableMatricLatest] = useState(false)
  const [sideBarModeLatest, setSideBarModeLatest] = useState('Blockchain')
  const [coinChildLatest, setCoinChildLatest] = useState('')
  const [dataLatest, setDataLatest] = useState('')
  const [textSearchLatest, setTextSearchLatest] = useState('')

  const [listSymbol, setListSymbol] = useState([])
  const [listSymbolLatest, setListSymbolLatest] = useState([])
  const [symbol, setSymbol] = useState('')
  const [symbolLatest, setSymbolLatest] = useState('')

  const handleDetailSideBarClick = (coin) => {
    // console.log('coint: '+JSON.stringify(listCoinChild))
    setCoinChild(coin)
    setTextSearch('')
    setIsShowTableMatric(true)
    setShowOptionsCoinChild(false)
    setData('')
  }
  const handleDetailSideBarClickLatest = (coin) => {
    // console.log('coint: '+JSON.stringify(listCoinChild))
    setCoinChildLatest(coin)
    setTextSearchLatest('')
    setIsShowTableMatricLatest(true)
    setShowOptionsCoinChildLatest(false)
    setDataLatest('')
  }

  const handleCoinChildDetailClick = (index) => {
    setCoinChildDetail(index)
  }

  const handleChooseData = (id, name) => {
    getMetricInfo(id)
      .then((data) => {
        setListSymbol(data['symbols'])
        if (!(data['symbols'][0] == undefined)) {
          setSymbol({ id: data['symbols'][0], value: data['symbols'][0] })
        } else {
          setSymbol('')
        }
      })
      .catch((error) => {
        console.log('error getMetricInfo: ' + error)
      })
    setData(id)
    setTextSearch(name)
    setIsShowTableMatric(false)
  }
  const handleChooseDataLatest = (id, name) => {
    getMetricInfo(id)
      .then((data) => {
        setListSymbolLatest(data['symbols'])
        if (!(data['symbols'][0] == undefined)) {
          setSymbolLatest({ id: data['symbols'][0], value: data['symbols'][0] })
        } else {
          setSymbolLatest('')
        }
      })
      .catch((error) => {
        console.log('error getMetricInfoLatest: ' + error)
      })
    setDataLatest(id)
    setTextSearchLatest(name)
    setIsShowTableMatricLatest(false)
  }
  // console.log('SYM: '+symbol)
  const setSearchText = (val) => {
    setTextSearch(val)
    setIsShowTableMatric(true)
  }
  const setSearchTextLatest = (val) => {
    setTextSearchLatest(val)
    setIsShowTableMatricLatest(true)
  }

  const handleSymbolDropDownClick = (obj) => {
    setSymbol(obj)
    setShowSymbolDropDown(false)
  }
  const handleSymbolDropDownLatestClick = (obj) => {
    setSymbolLatest(obj)
    setShowSymbolDropDownLatest(false)
  }

  const [img, setImg] = useState('')

  useEffect(() => {
    // getImage()
    // .then(data => {
    //   var reader = new window.FileReader();
    //     reader.readAsDataURL(data);
    //     reader.onload = function() {
    //       var imageDataUrl = reader.result;
    //       console.log("imageDataUrl: "+imageDataUrl)
    //       setImg(imageDataUrl)

    //       }

    // })
    // .catch(error => {
    //   console.log('errorrrrrr: '+JSON.stringify(error))
    // })

    axios
      .get(`${process.env.BASE_API_URL}/metric/categories`)
      .then((data) => {
        setDataSideBar(data?.data)
      })
      .catch((error) => {
        console.log('error: ' + error)
      })

    axios
      .get(`${process.env.BASE_API_URL}/metric/all`)
      .then((data) => {
        // console.log('all: '+JSON.stringify(data?.data))
        setAllMetric(data?.data)
      })
      .catch((error) => {
        console.log('error: ' + error)
      })
  }, [])

  var listCoin = Object.keys(dataSideBar).filter((coin) => coin != sideBarMode)

  var sideBarDetailList = dataSideBar[sideBarMode]
  sideBarDetailList = sideBarDetailList ? Object.keys(sideBarDetailList) : null

  //Latest
  var listCoinLatest = Object.keys(dataSideBar).filter((coin) => coin != sideBarModeLatest)

  var sideBarDetailListLatest = dataSideBar[sideBarModeLatest]
  sideBarDetailListLatest = sideBarDetailListLatest ? Object.keys(sideBarDetailListLatest) : null

  if (coinChild == '' && sideBarDetailList) {
    setCoinChild(sideBarDetailList[0])
  }

  if (coinChildLatest == '' && sideBarDetailListLatest) {
    setCoinChildLatest(sideBarDetailListLatest[0])
  }

  // console.log('sideBarDetailList'+JSON.stringify(sideBarDetailList))

  try {
    var dataListCoinChild
    var listCoinChild = sideBarDetailList ? dataSideBar[sideBarMode][coinChild] : null
    listCoinChild = listCoinChild ? Object.keys(listCoinChild) : null
    dataListCoinChild = listCoinChild?.map((coin) => {
      // console.log('coin: '+coin)
      var arrayCoin = dataSideBar[sideBarMode][coinChild][coin]?.filter((item) =>
        item['name'].toLowerCase().includes(textSearch?.toLowerCase())
      )
      var valueToObj = { name: coin, value: arrayCoin }
      return valueToObj
    })
    // console.log('dataListCoinChild: '+JSON.stringify(dataListCoinChild))
    // var dataListCoinChild
    // if(Object.keys(allMetric).length > 0) {
    //   // console.log('all: '+JSON.stringify(allMetric))
    //   dataListCoinChild = listCoinChild?.map(coin => {
    //         const valueToObj = dataSideBar[sideBarMode][coinChild][coin]?.map(k => {
    //           // console.log('k: '+k)
    //             var metricName = allMetric[k]?.name
    //             return {name: metricName, id: k}
    //         })?.filter(item => item.name.toLowerCase().includes(textSearch?.toLowerCase()))
    //         return [{name: coin, value: valueToObj}]
    //     })
    // }
    // console.log('metric: '+JSON.stringify(dataListCoinChild))

    // if(dataListCoinChild && dataListCoinChild[0] && dataListCoinChild[0][0] && !metric && dataListCoinChild[0][0].value[0].name) {
    //   // console.log('aa: '+JSON.stringify(dataListCoinChild[0][0]))
    //   setMetric(dataListCoinChild[0][0])
    // }
    // console.log('metric: '+JSON.stringify(metric))
  } catch (error) {
    console.log('erroreeee: ' + error)
  }

  //Latest
  try {
    var dataListCoinChildLatest
    var listCoinChildLatest = sideBarDetailListLatest ? dataSideBar[sideBarModeLatest][coinChildLatest] : null
    listCoinChildLatest = listCoinChildLatest ? Object.keys(listCoinChildLatest) : null
    dataListCoinChildLatest = listCoinChildLatest?.map((coin) => {
      // console.log('coin: '+coin)
      var arrayCoin = dataSideBar[sideBarModeLatest][coinChildLatest][coin]?.filter((item) =>
        item['name'].toLowerCase().includes(textSearchLatest?.toLowerCase())
      )
      var valueToObj = { name: coin, value: arrayCoin }
      return valueToObj
    })
    // console.log('listCoinChild: '+listCoinChild)
    //
    // if(Object.keys(allMetric).length > 0) {
    //   // console.log('all: '+JSON.stringify(allMetric))
    //   dataListCoinChildLatest = listCoinChildLatest?.map(coin => {
    //         const valueToObj = dataSideBar[sideBarModeLatest][coinChildLatest][coin]?.map(k => {
    //           // console.log('k: '+k)
    //             var metricName = allMetric[k]?.name
    //             return {name: metricName, id: k}
    //         })?.filter(item => item.name.toLowerCase().includes(textSearchLatest?.toLowerCase()))
    //         return [{name: coin, value: valueToObj}]
    //     })
    // }
    // console.log('metric: '+JSON.stringify(dataListCoinChildLatest))

    // if(dataListCoinChild && dataListCoinChild[0] && dataListCoinChild[0][0] && !metric && dataListCoinChild[0][0].value[0].name) {
    //   // console.log('aa: '+JSON.stringify(dataListCoinChild[0][0]))
    //   setMetric(dataListCoinChild[0][0])
    // }
    // console.log('metric: '+JSON.stringify(metric))
  } catch (error) {
    console.log('error: ' + error)
  }

  return (
    <div className={cx('data')}>
      {/* <img src={img} style={{width: 80, height: 80}}/> */}
      <div>
        <AppModal
          closeRef={closeModal}
          width={399}
          height={142}
          isOpen={openModal}
          contentStyle={{
            borderRadius: '16px',
            padding: '24px',
            border: 'none',
            backgroundColor: '#29384E'
          }}
        >
          <div className={cx('submit-modal')}>
            <div ref={closeModal} onClick={() => setOpenModal(false)} className={cx('close')}>
              <CloseIcon />
            </div>
            <p className={cx('heading')}>You have to register to participate in the tournament to access the data</p>
            <Link target='_blank' rel='noopener noreferrer' to='/signup' className={cx('btn')}>
              <AppButton
                style={{ width: '100%', background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
              >
                Sign up now
              </AppButton>
            </Link>
          </div>
        </AppModal>
        <div className={cx('bg-1')}></div>
        <div className={cx('bg-2')}></div>
        <div className={cx('bg-3')}></div>
        <Toaster position='top-center' reverseOrder={false} />
        <div className={cx(maxSm ? 'data-wrapper-mobile' : 'data-wrapper')}>
          {verifyClick ? <div className={cx('verify-notification')}>Please check your email to verify!</div> : null}
          <div className={cx('heading')}>Download historical data</div>
          <p className={cx('description')}>Obtain the most up-to-date financial information</p>
          <p className={cx('description')}>
            To obtain additional details regarding the data we offer, kindly refer to our document dedicated to the raw
            data section.
          </p>
          <Container>
            <div className={cx('download-wrapper')}>
              <Row gutter={12}>
                <Col lg={1} md={1}></Col>
                <Col lg={5} md={5} sm={24} xs={24} gap={12}>
                  <div ref={line1FirstRef} className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                    <div className={cx('custom-select')} onClick={() => setShowOptions(!showOptions)}>
                      <div className={cx('selected-option')}>{sideBarMode}</div>
                      <div className={cx('arrow')} style={showOptions ? { rotate: '180deg' } : {}}>
                        <ChevronDown />
                      </div>
                    </div>
                    <div style={showOptions ? {} : { display: 'none' }} className={cx('options')}>
                      {listCoin.map((option) => {
                        return (
                          <div key={option} className={cx('option')} onClick={() => handleOptionClick(option)}>
                            {option}
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </Col>
                <Col lg={6} md={6} sm={24} xs={24} gap={12}>
                  <div ref={line1SecondRef} className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                    <div className={cx('custom-select')} onClick={() => setShowOptionsCoinChild(!showOptionsCoinChild)}>
                      <div className={cx('selected-option')}>{coinChild}</div>
                      <div className={cx('arrow')} style={showOptionsCoinChild ? { rotate: '180deg' } : {}}>
                        <ChevronDown />
                      </div>
                    </div>
                    <div style={showOptionsCoinChild ? {} : { display: 'none' }} className={cx('options')}>
                      {sideBarDetailList?.map((option) => {
                        return (
                          <div key={option} className={cx('option')} onClick={() => handleDetailSideBarClick(option)}>
                            {option}
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </Col>
                {dataListCoinChild ? (
                  <Col style={{}} lg={6} md={6} sm={24} xs={24} gap={12}>
                    <div className={cx('form-field')}>
                      <input
                        ref={symbolInput}
                        id='username-login'
                        type='text'
                        onFocus={() => {
                          setShowSuggest(true)
                        }}
                        onBlur={() => {
                          setTimeout(() => {
                            setShowSuggest(false)
                          }, [200])
                        }}
                        value={textSearch}
                        onChange={(e) => setSearchText(e.target.value)}
                        placeholder='Search metric ...'
                      />
                      {/* <div style={!showSuggest ? { display: 'none' } : {}} className={cx('suggest-symbol')}>
                        {symbols?.symbols !== [] ? (
                          symbols?.symbols?.map((symbolItem) => {
                            if (symbolItem.includes(symbol.toUpperCase()))
                              return (
                                <div
                                  key={symbolItem}
                                  className={cx('symbol')}
                                  onClick={() => handleSymbolClick(symbolItem)}
                                >
                                  {symbolItem}
                                </div>
                              )
                            else {
                              return null
                            }
                          })
                        ) : (
                          <div className={cx('symbol')} key='not-found'>
                            No symbol matching your search
                          </div>
                        )}
                      </div> */}
                    </div>
                    {isShowTableMatric ? (
                      <div
                        style={{
                          width: '100%',
                          background: '#1c244eb4',
                          marginTop: 10,
                          paddingTop: 14,
                          paddingLeft: 20,
                          paddingRight: 20,
                          borderRadius: 10,
                          height: 220,
                          overflow: 'auto',
                          top: 70,
                          left: 0
                        }}
                      >
                        <div>
                          {dataListCoinChild?.map((item) => {
                            return (
                              <>
                                <div style={{ display: 'flex', alignItems: 'center', color: '#9DA7BA' }}>
                                  <i
                                    style={{ fontSize: 24, marginRight: 12 }}
                                    className='fa-brands fa-square-youtube'
                                  ></i>
                                  <div style={{ fontSize: 16 }}>{item['name']}</div>
                                </div>
                                <div style={{ marginLeft: 36, paddingBottom: 20 }}>
                                  {item['value']?.map((i) => {
                                    //#7A8FFE
                                    return (
                                      <>
                                        <div
                                          onClick={() => handleChooseData(i['id'], i['name'])}
                                          style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            marginTop: 14,
                                            cursor: 'pointer'
                                          }}
                                        >
                                          {/* <i style={{fontSize: 24, marginRight: 12, color: '#9DA7BA'}} class="fa-brands fa-square-youtube"></i> */}
                                          <div
                                            style={{
                                              width: 10,
                                              height: 10,
                                              borderRadius: 5,
                                              marginRight: 10,
                                              marginTop: 2,
                                              background: data == i['id'] ? '#7A8FFE' : 'white'
                                            }}
                                          ></div>
                                          <div
                                            className='wrap_content'
                                            style={{
                                              color: data == i['id'] ? '#7A8FFE' : 'white',
                                              maxWidth: '90%',
                                              width: 'max-content'
                                            }}
                                          >
                                            {i['name']}
                                          </div>
                                        </div>
                                      </>
                                    )
                                  })}
                                </div>
                              </>
                            )
                          })}
                        </div>
                      </div>
                    ) : null}
                  </Col>
                ) : (
                  <Col style={{ marginTop: 22 }} lg={6} md={6} sm={24} xs={24} gap={12}>
                    <div
                      style={{
                        width: '100%',
                        background: '#29384E',
                        marginTop: 10,
                        paddingTop: 14,
                        paddingLeft: 20,
                        paddingRight: 20,
                        borderRadius: 10,
                        height: 40
                      }}
                    ></div>
                  </Col>
                )}

                {/* <Col lg={10} md={10} sm={24} xs={24} gap={12}>
                  <div className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                    <div className={cx('custom-select')} onClick={() => setShowOptionsCoinChild(!showOptionsCoinChild)}>
                      <div className={cx('selected-option')}>{}</div>
                      <div className={cx('arrow')} style={showOptionsCoinChild ? { rotate: '180deg' } : {}}>
                        <ChevronDown />
                      </div>
                    </div>
                    <div style={showOptionsCoinChild ? {} : { display: 'none' }} className={cx('options')}>
                      {dataListCoinChild?.map((option) => {
                        return (
                          <div key={option} className={cx('option')} onClick={() => handleCoinChildDetailClick(option)}>
                            {option[0].name}
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </Col> */}
                {symbol ? (
                  <Col lg={3} md={3} sm={8} xs={8}>
                    <div ref={line1ThirdRef} className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                      <div className={cx('custom-select')} onClick={() => setShowSymbolDropDown(!showSymbolDropDown)}>
                        <div className={cx('selected-option')}>{symbol['value']}</div>
                        <div className={cx('arrow')} style={showSymbolDropDown ? { rotate: '180deg' } : {}}>
                          <ChevronDown />
                        </div>
                      </div>
                      <div style={showSymbolDropDown ? {} : { display: 'none' }} className={cx('options')}>
                        {listSymbol?.map((option) => {
                          return (
                            <div
                              key={option}
                              className={cx('option')}
                              onClick={() => handleSymbolDropDownClick({ id: option, value: option })}
                            >
                              {option}
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    {/* <DropDownComponent backGround={'#29384E'} currentState={symbol} setCurrentState={setSymbol} listElement={listSymbol?.map(i => {return {'id': i, 'value': i}})}/> */}
                  </Col>
                ) : null}

                <Col lg={2} md={2} sm={8} xs={8}>
                  <div style={{ justifyContent: 'flex-start' }} className={cx('button')}>
                    <AppButton
                      isLoading={isSubmittingFirst}
                      onClick={() => {
                        handleDownLoadZipData()
                      }}
                      style={{ background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
                    >
                      Download
                    </AppButton>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <div
          style={{ marginTop: 60, marginBottom: 220 }}
          className={cx(maxSm ? 'data-wrapper-mobile' : 'data-wrapper')}
        >
          {verifyClick ? <div className={cx('verify-notification')}>Please check your email to verify!</div> : null}
          <div className={cx('heading')}>Download recent round data</div>
          <p className={cx('description')}>Retrieve current data for upcoming submission</p>
          <Container>
            <div className={cx('download-wrapper')}>
              <Row gutter={12}>
                <Col lg={1} md={1} sm={24} xs={24} gap={12}></Col>
                <Col lg={5} md={5} sm={24} xs={24} gap={12}>
                  <div ref={line2FirstRef} className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                    <div className={cx('custom-select')} onClick={() => setShowOptionsLatest(!showOptionsLatest)}>
                      <div className={cx('selected-option')}>{sideBarModeLatest}</div>
                      <div className={cx('arrow')} style={showOptionsLatest ? { rotate: '180deg' } : {}}>
                        <ChevronDown />
                      </div>
                    </div>
                    <div style={showOptionsLatest ? {} : { display: 'none' }} className={cx('options')}>
                      {listCoinLatest.map((option) => {
                        return (
                          <div key={option} className={cx('option')} onClick={() => handleOptionClickLatest(option)}>
                            {option}
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </Col>
                <Col lg={6} md={6} sm={24} xs={24} gap={12}>
                  <div ref={line2SecondRef} className={cx('custom-select-wrapper')} onBlur={handleOutsideClickLatest}>
                    <div
                      className={cx('custom-select')}
                      onClick={() => setShowOptionsCoinChildLatest(!showOptionsCoinChildLatest)}
                    >
                      <div className={cx('selected-option')}>{coinChildLatest}</div>
                      <div className={cx('arrow')} style={showOptionsCoinChildLatest ? { rotate: '180deg' } : {}}>
                        <ChevronDown />
                      </div>
                    </div>
                    <div style={showOptionsCoinChildLatest ? {} : { display: 'none' }} className={cx('options')}>
                      {sideBarDetailListLatest?.map((option) => {
                        return (
                          <div
                            key={option}
                            className={cx('option')}
                            onClick={() => handleDetailSideBarClickLatest(option)}
                          >
                            {option}
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </Col>
                {dataListCoinChildLatest ? (
                  <Col style={{}} lg={6} md={6} sm={24} xs={24} gap={12}>
                    <div className={cx('form-field')}>
                      <input
                        ref={symbolInput}
                        id='username-login'
                        type='text'
                        onFocus={() => {
                          setShowSuggest(true)
                        }}
                        onBlur={() => {
                          setTimeout(() => {
                            setShowSuggest(false)
                          }, [200])
                        }}
                        value={textSearchLatest}
                        onChange={(e) => setSearchTextLatest(e.target.value)}
                        placeholder='Search metric ...'
                      />
                      {/* <div style={!showSuggest ? { display: 'none' } : {}} className={cx('suggest-symbol')}>
                        {symbols?.symbols !== [] ? (
                          symbols?.symbols?.map((symbolItem) => {
                            if (symbolItem.includes(symbol.toUpperCase()))
                              return (
                                <div
                                  key={symbolItem}
                                  className={cx('symbol')}
                                  onClick={() => handleSymbolClick(symbolItem)}
                                >
                                  {symbolItem}
                                </div>
                              )
                            else {
                              return null
                            }
                          })
                        ) : (
                          <div className={cx('symbol')} key='not-found'>
                            No symbol matching your search
                          </div>
                        )}
                      </div> */}
                    </div>
                    {isShowTableMatricLatest ? (
                      <div
                        style={{
                          width: '100%',
                          background: '#1c244eb4',
                          marginTop: 10,
                          paddingTop: 14,
                          paddingLeft: 20,
                          paddingRight: 20,
                          borderRadius: 10,
                          height: 220,
                          overflow: 'auto',
                          top: 70,
                          left: 0
                        }}
                      >
                        <div>
                          {dataListCoinChildLatest?.map((item) => {
                            return (
                              <>
                                <div style={{ display: 'flex', alignItems: 'center', color: '#9DA7BA' }}>
                                  <i
                                    style={{ fontSize: 24, marginRight: 12 }}
                                    className='fa-brands fa-square-youtube'
                                  ></i>
                                  <div style={{ fontSize: 16 }}>{item['name']}</div>
                                </div>
                                <div style={{ marginLeft: 36, paddingBottom: 20 }}>
                                  {item['value']?.map((i) => {
                                    //#7A8FFE
                                    return (
                                      <>
                                        <div
                                          onClick={() => handleChooseDataLatest(i['id'], i['name'])}
                                          style={{
                                            display: 'flex',
                                            alignItems: 'center',
                                            marginTop: 14,
                                            cursor: 'pointer'
                                          }}
                                        >
                                          {/* <i style={{fontSize: 24, marginRight: 12, color: '#9DA7BA'}} class="fa-brands fa-square-youtube"></i> */}
                                          <div
                                            style={{
                                              width: 10,
                                              height: 10,
                                              borderRadius: 5,
                                              marginRight: 10,
                                              marginTop: 2,
                                              background: dataLatest == i['id'] ? '#7A8FFE' : 'white'
                                            }}
                                          ></div>
                                          <div
                                            className='wrap_content'
                                            style={{
                                              color: dataLatest == i['id'] ? '#7A8FFE' : 'white',
                                              maxWidth: '90%',
                                              width: 'max-content'
                                            }}
                                          >
                                            {i['name']}
                                          </div>
                                        </div>
                                      </>
                                    )
                                  })}
                                </div>
                              </>
                            )
                          })}
                        </div>
                      </div>
                    ) : null}
                  </Col>
                ) : (
                  <Col style={{ marginTop: 22 }} lg={6} md={6} sm={24} xs={24} gap={12}>
                    <div
                      style={{
                        width: '100%',
                        background: '#29384E',
                        marginTop: 10,
                        paddingTop: 14,
                        paddingLeft: 20,
                        paddingRight: 20,
                        borderRadius: 10,
                        height: 40
                      }}
                    ></div>
                  </Col>
                )}

                {/* <Col lg={10} md={10} sm={24} xs={24} gap={12}>
                  <div className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                    <div className={cx('custom-select')} onClick={() => setShowOptionsCoinChild(!showOptionsCoinChild)}>
                      <div className={cx('selected-option')}>{}</div>
                      <div className={cx('arrow')} style={showOptionsCoinChild ? { rotate: '180deg' } : {}}>
                        <ChevronDown />
                      </div>
                    </div>
                    <div style={showOptionsCoinChild ? {} : { display: 'none' }} className={cx('options')}>
                      {dataListCoinChild?.map((option) => {
                        return (
                          <div key={option} className={cx('option')} onClick={() => handleCoinChildDetailClick(option)}>
                            {option[0].name}
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </Col> */}
                {symbolLatest ? (
                  <Col lg={3} md={3} sm={8} xs={8}>
                    <div ref={line2ThirdRef} className={cx('custom-select-wrapper')} onBlur={handleOutsideClick}>
                      <div
                        className={cx('custom-select')}
                        onClick={() => setShowSymbolDropDownLatest(!showSymbolDropDownLatest)}
                      >
                        <div className={cx('selected-option')}>{symbolLatest['value']}</div>
                        <div className={cx('arrow')} style={showSymbolDropDownLatest ? { rotate: '180deg' } : {}}>
                          <ChevronDown />
                        </div>
                      </div>
                      <div style={showSymbolDropDownLatest ? {} : { display: 'none' }} className={cx('options')}>
                        {listSymbolLatest?.map((option) => {
                          return (
                            <div
                              key={option}
                              className={cx('option')}
                              onClick={() => handleSymbolDropDownLatestClick({ id: option, value: option })}
                            >
                              {option}
                            </div>
                          )
                        })}
                      </div>
                    </div>
                    {/* <DropDownComponent backGround={'#29384E'} currentState={symbolLatest} setCurrentState={setSymbolLatest} listElement={listSymbolLatest?.map(i => {return {'id': i, 'value': i}})}/> */}
                  </Col>
                ) : null}
                <Col lg={2} md={2} sm={8} xs={8}>
                  <div style={{ justifyContent: 'flex-start' }} className={cx('button')}>
                    <AppButton
                      isLoading={isSubmitting}
                      onClick={() => {
                        handleDownLoadZipDataLatest()
                      }}
                      style={{ background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}
                    >
                      Download
                    </AppButton>
                  </div>
                </Col>
              </Row>
            </div>
          </Container>
        </div>
        <div className={cx('divider')}>
          <div className={cx('left')}></div>
          <div className={cx('center')}>
            <StarDivider />
            <StarDivider />
            <StarDivider />
          </div>
          <div className={cx('right')}></div>
        </div>
        <div
          style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
          className={cx('api-key-wrapper')}
        >
          <div style={{ maxWidth: 1200 }} className={cx('api-key')}>
            <Row style={{ width: '100%' }} gutter={12}>
              <Col lg={12} md={12} sm={0} xs={0}>
                <div className={cx('text')}>
                  <div style={{ marginTop: '24px' }} className={cx('heading')}>
                    Using NestQuant API
                  </div>
                  <p className={cx('description')}>
                    If you wish to engage with our data using an API, please explore{' '}
                    <a
                      style={{ color: '#b0aeff', fontWeight: '500' }}
                      href='https://github.com/nestquant/NestQuant-Tutorial'
                      target='_blank'
                      rel='noreferrer'
                    >
                      {' '}
                      this repository
                    </a>
                    .
                  </p>
                  <div className={cx('api-button')}>
                    <AppButton isLoading={isGettingAPIKey || isCreatingAPIKey} onClick={handleGetAPIKey}>
                      Get API key
                    </AppButton>
                  </div>
                  {apiKey && !newApiKey ? (
                    <div className={cx('api-key')}>{`Your API key: ${apiKey.api_key}`}</div>
                  ) : null}
                  {newApiKey ? <div className={cx('api-key')}>{`Your API key: ${newApiKey.api_key}`}</div> : null}
                </div>
              </Col>
              <Col lg={12} md={12} sm={24} xs={24}>
                <div className={cx('get-api-key')}>
                  <img src={data3} alt='api-key' />
                </div>
              </Col>
              <Col lg={0} md={0} sm={24} xs={24}>
                <div className={cx('text-mobile')}>
                  <div style={{ marginTop: '24px' }} className={cx('heading')}>
                    Using NestQuant API
                  </div>
                  <p className={cx('description')}>
                    If you wish to engage with our data using an API, please explore{' '}
                    <a
                      style={{ color: '#b0aeff', fontWeight: '500' }}
                      href='https://github.com/nestquant/NestQuant-Tutorial'
                      target='_blank'
                      rel='noreferrer'
                    >
                      {' '}
                      this repository
                    </a>
                    .
                  </p>
                  <div className={cx('api-button')}>
                    <AppButton isLoading={isGettingAPIKey || isCreatingAPIKey} onClick={handleGetAPIKey}>
                      Get API key
                    </AppButton>
                  </div>
                  {apiKey && !newApiKey ? (
                    <div className={cx('api-key')}>{`Your API key: ${apiKey.api_key}`}</div>
                  ) : null}
                  {newApiKey ? <div className={cx('api-key')}>{`Your API key: ${newApiKey.api_key}`}</div> : null}
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Data
