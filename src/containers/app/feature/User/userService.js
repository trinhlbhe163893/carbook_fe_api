import { createApi } from '@reduxjs/toolkit/query/react'
import customFetchBase from '@src/configs/customFetchBase'

// const tournamentName = process.env.tournament_id

export const userApi = createApi({
  reducerPath: 'userApi',
  baseQuery: customFetchBase,
  endpoints: (build) => ({
    getPersonalPerformation: build.query({
      query: () => ({
        url: `/user/performance`
      })
    }),
    getPublicPerformance: build.query({
      query: (username) => ({
        url: `/user/performance/${username}`
      })
    }),
    getPublicInformation: build.query({
      query: (username) => ({
        url: `/user/${username}`
      })
    }),
    getData: build.query({
      query: (args) => ({
        url: `/data/download_link`,
        params: args,
        responseHandler: async (response) => {
          if (response.status === 200) {
            return await response.json()
            // window.location.assign(window.URL.createObjectURL(await response.blob()))
          } else {
            return await response.json()
          }
        },
        cache: 'no-cache'
      })
    }),
    verifyEmail: build.mutation({
      query: (body) => ({
        url: '/user/verify',
        method: 'POST',
        body: body
      })
    }),
    getUserPersonalInformation: build.query({
      query: () => ({
        url: `/user`
      })
    }),
    changeUserInformation: build.mutation({
      query: (body) => {
        return {
          url: '/user',
          method: 'PUT',
          body: body,
          responseHandler: async (response) => {
            const responseBody = await response.json()
            return responseBody
          }
        }
      }
    }),
    changeUserPassWord: build.mutation({
      query: (body) => {
        return {
          url: '/user/password',
          method: 'PUT',
          body: new URLSearchParams(body).toString(),
          headers: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          responseHandler: async (response) => {
            const responseBody = await response.json()
            return responseBody
          }
        }
      }
    }),
    createApiKey: build.mutation({
      query: () => {
        return {
          url: '/data/api-key',
          method: 'POST'
        }
      }
    }),
    getApiKey: build.query({
      query: () => {
        return {
          url: '/user/api-key'
        }
      }
    }),
    forgotPassword: build.mutation({
      query: (body) => {
        return {
          url: '/user/forgot-password',
          method: 'POST',
          body: body
        }
      }
    }),
    getSymbolByCategory: build.query({
      query: (args) => {
        return {
          url: '/data/symbols',
          params: args
        }
      }
    }),
    getVerifytournament: build.query({
      query: () => {
        return {
          url: '/tournament/nestquant_tournament_2023/registered'
        }
      }
    }),
    getLeaderboard: build.query({
      query: ({ tournament_id, submission_type }) => {
        return {
          url: `/tournament/${tournament_id}/performance/all?submission_type=${submission_type}`
        }
      }
    }),
    getTeamInformation: build.query({
      query: (args) => ({
        // url: `/team`,
        url: `/team?tournament_id=nestquant_tournament_2023`,
        params: args
      })
    }),
    changeTeamInformation: build.mutation({
      query: (body) => {
        return {
          url: '/team',
          method: 'PUT',
          body: body,
          responseHandler: async (response) => {
            const responseBody = await response.json()
            return responseBody
          }
        }
      }
    }),
    getTeamMembers: build.query({
      query: () => ({
        url: `/team/members`
      })
    }),
    submitModel: build.mutation({
      query: ({ params, body }) => ({
        url: `/tournament/${params['tournamentSearchId']}/submit/file?submission_type=${params['submissionType']}&symbol=${params['symbol']}&model_id=${params['model']}`,
        method: 'POST',
        body: body,
        prepareHeaders: (headers) => {
          headers.set('Content-Type', 'multipart/form-data')
          return headers
        }
      })
    }),
    // https://api-dev.nestquant.com/tournament/{tournament_id}/performance/{submission_type}/mean-scores
    getSubmissionMeanScores: build.query({
      query: ({ path }) => ({
        url: `/tournament/${path.tournamentId}/performance/${path.submission}/mean-scores`
        // params: params
      })
    }),
    getSubmissionTimeRecords: build.query({
      query: ({ path, params }) => ({
        url: `/tournament/${path.tournamentId}/performance/${path.submission}/records`,
        params: params
      })
    }),
    getSubmissionSymbols: build.query({
      query: ({ path }) => ({
        url: `/tournament/${path.tournamentId}/performance/${path.submission}/symbols`
      })
    }),
    getModelPerformance: build.query({
      query: ({ path, params }) => ({
        url: `/tournament/${path.tournamentId}/performance/${path.submission}/record`,
        params: params
      })
    }),
    getAllUserPerformance: build.query({
      query: ({ path, params }) => ({
        url: `/tournament/${path.tournamentId}/performance/all`,
        params: params
      })
    }),
    deleteSubmission: build.mutation({
      query: ({ path, body }) => ({
        url: `/tournament/${path.tournamentId}/performance/${path.submission}/${path.submission_time}`,
        method: 'DELETE',
        params: body
      })
    }),
    getCurrentRound: build.query({
      query: (tournament_id) => ({
        url: `tournament/${tournament_id}/current-round`
      })
    }),
    getVerifyCompetition: build.query({
      query: () => {
        return {
          url: '/competition/nestquant_tournament_2023/registered'
        }
      }
    }),
    changeAvatar: build.mutation({
      query: (body) => {
        return {
          url: '/user/image',
          method: 'POST',
          body: body,
          // formData:true  ,
          prepareHeaders: (headers) => {
            headers.set('Content-Type', 'multipart/form-data')
            return headers
          },
          responseHandler: async (response) => {
            const responseBody = await response.json()
            return responseBody
          }
        }
      }
    })
  })
})

export const {
  useLazyGetDataQuery,
  useGetPersonalPerformationQuery,
  useGetPublicInformationQuery,
  useGetPublicPerformanceQuery,
  useVerifyEmailMutation,
  useChangeUserInformationMutation,
  useChangeUserPassWordMutation,
  useGetUserPersonalInformationQuery,
  useForgotPasswordMutation,
  useLazyGetApiKeyQuery,
  useCreateApiKeyMutation,
  useLazyGetSymbolByCategoryQuery,
  useLazyGetVerifytournamentQuery,
  useLazyGetLeaderboardQuery,
  useChangeTeamInformationMutation,
  useLazyGetTeamInformationQuery,
  useLazyGetTeamMembersQuery,
  useLazyGetVerifyCompetitionQuery,
  useChangeAvatarMutation
} = userApi
