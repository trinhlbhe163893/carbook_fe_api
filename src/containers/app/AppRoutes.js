import { customerRouteList } from './feature/Homepage/route'
import { boardRouteList } from './feature/LeaderBoard/route'
import { staticRouteList } from './feature/StaticPages/route'
import { userRouteList } from './feature/User/route'
import { companyRouteList } from './feature/CompanyManagement/route'
import { routeRouteList } from './feature/RoutesManagement/route'
import { booking } from './feature/Ticket/route'

export const AppRouteList = [
  ...customerRouteList,
  ...staticRouteList,
  ...userRouteList,
  ...boardRouteList,
  ...companyRouteList, ...routeRouteList,
  ...booking
]
