/* eslint-disable no-unused-vars */
import { createApi } from '@reduxjs/toolkit/query/react'
import { REFRESH_TOKEN_EXPIRATION, RESPONSE_ERROR_STATUS } from '@src/configs'
import customFetchBase from '@src/configs/customFetchBase'
import Cookies from 'universal-cookie'
const cookies = new Cookies()

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: customFetchBase,
  endpoints: (build) => ({
    login: build.mutation({
      query: (body) => {
        return {
          url: '/api/User/login',
          method: 'POST',
          body: body,
          headers: {
            'content-type': 'application/json'
          },
          responseHandler: async (response) => {
            const responseBody = await response.json()
            console.log(responseBody)
            if (responseBody.status !== RESPONSE_ERROR_STATUS) {
              console.log('responseBody', responseBody)
              cookies.set('access_token', responseBody?.authorization, {
                maxAge: REFRESH_TOKEN_EXPIRATION
              })
              // cookies.set('user_id', responseBody?.metadata?.user?._id, {
              //   maxAge: REFRESH_TOKEN_EXPIRATION
              // })
            }
            return responseBody
          }
        }
      }
    }),
    verifyOTP: build.mutation({
      query: (body) => {
        return {
          url: 'auth',
          method: 'POST',
          body: new URLSearchParams(body).toString(),
          // headers: {
          //   'content-type': 'application/x-www-form-urlencoded'
          // },
          responseHandler: async (response) => {
            const responseBody = await response.json()
            if (responseBody.access_token) {
              cookies.set('access_token', responseBody?.access_token, {
                path: '/',
                domain: process.env.DOMAIN
              })
              cookies.set('access_token_signal', responseBody?.access_token, {
                path: '/',
                domain: process.env.DOMAIN
              })
            }
            return responseBody
          }
        }
      }
    }),
    signup: build.mutation({
      query: (body) => {
        return {
          url: '/api/User/register',
          method: 'POST',
          body: body
        }
      }
    }),
    getProfile: build.query({
      query: () => ({
        url: '/api/User/userprofile',
        cache: 'no-cache'
      }),
      invalidate: (oldData, newData, { queryFulfilled }) => {
        // Only invalidate if the status of the response has changed.

        return oldData?.status !== newData?.status
      }
    }),
    oAuthLogin: build.query({
      query: () => ({
        url: '/login/success',
        credentials: 'include',
        responseHandler: async (response) => {
          const responseBody = await response.json()

          if (response.ok) {
            cookies.set('access_token', responseBody?.authorization, {
              maxAge: REFRESH_TOKEN_EXPIRATION,
              path: '/',
              domain: process.env.DOMAIN
            })
            cookies.set('user_id', responseBody?.metadata?.user?._id, {
              maxAge: REFRESH_TOKEN_EXPIRATION
            })
            cookies.set('access_token_signal', responseBody?.authorization, {
              path: '/',
              domain: process.env.DOMAIN
            })
          }
          return responseBody
        }
      })
    }),
    logout: build.mutation({
      query: () => {
        return {
          url: '/user/logout',
          method: 'POST',
          responseHandler: async (response) => {
            const responseBody = await response.json()
            if (responseBody.code !== 403) {
              // clear cookies
              cookies.remove('access_token',{
                path: '/',
                domain: process.env.DOMAIN
              })
              cookies.remove('access_token_signal', {
                path: '/',
                domain: process.env.DOMAIN
              })
              cookies.remove('user_id')
            }
            return responseBody
          }
        }
      }
    }),

    refreshToken: build.mutation({
      query: () => {
        return {
          url: '/user/refresh-token',
          method: 'POST',
          responseHandler: async (response) => {
            const responseBody = await response.json()
            if (responseBody.errorStatus !== RESPONSE_ERROR_STATUS) {
              cookies.set('user_id', responseBody?.metadata?.user?._id)
              cookies.set('access_token', responseBody?.metadata?.tokens?.accessToken,{
                path: '/',
                domain: process.env.DOMAIN
              })
              cookies.set('access_token_signal', responseBody?.metadata?.tokens?.accessToken, {
                path: '/',
                domain: process.env.DOMAIN
              })
            }
            return responseBody
          }
        }
      }
    })
  })
})

export const {
  useRefreshTokenMutation,
  useLoginMutation,
  useSignupMutation,
  useVerifyOTPMutation,
  useLogoutMutation,
  useLazyGetProfileQuery
} = authApi
