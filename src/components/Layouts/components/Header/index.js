/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */

import classNames from 'classnames/bind'
import { Link, useNavigate } from 'react-router-dom'
import styles from './Header.module.sass'
// import logo_mobile from '@src/assets/images/logo_mobile.png'
import { LogoNestQuantMobile, NestQuantLogo } from '@src/assets/svgs'
import { I18LANGUAGE_KEY } from '@src/configs'
import { useEffect, useState } from 'react'
// import AppButton from '@src/components/AppButton'
import AppButton from '@src/components/AppButton'
import { Col, Row } from 'antd'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { Container } from '@mui/material'
import { Menu } from 'lucide-react'
import Cookies from 'universal-cookie'
const cookies = new Cookies()
// import { getImage } from '@src/axios/getApiAll'

const cx = classNames.bind(styles)

function Header() {
  const [openMenu, setOpenMenu] = useState(false)
  // const [iconshow, setIconshow] = useState(false)
  const userInfo = useSelector((state) => state.auth.user)
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn)
  const { t } = useTranslation()
  const navigate = useNavigate()
  // const [isLoading, setIsLoading] = useState(false)

  // const handleChangeLang = (e) => {
  //   setSwitchLang(e.target.checked)
  //   if (e.target.checked) {
  //     localStorage.setItem(I18LANGUAGE_KEY, 'vi')
  //     i18n.changeLanguage('vi')
  //   } else {
  //     localStorage.setItem(I18LANGUAGE_KEY, 'en')
  //     i18n.changeLanguage('en')
  //   }
  // }
  // const [img, setImg] = useState('')

  useEffect(() => {
    const currLang = localStorage.getItem(I18LANGUAGE_KEY)
    if (!currLang) localStorage.setItem(I18LANGUAGE_KEY, 'en')
  }, [])

  // console.log("userInfo: "+JSON.stringify(userInfo))
  const handleLogin = () => {
    try {
      cookies.set('current_page', process.env.TOURNAMENT_PAGE, {
        path: '/',
        domain: process.env.DOMAIN
      })
    } catch (error) {
      console.log('errorSetCurrentPage: ' + error)
    }
    navigate('/login')
  }
  const handleOverview = () => {
    try {
      cookies.set('current_page', process.env.TOURNAMENT_PAGE, {
        path: '/',
        domain: process.env.DOMAIN
      })
    } catch (error) {
      console.log('errorSetCurrentPage: ' + error)
    }
    navigate(`/overview`)
  }
  return (
    <header className={cx('header-wrapper')}>
      <Container>
        <div className={cx('header')}>
          <Row justify='space-between'>
            <Col lg={8} md={14} sm={6} style={{ display: 'flex', alignItems: 'center' }}>
              <Row>
                <Col lg={0} md={24} xs={24} sm={24}>
                  <div className={cx('logo-mobile-wrapper')}>
                    <div
                      style={{ display: 'flex', alignItems: 'center' }}
                      onClick={() => {
                        setOpenMenu(!openMenu)
                        // , setIconshow(!iconshow)
                      }}
                      className={cx('menu-icon')}
                    >
                      <Menu color='#FFFFFF' />
                    </div>
                    {openMenu ? (
                      <>
                        <div className={cx('menu-mobile')}>
                          {/* {isLoggedIn ? (
                                <Link
                                  className={cx('__item')}
                                  to='/submission'
                                  onClick={() => {
                                    setOpenMenu(false)
                                  }}
                                >
                                  {t('header.nav.submission')}
                                </Link>
                              ) : null} */}
                          {isLoggedIn ? (
                            <Link
                              to='/model'
                              onClick={() => {
                                setOpenMenu(false)
                              }}
                            ></Link>
                          ) : null}
                          {/* <Link
                            to='/data'
                            onClick={() => {
                              setOpenMenu(false)
                            }}
                            className={cx('__item')}
                          >
                            {t('header.nav.data')}
                          </Link>

                          <Link
                            to='/leaderboard'
                            onClick={() => {
                              setOpenMenu(false)
                            }}
                            className={cx('__item')}
                          >
                            {t('header.nav.leaderboard')}
                          </Link>

                          <Link
                            to='https://tournament-docs.nestquant.com/'
                            onClick={() => {
                              setOpenMenu(false)
                            }}
                            className={cx('__item')}
                          >
                            {t('header.nav.documents')}
                          </Link> */}
                          {/* <Link to={'//' + process.env.VAULT_PAGE} className={cx('__item')}>
                            {t('header.nav.vault')}
                          </Link>
                          <Link className={cx('__item')} style={{ opacity: 0.6 }}>
                            {t('header.nav.signal')}
                          </Link> */}
                          {/* <Link
                                to='https://discord.com/invite/Nqv3pXBpCu'
                                onClick={() => {
                                  setOpenMenu(false)
                                }}
                                className={cx('__item')}
                                >
                                {t('header.nav.contacts')}
                              </Link> */}

                          <Link className={cx('__item')} to='/'>
                            Home
                          </Link>
                          <Link className={cx('__item')} to='/booking'>
                            Booking
                          </Link>
                        </div>
                        <div
                          onClick={() => {
                            setOpenMenu(false)
                          }}
                          className={cx('menu-back')}
                        ></div>
                      </>
                    ) : null}
                    <div className={cx('logo')}>
                      <Link to='/'>
                        {/* <LogoNestQuantMobile className='logo_mobile' /> */}

                        {/* <img className='logo_mobile' src={logo_mobile} alt='logo' /> */}
                      </Link>
                    </div>
                  </div>
                </Col>
                <Col lg={24} md={0} xs={0} sm={0}>
                  <div className={cx('logo')}>
                    <Link to='/'>
                      <NestQuantLogo />

                      {/* <img className='logo' src={logo} alt='logo' /> */}
                    </Link>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={16} md={0} sm={0} xs={0}>
              <div className={cx('controls')}>
                <nav className={cx('nav')}>
                  {/* <Link to='#' className={cx('__item')}>
                      {t('header.nav.model')}
                    </Link> */}
                  {/* {isLoggedIn ? (
                        <Link className={cx('__item')} to='/submission'>
                          {t('header.nav.submission')}
                        </Link>
                      ) : null} */}
                  {isLoggedIn ? (
                    <Link className={cx('__item')} to='/model'>
                      {/* {t('header.nav.model')} */}
                    </Link>
                  ) : null}
                  <Link className={cx('__item')} to='/'>
                    Home
                  </Link>
                  {/* <Link className={cx('__item')} to='/vehicle'>
                    Vehicle
                  </Link>

                  <Link to='/ticket' className={cx('__item')}>
                    Ticket
                  </Link>
                  <Link to='/account' className={cx('__item')}>
                    Account
                  </Link>
                  <Link to='/route' className={cx('__item')}>
                    Route
                  </Link>
                  <Link to='/company' className={cx('__item')}>
                    Company
                  </Link> */}
                  <Link to='/booking' className={cx('__item')}>
                    Booking
                  </Link>
                </nav>
                {isLoggedIn ? (
                  <div style={{ width: '50px' }}>
                    <div onClick={handleOverview}>
                      {userInfo?.profile_image_url ? (
                        <img alt='' className={cx('imgAvatarPC')} src={userInfo?.profile_image_url} />
                      ) : (
                        <div className={cx('avatar')}>{userInfo?.userName && userInfo?.userName[0]?.toUpperCase()}</div>
                      )}
                    </div>
                  </div>
                ) : (
                  <div className={cx('auth')}>
                    <div onClick={handleLogin}>
                      <AppButton>{t('header.auth.sign-in')}</AppButton>
                    </div>
                    <Link to='/signup'>
                      <AppButton style={{ background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}>
                        {t('header.auth.sign-up')}
                      </AppButton>
                    </Link>
                  </div>
                )}
              </div>
            </Col>
            <Col lg={0} md={10} sm={10}>
              {isLoggedIn ? (
                <div style={{ width: '40px', marginLeft: 'auto' }}>
                  <div onClick={handleOverview}>
                    {userInfo?.profile_image_url ? (
                      <img alt='' className={cx('imgAvatar')} src={userInfo?.profile_image_url} />
                    ) : (
                      <div className={cx('avatar')}>{userInfo?.userName && userInfo?.userName[0]?.toUpperCase()}</div>
                    )}
                  </div>
                </div>
              ) : (
                <div className={cx('auth-mobile')}>
                  <div onClick={handleLogin}>
                    <AppButton>{t('header.auth.sign-in')}</AppButton>
                  </div>
                  <Link to='/signup'>
                    <AppButton style={{ background: 'linear-gradient(96.06deg, #526AEA 4.1%, #B0AEFF 146.32%)' }}>
                      {t('header.auth.sign-up')}
                    </AppButton>
                  </Link>
                </div>
              )}
              {/* <Link to='/overview'>
                  <div className={cx('avatar')}>{userInfo?.username && userInfo?.username[0]?.toUpperCase()}</div>
                </Link> */}
            </Col>
          </Row>
        </div>
      </Container>
    </header>
  )
}

export default Header
