import React from 'react'
import './Avatar.scss'
import { IconEdit } from '@src/assets/svgs'
const Avatar = ({ username, img }) => {
  // Lấy chữ cái đầu của username
  const firstLetter = username?.charAt(0).toUpperCase()
  const avatarStyle = {
    // backgroundColor: '#596FEB',
    color: '#FFFF',
    borderRadius: '8px',
    // width: '80px',
    // height: '80px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '36px',
    fontWeight: 'bold'
  }
  return (
    <>
      {img ? (
        <img alt='' className='imgAvatar' src={img} />
      ) : (
        <div className='avatar' style={avatarStyle}>
          <span>{firstLetter}</span>
        </div>
      )}
      <div className='iconEdit'>
        <IconEdit />
      </div>
    </>
  )
}

export default Avatar
