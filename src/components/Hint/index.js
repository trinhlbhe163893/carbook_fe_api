import { InforIcon } from '@src/assets/svgs'
import React from 'react'
import './HintStyle.scss'
import ReactTooltip from 'react-tooltip'

export default function index({ content }) {
  var randomDecimal = Math.random();
  return (
    <div className='wrap'>
      <div
        style={{
          position: 'relative', // Set position relative for positioning the tooltip
          cursor: 'pointer',    // Change cursor to indicate interactivity
        }}
        data-tip={content} data-for={`tooltip-${randomDecimal}`}
      >
        <InforIcon />
      </div>
      <ReactTooltip
        id={`tooltip-${randomDecimal}`}
        place="bottom"
        // type="dark"
        effect="solid"
        arrowColor='rgba(49, 60, 101, 1)'
        className="custom-tooltip" // Add a custom class for styling the tooltip
      />
    </div>
  )
}
