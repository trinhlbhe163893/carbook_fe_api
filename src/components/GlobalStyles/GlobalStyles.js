import PropTypes from 'prop-types'
import './GlobalStyles.sass'

import { useEffect } from 'react'
import { useLazyGetProfileQuery } from '@src/containers/authentication/feature/Auth/authService'
import { login, logout, setUser } from '@src/containers/authentication/feature/Auth/authSlice'
import { useDispatch, useSelector } from 'react-redux'
// import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
import { getUserProfile } from '@src/axios/getApiAll'
import ReactLoading from 'react-loading'
const cookies = new Cookies()

// const events = ['load', 'mousemove', 'mousedown', 'click', 'scroll', 'keypress']

function GlobalStyles({ children }) {
  var loadingLogin = useSelector((state) => state.home.isShowLoading)
  const [getProfile] = useLazyGetProfileQuery()
  const dispatch = useDispatch()
  // let timer

  // this function sets the timer that logs out the user after 10 secs
  // const handleLogoutTimer = () => {
  //   /*timer = */ setTimeout(() => {
  //     // clears any pending timer.
  //     // resetTimer();
  //     // // // Listener clean up. Removes the existing event listener from the window
  //     // Object.values(events).forEach((item) => {
  //     //   window.removeEventListener(item, resetTimer);
  //     // });

  //     // // logs out user
  //     logoutAction()
  //     handleLogoutTimer()
  //   }, 10 * 60 * 1000)
  // }

  // this resets the timer if it exists.
  // const resetTimer = () => {
  //   if (timer) clearTimeout(timer)
  // }

  // useEffect(() => {
  //   // Object.values(events).forEach((item) => {
  //   //   window.addEventListener(item, () => {
  //   //     resetTimer();
  //   handleLogoutTimer()
  //   // });
  //   // });
  // }, [])

  // logs out user by clearing out auth token in localStorage and redirecting url to /signin page.
  // const logoutAction = () => {
  //   localStorage.clear()
  //   const accessToken = cookies.get('access_token')

  //   if (accessToken) {
      
  //     getProfile('a', false)
  //       .then((response) => {

  //         if (response?.error?.status === 401 || response?.isSuccess === false) {
  // console.log("GLOBAL")

  //           dispatch(logout())
  //           cookies.remove('access_token',{
  //             path: '/',
  //             domain: process.env.DOMAIN
  //           })
  //           cookies.remove('access_token_signal',{
  //             path: '/',
  //             domain: process.env.DOMAIN
  //           })
  //           cookies.remove('current_page', {
  //             path: '/',
  //             domain: process.env.DOMAIN
  //           })
  //           location.href = '/login'
  //         } else {
  //           dispatch(setUser({ ...response.data }))
  //           dispatch(login())
  //         }
  //       })
  //       .catch((err) => {
  //         console.log('error:: ', err)
  //       })
  //   } else {
  //     dispatch(logout())
  //   }
  // }

  return (
    <div>
      {
        loadingLogin ? 
        <div>
          <div style={{position: 'absolute', top: '50%', right: '50%'}}>
            <ReactLoading type='spin' height={32} width={32} />
          </div>
          <div style={{opacity: 0.5}}>{children}</div>
        </div>
        :
        <div>{children}</div>
      }
      
    </div>

  )
}

GlobalStyles.propTypes = {
  children: PropTypes.node.isRequired
}

export default GlobalStyles
