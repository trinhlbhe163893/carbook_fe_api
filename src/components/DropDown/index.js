/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState, useRef, useEffect } from 'react'
import './DropDown.scss'

const DropDownComponent = ({
  listElement,
  currentState,
  setCurrentState,
  width,
  mode,
  backGround,
  height,
  CallFnWhenChangeValue
}) => {
  const [onChoose, setOnChoose] = useState(false)
  const dropDownComponentRef = useRef()
  // console.log('listElement: '+JSON.stringify(listElement))
  useEffect(() => {
    const handleClickOutSide = (e) => {
      if (dropDownComponentRef.current && !dropDownComponentRef.current.contains(e.target)) {
        setOnChoose(false)
      }
    }
    document.body.addEventListener('click', handleClickOutSide)
  }, [])

  function handleClickChooseBTC() {
    setOnChoose(!onChoose)
  }
  function onClickChooseCoin(ele) {
    // setSideBarMode(mode)
    if (currentState['id'] != ele['id']) {
      setCurrentState(ele)
      if (CallFnWhenChangeValue) {
        CallFnWhenChangeValue()
      }
    }
    setOnChoose(false)
  }
  return (
    <div ref={dropDownComponentRef}>
      {/* Initial */}
      <div
        onClick={() => handleClickChooseBTC()}
        style={{
          background: backGround ? backGround : '#29384e',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          cursor: 'pointer',
          color: 'white',
          paddingLeft: 12,
          paddingRight: 12,
          borderRadius: 12,
          paddingTop: 6,
          paddingBottom: 6,
          marginTop: 6,
          height: 36
        }}
      >
        <div style={{ display: 'flex', alignItems: 'center', fontSize: 16, fontWeight: 400, padding: 4 }}>
          {listElement != undefined
            ? listElement.map((item) => {
                if (item.id == currentState['id']) return item.value
              })
            : currentState['value']}
          {/* <div>{currentState && currentState['id'] ? listElement != undefined ? listElement.map((item) => { if (item.id == currentState['id']) return item.value }) : 'abc' : currentState['name']} </div> */}
        </div>
        {mode != 'noChoose' ? (
          <svg
            className={`animate-example ${onChoose ? 'rotate-animation' : ''}`}
            style={onChoose ? {} : { transform: 'rotate(-90deg)' }}
            width='16'
            height='9'
            viewBox='0 0 16 9'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              fillRule='evenodd'
              clipRule='evenodd'
              d='M0.792893 0.542893C1.18342 0.152369 1.81658 0.152369 2.20711 0.542893L8 6.33578L13.7929 0.542891C14.1834 0.152367 14.8166 0.152366 15.2071 0.542891C15.5976 0.933415 15.5976 1.56658 15.2071 1.9571L9.41422 7.75C8.63317 8.53105 7.36684 8.53105 6.58579 7.75L0.792894 1.95711C0.402369 1.56658 0.402369 0.933418 0.792893 0.542893Z'
              fill='white'
            />
          </svg>
        ) : (
          ''
        )}
      </div>
      {mode != 'noChoose' && (
        <div
          className={`animate-exampleABS ${onChoose ? 'fade-in' : 'fade-out'}`}
          style={{
            position: 'absolute',
            zIndex: 100,
            background: backGround ? backGround : '#29384e',
            // display: 'flex',
            // alignItems: 'center',
            // justifyContent: 'space-between',
            flexWrap: 'wrap',
            cursor: 'pointer',
            color: 'white',
            paddingLeft: 12,
            paddingRight: 12,
            borderRadius: 16,
            paddingTop: 8,
            paddingBottom: 8,
            marginTop: 10,
            // display: 'block',
            display: onChoose ? 'block' : 'none',
            width: width ? width : '90%',
            // zIndex: 100,
            height: 176,
            overflow: 'auto'
          }}
        >
          {listElement != undefined
            ? listElement.map((ele, index) => {
                const check = currentState && ele['id'] == currentState['id']
                return (
                  <div
                    key={index}
                    className='hoverDropDown'
                    onClick={() => onClickChooseCoin(ele)}
                    style={{
                      background: check ? '#364061' : '#29384e',
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingLeft: 10,
                      paddingRight: 10,
                      borderRadius: 7,
                      width: '100%',
                      paddingTop: 6,
                      paddingBottom: 6,
                      marginBottom: 5,
                      marginTop: 5
                    }}
                  >
                    <div
                      style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: 16 }}
                    >
                      <div style={check ? { color: '#7A88FE' } : { color: 'white' }}>{ele['value']}</div>
                    </div>
                  </div>
                )
              })
            : ''}
        </div>
      )}
    </div>
  )
}

export default DropDownComponent
