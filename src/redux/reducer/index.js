import { combineReducers } from 'redux'
import authReducer from '@src/containers/authentication/feature/Auth/authSlice'
import userReducer from '@src/containers/app/feature/User/userSlice'
import homeReducer from '@src/containers/app/feature/Customer/pages/Home/homeSlice'
import modelReducer from '@src/containers/app/feature/User/components/CurrenRound/modelSlice'
import { authApi } from '@src/containers/authentication/feature/Auth/authService'
import { userApi } from '@src/containers/app/feature/User/userService'

const allReducers = combineReducers({
  auth: authReducer,
  user: userReducer,
  home: homeReducer,
  model: modelReducer,
  authApi: authApi.reducer,
  userApi: userApi.reducer
})

export const rootReducer = (state, action) => {
  if (action.type === 'auth/logout') {
    state = undefined
  }
  return allReducers(state, action)
}
