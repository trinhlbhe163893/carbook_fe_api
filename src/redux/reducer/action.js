/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-loss-of-precision */

import { createAsyncThunk } from '@reduxjs/toolkit'
// import { ethers } from 'ethers'
// import coin_icon from '../../src/assets/coin-icon.png'
// import { setAlert, clearAlert } from '@src/containers/app/feature/User/alertSlice'

// const typechain = require("nestquant-vault-sdk");

// const CONTROLLER_ETHERERUM = '0x0Dd957413364e2d8E5621974C60807f926607434'
// let controller

export const connectWallet = createAsyncThunk('counterconnectWallet', async () => {
  const { ethereum } = window
  // const provider = new ethers.providers.Web3Provider(window.ethereum);

  if (!ethereum) {
    throw new Error('MetaMask is not available.')
  }
  const accounts = await ethereum.request({
    method: 'eth_requestAccounts'
  })
  return accounts[0]
})

export const getUSDTBalance = createAsyncThunk('getUSDTBalance', async (/*ob*/) => {
  // // const provider = new ethers.providers.Web3Provider(window.ethereum);
  // const vault = typechain.Vault__factory.connect(ob.address);
  // const decimals = (await vault.connect(provider.getSigner()).decimals()).toString();
  // const underlyingAddress = await vault.connect(provider.getSigner()).underlying();
  // const underlying = typechain.MockUnderlyingToken__factory.connect(underlyingAddress);
  // const underlyingBalance = await underlying.connect(provider.getSigner()).balanceOf(ob.acc);
  // return (underlyingBalance.toString() / (10 ** decimals)).toFixed(2);
  return (2000000000.3234234234).toFixed(2)
})

export const getStaked = createAsyncThunk('Staked', async (/*ob*/) => {
  // // const provider = new ethers.providers.Web3Provider(window.ethereum);
  // // console.log(ob.acc);
  // // console.log(ob.address);
  // const vault = typechain.Vault__factory.connect(ob.address);
  // const staked = await vault.connect(provider.getSigner()).balanceOf(ob.acc);
  // return (staked.toString() / (10 ** 18)).toFixed(2);
  return (11000.23232).toFixed(2)
})

export const deposit = createAsyncThunk('deposit', async (/*ob, { dispatch }*/) => {
  // // const provider = new ethers.providers.Web3Provider(window.ethereum);
  // const vault = typechain.Vault__factory.connect(ob.address);
  // const decimals = (await vault.connect(provider.getSigner()).decimals()).toString();
  // const underlyingAddress = await vault.connect(provider.getSigner()).underlying();
  // const underlying = typechain.MockUnderlyingToken__factory.connect(underlyingAddress);
  // const amountInFixed = ethers.utils.parseUnits(ob.amount, decimals).toBigInt();
  // // console.log(ob.amount);
  // // console.log(ob.address);
  // // const dispatch = useDispatch()
  // try {

  //     // console.log("Sign transaction to approve tokens for vault");
  //     dispatch(setAlert({ type: "info", message: "Sign transaction to approve tokens for vault" }));
  //     const tx = await underlying.connect(provider.getSigner()).approve(vault.target, amountInFixed);
  //     // dispatch(setDataToast({type: "success", mess:"Sign transaction to approve tokens for vault"}))
  //     // console.log(`Waiting for confirmation`);
  //     // await tx.wait();
  //     // console.log(`Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/${tx.hash}`);
  //     // dispatch(setAlert({ type: "success", message: `Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/${tx.hash}` }));
  //     // dispatch(setDataToast({type: "success", mess:`Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/${tx.hash}`}))
  // } catch (error) {
  //     // console.log("Please try again, transaction is rejected!");
  //     dispatch(setAlert({ type: "error", message: "Transaction rejected" }));
  //     return { type: "error", mess: "Please try again, transaction is rejected!" }
  //     // dispatch(setDataToast({type: "error", mess:"Please try again, transaction is rejected!"}))
  // }

  // try {
  //     // console.log(`Sign transaction to place a deposit order for vault`);
  //     dispatch(setAlert({ type: "info", message: "Sign transaction to place a deposit order for vault" }));
  //     const tx = await vault.connect(provider.getSigner()).newDepositOrder(amountInFixed);
  //     // console.log(`Waiting for confirmation`);
  //     // await tx.wait();
  //     // console.log(`Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/${tx.hash}`);
  //     dispatch(setAlert({ type: "success", message: `Successfull, see the transaction on etherscan: `, url: `https://sepolia.etherscan.io/tx/${tx.hash}`}));
  //     // var r = { type: "success", mess: "Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/" + tx.hash }
  // } catch (error) {
  //     // console.log("Please try again, transaction is rejected!");
  //     // const txHash = await tx.hash
  //     dispatch(setAlert({ type: "error", message: "Transaction rejected" }));
  // }
  // // const r = {type: "success", mess:  "Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/" +tx.hash}
  // // console.log(`https://sepolia.etherscan.io/tx/${tx.hash}`);
  // // console.log("TX:"+ tx.hash )
  return null
})

export const withdraw = createAsyncThunk('withdraw', async (/*ob, { dispatch }*/) => {
  // // const provider = new ethers.providers.Web3Provider(window.ethereum);
  // const vault = typechain.Vault__factory.connect(ob.address);
  // const decimals = (await vault.connect(provider.getSigner()).decimals()).toString();
  // const amountInFixed = ethers.utils.parseUnits(ob.amount, decimals).toBigInt();
  // // console.log(amountInFixed);
  // // console.log(ob.amount);
  // // console.log(ob.address);
  // try {
  //     // console.log(`Sign transaction to place a withdraw order for vault`);
  //     dispatch(setAlert({ type: "info", message: "Sign transaction to place a withdraw order for vault" }));
  //     const tx = await vault.connect(provider.getSigner()).newWithdrawOrder(amountInFixed);
  //     // console.log(`Waiting for confirmation`);
  //     // await tx.wait();
  //     // console.log(`Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/${tx.hash}`);
  //     dispatch(setAlert({ type: "success", message: `Successfull, see the transaction on etherscan: `, url: `https://sepolia.etherscan.io/tx/${tx.hash}`}));
  //     // var r = { type: "success", mess: "Successfull, see the transaction on etherscan: https://sepolia.etherscan.io/tx/" + tx.hash }
  // } catch (error) {
  //     // console.log("Please try again, transaction is rejected!");
  //     dispatch(setAlert({ type: "error", message: "Transaction rejected" }));
  // }
  return null
})

export const disconnect = createAsyncThunk('disconnect', async () => {
  return ''
})

// tableData
// export const getTableData = createAsyncThunk('getTableData', async (value) => {
// const provider = new ethers.providers.Web3Provider(window.ethereum);
//     if (Object.keys(value).length == 0) {
//         let vaultAddressList;
//         let controllerAdd;
//         // kiem tra xem mang nao dang dc chon ETH hay BSC default: ETH
//         switch (0) {
//             case 0:
//                 controllerAdd = CONTROLLER_ETHERERUM;
//                 break;
//             default:
//                 controllerAdd = CONTROLLER_ETHERERUM;
//         }
//         controller = typechain.Controller__factory.connect(controllerAdd);
//         vaultAddressList = (await controller.connect(provider.getSigner()).getAllVaults()).toArray();
//         // console.log(vaultAddressList);
//         const list = await Promise.all(vaultAddressList.map(async (e) => {
//             // console.log(e);
//             const vault = typechain.Vault__factory.connect(e);
//             const balance = (await vault.connect(provider.getSigner()).balanceOf(await provider.getSigner().getAddress())).toString();
//             const decimals = (await vault.connect(provider.getSigner()).decimals()).toString();
//             // console.log((balance / 10 ** decimals).toFixed(2));
//             // console.log(decimals);
//             const tvl = (await vault.connect(provider.getSigner()).underlyingBalanceWithInvestment()).toString();
//             // console.log(tvl / 10 ** decimals);
//             const name = await controller.connect(provider.getSigner()).getVaultName(e);
//             // console.log(name);
//             return {
//                 id: e,
//                 vault: { img: coin_icon, name: name },
//                 apy: "82.49%",
//                 riskEstimation: "1.12",
//                 tvl: `$${(tvl / 10 ** decimals).toFixed(2)}`,
//                 myBalance: `$${(balance / 10 ** decimals).toFixed(2)}`,
//                 address: e
//             }
//         }));

//         return list
//     }
//     return value

// });

export const setDataVault = createAsyncThunk('getTableData', async (value) => {
  return value
})

export const setDataVaultSelected = createAsyncThunk('setDataVaultSelected', async (value) => {
  return value
})

// set data Toast
export const setDataToast = createAsyncThunk('setDataToast', async (value) => {
  return value
})
