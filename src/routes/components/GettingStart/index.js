/* eslint-disable jsx-a11y/no-static-element-interactions */
import {
  ExitIcon,
  RightArrowIcon,
  TreeIcon,
  BulbIcon,
  StarIcon,
  LineIconLong,
  LineIconShort,
  RewardIcon,
  PieChartIcon
} from '@src/assets/svgs'
import './GettingStart.scss'

import { useState } from 'react'
import useMediaQuery from '@src/hooks/useMediaQuery'
import { useNavigate } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import {
  setShowGettingStartOff,
  setShowGettingStartOn
} from '@src/containers/app/feature/Customer/pages/Home/homeSlice'
import { Link } from 'react-router-dom'

const GettingStart = ({ children }) => {
  const isShowGettingStart = useSelector((state) => state.home.showGettingStart)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const maxSmSize = useMediaQuery('(max-width: 768px)')
  const maxSmZoom = useMediaQuery('(max-width: 1900px)')

  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn)

  const [gettingStartMode, setGettingStartMode] = useState('dataScientist')
  // const [isOpen, setIsOpen] = useState(true)

  const handleOnclickMode = (mode) => {
    setGettingStartMode(mode)
  }

  const handleOnclickNavigate = (data) => {
    isLoggedIn ? navigate(data) : navigate('/login')
    handleTurnOfGettingStart()
  }

  const handleTurnOfGettingStart = () => {
    dispatch(setShowGettingStartOff())
  }

  const handleTurnOnGettingStart = () => {
    dispatch(setShowGettingStartOn())
  }

  return (
    <>
      {children}
      {isShowGettingStart ? (
        <div className={`animate-example ${isShowGettingStart ? 'fade-in' : 'fade-out'}`}>
          <div className={maxSmSize ? 'containerMobile' : maxSmZoom ? 'containerZoom' : 'container'}>
            <div className='header'>
              <div className={maxSmSize ? 'headerTextMobile' : `${maxSmZoom ? 'headerTextMobile' : 'headerText'}`}>
                Getting Started!
              </div>
              <div onClick={() => handleTurnOfGettingStart()} className='headerIcon'>
                <ExitIcon />
              </div>
            </div>

            <div className='tabWrap'>
              <div
                onClick={() => handleOnclickMode('dataScientist')}
                className={
                  gettingStartMode == 'dataScientist'
                    ? `active ${maxSmSize ? 'tabMobile' : 'tab'}`
                    : `${maxSmSize ? 'tabMobile' : 'tab'}`
                }
              >
                Data Scientist
              </div>
              <div
                onClick={() => handleOnclickMode('Others')}
                className={
                  gettingStartMode == 'Others'
                    ? `active  ${maxSmSize ? 'tabMobile' : 'tab'}`
                    : ` ${maxSmSize ? 'tabMobile' : 'tab'}`
                }
              >
                Others
              </div>
            </div>
            {gettingStartMode == 'dataScientist' ? (
              <>
                <div onClick={() => handleOnclickNavigate('/data')} className='content'>
                  <div className='contentBlock1'>
                    <div>
                      <div className='iconWrap'>
                        <div className='icon'>
                          <PieChartIcon />
                        </div>
                        <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                          Data Mining
                        </div>
                      </div>
                      <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                        Explore what we have and what you can do with our data.
                      </div>
                    </div>
                  </div>
                  <div className='contentBlock2'>
                    <RightArrowIcon />
                  </div>
                  <div className={maxSmSize ? 'lineIconMobile' : 'lineIcon'}>
                    <LineIconShort />
                  </div>
                </div>

                <div onClick={() => handleOnclickNavigate('/model')} className='content'>
                  <div className='contentBlock1'>
                    <div>
                      <div className='iconWrap'>
                        <div className='icon'>
                          <TreeIcon />
                        </div>
                        <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                          Make your first model/submission
                        </div>
                      </div>
                      <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                        Make your first submission, explore the backtest tool for validation.
                      </div>
                    </div>
                  </div>
                  <div className='contentBlock2'>
                    <RightArrowIcon />
                  </div>
                  <div className='lineIcon'>
                    <LineIconLong />
                  </div>
                </div>

                <Link to='https://tournament-docs.nestquant.com/nestquant-tournament/overview' style={{ textDecoration: 'none' }}>
                  <div className='content'>
                    <div className='contentBlock1'>
                      <div >
                        <div className='iconWrap'>
                          <div className='icon'>
                            <BulbIcon />
                          </div>
                          <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                            Learn more about our tournament(s)
                          </div>
                        </div>
                        <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                          Understand the rules and the evaluation metrics.
                        </div>
                      </div>
                    </div>
                    <div className='contentBlock2'>
                      <RightArrowIcon />
                    </div>
                    <div className={maxSmSize ? 'lineIconMobile' : 'lineIcon'}>
                      <LineIconShort />
                    </div>
                  </div>
                </Link>

                <div className='content'>
                  <div className='contentBlock1'>
                    <div>
                      <div className='iconWrap'>
                        <div className='icon'>
                          <StarIcon />
                        </div>
                        <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                          Go live with your model
                        </div>
                      </div>
                      <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                        Build your model and submit it to the live tournament.
                      </div>
                      {/* <div className='detail'>Explore another tournament</div> */}
                    </div>
                  </div>
                  <div className='contentBlock2'>
                    <RightArrowIcon />
                  </div>
                </div>
              </>
            ) : (
              <>
                <div onClick={() => handleOnclickNavigate('/model')} className='content'>
                  <div className='contentBlock1'>
                    <div>
                      <div className='iconWrap'>
                        <div className='icon'>
                          <TreeIcon />
                        </div>
                        <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                          Make your first submission
                        </div>
                      </div>
                      <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                        Try to predict the prices and beat the others
                      </div>
                    </div>
                  </div>
                  <div className='contentBlock2'>
                    <RightArrowIcon />
                  </div>
                  <div className={maxSmSize ? 'lineIconMobile' : 'lineIcon'}>
                    <LineIconShort />
                  </div>
                </div>

                <Link to='https://tournament-docs.nestquant.com/nestquant-tournament/overview' style={{ textDecoration: 'none' }}>
                  <div className='content'>
                    <div className='contentBlock1'>
                      <div>
                        <div className='iconWrap'>
                          <div className='icon'>
                            <BulbIcon />
                          </div>
                          <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                            Learn more about our tournament(s)
                          </div>
                        </div>
                        <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                          Understand the rules and the evaluation metrics.
                        </div>
                      </div>
                    </div>
                    <div className='contentBlock2'>
                      <RightArrowIcon />
                    </div>
                    <div className={maxSmSize ? 'lineIconMobile' : 'lineIcon'}>
                      <LineIconShort />
                    </div>
                  </div>
                </Link>

                <div onClick={() => handleOnclickNavigate('/model')} className='content'>
                  <div className='contentBlock1'>
                    <div>
                      <div className='iconWrap'>
                        <div className='icon'>
                          <StarIcon />
                        </div>
                        <div className={`${maxSmSize ? 'labelMobile' : `${maxSmZoom ? 'labelMobile' : 'label'}`}`}>
                          Turn submission into a habit
                        </div>
                      </div>
                      <div className={`${maxSmSize ? 'detailMobile' : `${maxSmZoom ? 'detailMobile' : 'detail'}`}`}>
                        Boost your earnings by consistently delivering high-quality submission.
                      </div>
                    </div>
                  </div>
                  <div className='contentBlock2'>
                    <RightArrowIcon />
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      ) : (
        <div className='OpenButton' onClick={() => handleTurnOnGettingStart()}>
          <RewardIcon />
        </div>
      )}
    </>
  )
}

export default GettingStart
