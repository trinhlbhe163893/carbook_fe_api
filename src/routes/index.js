/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react-hooks/exhaustive-deps */
import { AppRouteList } from '@src/containers/app/AppRoutes'
import { AuthRouteList } from '@src/containers/authentication/AuthRoutes'
import { memo, useEffect } from 'react'
import { useRoutes, useNavigate, useSearchParams } from 'react-router-dom'
// import jwt_decode from 'jwt-decode'
import { useLazyGetProfileQuery } from '@src/containers/authentication/feature/Auth/authService'
import { login, logout, setUser } from '@src/containers/authentication/feature/Auth/authSlice'
import { useDispatch, useSelector } from 'react-redux'
// import { toast } from 'react-toastify'
import Cookies from 'universal-cookie'
// import { setIsShowLoading } from '@src/containers/app/feature/Customer/pages/Home/homeSlice'
const cookies = new Cookies()

export const AppRoutes = () => {
  const [getProfile] = useLazyGetProfileQuery()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [searchParams, setSearchParams] = useSearchParams();
  // const location = useLocation()
  // const [loginSuccess] = authApi.endpoints.oAuthLogin.useLazyQuery()

  // useEffect(() => {
  //   const accessToken = cookies.get('access_token')
  //   console.log('accessToken', accessToken)

  //   if (!accessToken) {
  //     const loginRequest = async () => {
  //       console.log('login request running')
  //       const response = await loginSuccess()
  //       if (response.isSuccess) {
  //         dispatch(setUser(response.data.metadata.user))
  //         dispatch(login())
  //       } else if (response) {
  //         console.log('error response: ', response.error.data.message.strategy)
  //         if (location.pathname !== '/signup') navigate('/login')
  //         toast.warn(response.error.data.message.error)
  //       }
  //     }
  //     loginRequest()
  //   }
  // }, [])

  useEffect( () => {
    
    async function checkLogin(){
    const accessToken = cookies.get('access_token')

    if (accessToken && !searchParams.has('auth_str')) {
      // dispatch(setIsShowLoading(true))
      await getProfile({})
        .then((response) => {
          // console.log("asdas: " + JSON.stringify(response))
          if (response?.error?.status === 401 || response?.isSuccess === false) {
            console.log("Router")
            dispatch(logout())
            cookies.remove('access_token', {
              path: '/',
              domain: process.env.DOMAIN
            })
            cookies.remove('access_token_signal', {
              path: '/',
              domain: process.env.DOMAIN
            })
            cookies.remove('current_page', {
              path: '/',
              domain: process.env.DOMAIN
            })
            // dispatch(setIsShowLoading(false))
            navigate('/login')
          } else {
            // console.log("asdasda: " + JSON.stringify(response))
            dispatch(setUser({ ...response.data }))
            dispatch(login())
            // dispatch(setIsShowLoading(false))
          }
        })
        .catch((err) => {
          console.log('error:: ', err)
          // dispatch(setIsShowLoading(false))
        })
      // }
    } else {
      dispatch(logout())
      // dispatch(setIsShowLoading(false))
    }
  }
  checkLogin()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const routes = [...AppRouteList, ...AuthRouteList]

  return useRoutes([...routes])
}

export const WebRoutes = memo(AppRoutes)
